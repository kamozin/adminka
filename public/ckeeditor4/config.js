/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.extraPlugins = "youtube";
    config.allowedContent = true;
    config.autoParagraph = false;
    config.format_p =
        {
            element: 'p',
            attributes:
                {
                    'class': 'images-content'
                }
        };
};

CKEDITOR.config.font_names = "Open Sans;";
CKEDITOR.config.font_defaultLabel = 'Open Sans';
