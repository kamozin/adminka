<?php

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Route::any('/check-ip', function () {
    $ip = request()->ip(); // Получаем IP-адрес клиента
    Log::info("Request from IP: $ip");
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/sendSmsOrder', 'Api\V1\StatusController@sendSmsOrder');
Route::get('/worksImage', 'WorksImageController@index');

Route::get('/getProductId', 'Raec\IndexController@getProductId');
Route::get('/eFinder', 'eFinderController@index');
Route::get('/getRaecProduct', 'RaecController@getRaecProduct');
Route::get('/ValueRetailFeatureValueProductDropDuplicateController', 'ValueRetailFeatureValueProductDropDuplicateController@start');
Route::get('/RemovedProductFromAssortimentController', 'RemovedProductFromAssortimentController@start');

Route::get('/wbupload', 'WBController@index');
Route::get('/valuesUpdate', 'TestController@valuesUpdate'); //подмена значений
Route::get('/generatePrice', 'TestController@generatePrice'); //подмена значений


Route::get('/checkProducts', 'CheckFiltersController@checkProducts');
Route::get('/checkFeutures', 'CheckFiltersController@checkFeutures');
Route::get('/checkValueFeaturesCategory', 'CheckFiltersController@checkValueFeaturesCategory');
Route::get('/dropValueFeaturesProductNotDisplay', 'CheckFiltersController@dropValueFeaturesProductNotDisplay'); // удаление характеристик
Route::get('/setTaskCheckProducts', 'CheckFiltersController@setTaskCheckProducts');
Route::get('uploadImageShop/{id}', 'RaecController@uploadImageShop');
Route::get('uploadImageShopJob', 'RaecController@loadImageTask');


Route::post('/upload/list/sticker', 'ListProductController@uploadIcon');
Route::post('/upload/list/sticker/product', 'ListProductController@uploadIconProduct');
Route::get('/antey', 'UploadController@antey');
Route::get('/load/image', 'LoadImage@start');
Route::get('/load/image/raec', 'LoadImage@startRaec'); // загружаем из списка
Route::get('loadImg', 'LoadImage@loadImg'); // загружаем из списка
Route::get('loadImageUrl', 'LoadImage@loadImageUrl'); // загружаем из списка
Route::get('loadImgProduct', 'LoadImage@loadImgProduct'); // загружаем из списка
Route::get('/setProductLoadImage', 'LoadImage@setProductLoadImage'); //добавляем в список товары для загрузки фото
Route::get('/CheckImage', 'CheckImageController@start');
Route::get('/fileRaec', 'FileController@index');
Route::get('/analogRaec', 'AnalogController@index');
Route::get('/imageTrim', 'ImageTrimController@index');
Route::get('/imageTrim/original', 'ImageTrimController@original');
Route::get('/image/trim/handle', 'ImageTrimController@handle');


Auth::routes();
//Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
//Route::get('/2fa/disable', 'Google2FAController@disableTwoFactor');
//Route::get('/2fa/validate', 'Google2FAController@getValidateToken');
//Route::post('/2fa/validate', ['middleware' => 'throttle:5', 'uses' => 'Google2FAController@postValidateToken']);

Route::group(['prefix' => '2fa'], function () {
    Route::get('/', 'LoginSecurityController@show2faForm');
    Route::get('/generateSecret', 'LoginSecurityController@generate2faSecret')->name('generate2faSecret');
    Route::post('/enable2fa', 'LoginSecurityController@enable2fa')->name('enable2fa');
    Route::post('/disable2fa', 'LoginSecurityController@disable2fa')->name('disable2fa');

    // 2fa middleware
    Route::post('/2faVerify', function () {
        return redirect(URL()->previous());
    })->name('2faVerify')->middleware('2fa');
});

Route::middleware(['auth', '2fa'])->group(function () {
    Route::get('/', 'CategoryController@index')->middleware(['auth', '2fa', 'throttle']);
    Route::group(['prefix' => 'transfer'], function () {
        Route::get('/category', 'Transfer\CategoryController@category1C');
        Route::get('/category-retail/etim', 'Transfer\CategoryController@uploadExcelEtimToCategoryRelated');
        Route::get('/features', 'Transfer\FeaturesController@parseFeatures');
        Route::get('/features/set/category', 'Transfer\FeaturesController@parseFeaturesToCategory');
        Route::get('/features/value', 'Transfer\FeaturesController@parceFeatureValue');
        Route::get('/features/value/to/category', 'Transfer\FeaturesController@parseValueFeatureCategory');
        Route::get('/products', 'Transfer\ProductController@index');
        Route::get('/products/filters', 'Transfer\ProductController@getProductsFeatures');
        Route::get('/users', 'Transfer\UserController@index');
        Route::get('/orders', 'Transfer\UserController@orders');
        Route::get('/orders/products', 'Transfer\UserController@orderProducts');


        Route::get('/raec/files', 'Transfer\ProductController@transferFiles');

    });
    Route::post('/orders/update/payment', 'OrdersController@updatePayment');

    Route::group(['prefix' => 'category-retail'], function () {
        Route::get('', 'CategoryController@index');
        Route::get('/create', 'CategoryController@create');
        Route::post('', 'CategoryController@store');
        Route::get('/edit/{uid}', 'CategoryController@edit');
        Route::post('/update', 'CategoryController@update');
        Route::post('/display', 'CategoryController@display');
        Route::get('/sort', 'CategoryController@sort');
        Route::post('/sort/update', 'CategoryController@sortUpdate');
        Route::get('/struct', 'CategoryController@struct');
        Route::post('/struct/update', 'CategoryController@structUpdate');
        Route::post('/relation/catalog/update', 'CategoryController@relationCatalogUpdate');
        Route::get('/relation/etim/not', 'CategoryController@getEtimNotRelation');
        Route::get('/relation/etim/{uid}', 'CategoryController@relationEtim');
        Route::post('/relation/etim/update', 'CategoryController@relationEtimUpdate');

    });

    Route::group(['prefix' => 'telegram-user'], function () {
        Route::get('', 'TelegramUserController@index')->name('telegram-user');
        Route::get('/create', 'TelegramUserController@create')->name('telegramuser.create');
        Route::post('', 'TelegramUserController@store')->name('telegramuser.store');
        Route::get('/edit', 'TelegramUserController@edit');
        Route::post('/update', 'TelegramUserController@update')->name('telegramuser.update');
        Route::get('/display', 'TelegramUserController@display');
        Route::get('/delete', 'TelegramUserController@delete');
    });

    Route::group(['prefix' => 'features'], function () {
        Route::get('', 'FeaturesController@index');
        Route::get('/show/{uid}', 'FeaturesController@show');
        Route::post('/sort/update', 'FeaturesController@setSortFeatures');
        Route::post('/get/json', 'FeaturesController@getFeatureJson');
        Route::post('/update/display', 'FeaturesController@updateDisplay');
        Route::post('/update/gold', 'FeaturesController@updateGold');
        Route::post('/updateQueueFeature', 'FeaturesController@updateQueueFeature');
        Route::post('/update/filter/use', 'FeaturesController@updateUseFilter');
        Route::get('/get/etim', 'FeaturesController@getEtimFeatures');
        Route::post('/associate/etim', 'FeaturesController@associateEtim');
        Route::get('/get/value', 'FeaturesController@getValue');
        Route::get('/products', 'FeaturesController@filterProducts');
    });


    Route::group(['prefix' => 'etim/group'], function () {
        Route::get('', 'CategoryController@index');
        Route::get('/create', 'CategoryController@create');
        Route::post('/store', 'CategoryController@strore');
        Route::get('/edit/{uid}', 'CategoryController@edit');
        Route::post('/update', 'CategoryController@update');
    });

    Route::group(['prefix' => 'etim/class'], function () {
        Route::get('', 'CategoryController@index');
        Route::get('/create', 'CategoryController@create');
        Route::post('/store', 'CategoryController@strore');
        Route::get('/edit/{uid}', 'CategoryController@edit');
        Route::post('/update', 'CategoryController@update');
    });

    Route::group(['prefix' => 'raec'], function () {
        Route::get('product/{raecId}', 'RaecController@index');
        Route::post('upload/file/from/download/products', 'RaecController@getUploadFile');
        Route::get('repeatUploadRaec', 'RaecController@repeatUpload');
        Route::get('load/raec/sku', 'RaecController@loadRaecSku');
        Route::get('featureRaecProcessing', 'RaecController@featureRaecProcessing'); //проставляем тип после прогрузки каждый фильтр отдельно
        Route::get('featureProcessingCategory', 'RaecController@featureProcessingCategory'); //проставляем тип после прогрузки каждый фильтр отдельно без job
        Route::get('reduceToNumber', 'RaecController@reduceToNumber'); // если тип N то приводим к числу
        Route::get('store/product', 'Raec\IndexController@index');
        Route::get('list/upload', 'Raec\IndexController@index')->name('raec.ListProductUpload');
        Route::get('list/upload/values', 'Raec\IndexController@indexValues')->name('raec.indexValues');
        Route::get('file/upload', 'Raec\IndexController@fileUpload');
        Route::get('file/upload/values', 'Raec\IndexController@fileUploadValues');
        Route::post('upload/file/from/download/products/values', 'Raec\IndexController@uploadValues');
        Route::get('dropProductRaecCategory', 'RaecController@dropProductRaecCategory');
        Route::get('loadValues', 'LoadValues@index');
        Route::get('loadImage', 'RaecController@loadImage');
        Route::get('loadImageV', 'RaecController@loadImageV');
        Route::get('loadImageOne', 'RaecController@loadImageOne');
        Route::get('video', 'RaecController@video');


    });

    Route::group(['prefix' => 'faq'], function () {
        Route::get('/', 'FaqController@index')->name('faq.index');
        Route::post('/store', 'FaqController@store')->name('faq.store');
        Route::get('/edit', 'FaqController@edit')->name('faq.edit');
        Route::get('/create', 'FaqController@create')->name('faq.create');
        Route::get('/drop', 'FaqController@drop')->name('faq.drop');
        Route::post('/update', 'FaqController@update')->name('faq.update');
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('/', 'NewsController@index')->name('news.index');
        Route::post('/store', 'NewsController@store')->name('news.store');
        Route::get('/edit', 'NewsController@edit')->name('news.edit');
        Route::get('/create', 'NewsController@create')->name('news.create');
        Route::get('/drop', 'NewsController@drop')->name('news.drop');
        Route::post('/update', 'NewsController@update')->name('news.update');
    });

    Route::group(['prefix' => 'actions'], function () {
        Route::get('/', 'ActionsController@index')->name('actions.index');
        Route::post('/store', 'ActionsController@store')->name('actions.store');
        Route::get('/edit', 'ActionsController@edit')->name('actions.edit');
        Route::get('/create', 'ActionsController@create')->name('actions.create');
        Route::get('/drop', 'ActionsController@drop')->name('actions.drop');
        Route::post('/update', 'ActionsController@update')->name('actions.update');
        Route::post('/update/visible', 'ActionsController@updateVisible')->name('actions.update.visible');

        Route::post('/upload', 'ActionsController@uploadImageText')->name('actions.upload.file');
    });

    Route::group(['prefix' => 'articles'], function () {
        Route::get('/', 'ArticlesController@index')->name('articles.index');
        Route::post('/store', 'ArticlesController@store')->name('articles.store');
        Route::get('/edit', 'ArticlesController@edit')->name('articles.edit');
        Route::get('/create', 'ArticlesController@create')->name('articles.create');
        Route::get('/drop', 'ArticlesController@drop')->name('articles.drop');
        Route::post('/update', 'ArticlesController@update')->name('articles.update');
        Route::post('/update/visible', 'ArticlesController@updateVisible')->name('articles.update.visible');

        Route::post('/upload', 'ArticlesController@uploadImageText')->name('articles.upload.file');
    });

    Route::group(['prefix' => 'contact'], function () {
        Route::get('/', 'ContactsController@index')->name('contacts.index');
        Route::post('/store', 'ContactsController@store')->name('contacts.store');
        Route::get('/edit/{id}', 'ContactsController@edit')->name('contacts.edit');
        Route::get('/create', 'ContactsController@create')->name('contacts.create');
        Route::get('/drop/{id}', 'ContactsController@drop')->name('contacts.drop');
        Route::post('/update', 'ContactsController@update')->name('contacts.update');
    });


    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', 'ClientsController@index')->name('clients');
        Route::post('/update/phone', 'ClientsController@updatePhone');
    });


    Route::group(['prefix' => 'list/product'], function () {
        Route::get('', 'ListProductController@index')->name('list.index');
        Route::post('store', 'ListProductController@storeList');
        Route::post('get', 'ListProductController@getProduct');
        Route::post('store/item', 'ListProductController@storeProductList');
        Route::post('delete', 'ListProductController@dropList');
        Route::post('update', 'ListProductController@update');
        Route::post('delelet/item', 'ListProductController@deleteProduct');
        Route::post('display', 'ListProductController@displayList');
        Route::post('sort/update', 'ListProductController@sortUpdate');
        Route::post('sort/update/product', 'ListProductController@saveTagsProduct');
        Route::post('saveTags/product', 'ListProductController@saveTagsProduct');
        Route::post('title/save', 'ListProductController@titleSave');
        Route::post('change/view', 'ListProductController@changeView');
    });

    Route::group(['prefix' => 'slider'], function () {
        Route::get('', 'SliderController@index')->name('slider.index');
        Route::post('store', 'SliderController@store');
        Route::post('update', 'SliderController@update');
        Route::post('delete', 'SliderController@delete');
        Route::post('upload', 'SliderController@uploadSlider');
        Route::post('display', 'SliderController@displaySlider');
    });

    Route::group(['prefix' => 'banner'], function () {
        Route::get('', 'BannerController@index')->name('banner.index');
        Route::post('store', 'BannerController@store');
        Route::post('update', 'BannerController@update');
        Route::post('upload', 'BannerController@upload');
        Route::post('display', 'BannerController@display');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('', 'OrdersController@index')->name('orders.index');
        Route::get('{id}', 'OrdersController@order')->name('orders.detail');
        Route::get('/success/sms', 'OrdersController@ordersSuccessSms');

    });


    Route::group(['prefix' => 'brand-main'], function () {
        Route::get('', 'BrandMainPageController@index')->name('brand_main_page');
        Route::get('create', 'BrandMainPageController@create')->name('brand_main_page.create');
        Route::post('store', 'BrandMainPageController@store')->name('brand_main_page.store');
        Route::get('edit/{id}', 'BrandMainPageController@edit')->name('brand_main_page.edit');
        Route::post('update', 'BrandMainPageController@update')->name('brand_main_page.update');
        Route::get('delete/{id}', 'BrandMainPageController@drop')->name('brand_main_page.drop');
        Route::post('/update/sort', 'BrandMainPageController@updateSort')->name('brand_main_page');
    });

    Route::group(['prefix' => 'opt/users'], function () {
        Route::get('', 'OptUserController@index');
    });

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    Route::post('/upload/image', 'UploadController@upload')->name('upload.image');
    Route::get('/upload/brand', 'BrandController@upload')->name('upload.brand');
    Route::get('/upload/storage', 'BrandController@storageUpload')->name('upload.storage');

    Route::get('/brand', 'BrandController@index')->name('upload.storage');
    Route::post('/brand/display', 'BrandController@display');
    Route::post('/brand/delivery', 'BrandController@setTimeDeliveryBrand');
    Route::post('/storage/display', 'BrandController@displayStorage');
    Route::post('/storage/delivery', 'BrandController@setTimeDeliveryStorage');
    Route::post('/storage/get', 'BrandController@getStorage');

//Теги
    Route::post('/list/product/saveTags', 'TagsController@saveTags');
    Route::post('/list/product/saveTags/product', 'TagsController@saveTagsProduct');


});
