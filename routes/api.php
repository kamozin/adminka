<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/etimgroup', 'Api\Raec\IndexController@uploadEtimGroup');
Route::get('/etimclass', 'Api\Raec\IndexController@uploadEtimClass');
Route::get('/etimclass/feuters', 'Api\Raec\IndexController@uploadFeuters');

Route::post('/display', 'Api\V1\ProductController@display');


//Route::middleware(['basicAuth'])->group(function () {
    Route::group(['prefix' => 'v1'], function () {
        Route::get('/market/products', 'Api\V1\MarketController@index');
        Route::post('/discounts/create', 'Api\V1\ActionsController@index');
//    Категории
        Route::get('/category', 'CategoryController@index');
        Route::post('/category', 'Api\V1\CategoryController@store');
        Route::post('/category/delete', 'Api\V1\CategoryController@delete');

//    Цены
        Route::group(['prefix' => 'price'], function () {
            Route::post('', 'Api\V1\PriceController@index');

        });

        Route::group(['prefix' => 'stock'], function () {
            Route::post('', 'Api\V1\StockController@index');
        });


        Route::post('/product', 'Api\V1\ProductController@store');


        Route::post('/actions', 'Api\V1\ActionsController@index');
        Route::post('/stopActions', 'Api\V1\ActionsController@stopActions');

        /*
        Route::group(['prefix' => 'product'], function () {

            Route::post('', 'Api\V1\ProductController@update');
            Route::post('display', 'Api\V1\ProductController@display');
            Route::post('image', 'Api\V1\ProductController@image');

        });
*/
        Route::group(['prefix' => 'features'], function () {
            Route::post('', 'Api\V1\FeaturesController@index');
            Route::post('/update', 'Api\V1\FeaturesController@update');
            Route::post('/value/update', 'Api\V1\FeaturesController@updateValue');
        });

        Route::post('/order/status', 'Api\V1\StatusController@index');
        Route::post('/get/status/sms', 'Api\V1\StatusController@getStatus');
    });

//});
