<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FeaturesRetailToEtim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features_retail_to_etim', function (Blueprint $table) {
            $table->string('features_retail_uid')->comment('uid, свойства');
            $table->string('etimId')->comment('Указание на etim свойства');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('etim', function (Blueprint $table) {
            //
        });
    }
}
