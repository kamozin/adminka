<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturesRetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features_retail', function (Blueprint $table) {
            $table->string('uid')->comment('uid присвоенный при создании');
            $table->string('title')->comment('наименование');
            $table->string('external_uid')->nullable()->comment('uid 1C');
            $table->string('unit_features')->nullable()->comment('uid ед. измерения');
            $table->string('type')->nullable()->comment('Тип фильтра');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('features_retail');
    }
}
