<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValueFeatureRetailToCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('value_feature_retail_to_category', function (Blueprint $table) {
            $table->string('category_retail_uid');
            $table->string('feature_retail_uid');
            $table->string('feature_value_retail_uid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('value_feature_retail_to_category');
    }
}
