<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Units extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->string('uid')->comment('uid ед. измерения интернет магазина');
            $table->string('title')->comment('наименование');
            $table->integer('title_abb')->comment('краткое наименование');
            $table->integer('code')->comment('код');
            $table->string('external_id')->comment('id или uid внешний');
            $table->string('system')->comment('система');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
