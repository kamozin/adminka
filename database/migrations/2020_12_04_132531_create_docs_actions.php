<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocsActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('docs_actions', function (Blueprint $table) {
            $table->string('uid');
            $table->string('title');
            $table->string('number_doc');
            $table->string('data_dok');
            $table->string('ot');
            $table->string('do');
            $table->string('cmnt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('docs_actions');
    }
}
