<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtimclass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etimclass', function (Blueprint $table) {
            $table->id();
            $table->string('classId');
            $table->string('groupId');
            $table->string('descriptionEn');
            $table->string('descriptionRu');
            $table->string('etimVersionStart');
            $table->string('etimVersionEnd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etimclass');
    }
}
