<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RetailFeatureValueProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retail_feature_value_product', function (Blueprint $table) {
            $table->string('category_uid')->comment('uid категории');
            $table->string('product_uid')->comment('uid товара');
            $table->string('feature_uid')->comment('uid свойства');
            $table->string('value_uid')->comment('uid значения');
            $table->string('value')->comment('значение');
            $table->string('system')->comment('система откуда пришли данные');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
