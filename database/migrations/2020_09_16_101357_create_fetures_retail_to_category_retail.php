<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeturesRetailToCategoryRetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('features_retail_to_category_retail', function (Blueprint $table) {
            $table->string('features_retail_uid')->comment('uid, свойства');
            $table->string('category_retail_uid')->comment('uid категории к которой привязан');
            $table->integer('display_category_retail')->comment('признак отображения как фильтра в рамках категории');
            $table->integer('sort_category_retail')->comment('сортировка в рамках категории');
            $table->integer('sort_category_retail_product')->comment('сортировка в рамках категории как характеристики');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fetures_retail_to_category_retail');
    }
}
