<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('uid')->comment('uid в ИМ');
            $table->string('external_id')->nullable()->comment('uid или id из внешней системы');
            $table->integer('raec_id')->comment('РАЭК ID');
            $table->integer('code')->nullable()->comment('Код Авенты');
            $table->string('title')->comment('Наименование товара');
            $table->string('slug')->comment('url товара');
            $table->string('sku')->comment('артикул товара');
            $table->string('brand_uid')->comment('бренд товара');
            $table->string('description')->comment('Описание товара');
            $table->string('parent_id')->comment('категория товара');
            $table->string('attribute_bay')->default(0)->comment('признак бухты');
            $table->string('multiplicity')->default(1)->comment('кратность');
            $table->string('unit_id')->comment('Ед. измерения');
            $table->string('liquid_stock')->default(0)->comment('Ликвидный остаток');
            $table->string('display')->comment('видимость товара');
            $table->string('system')->comment('система обмена');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
