<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeatureValueRetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_value_retail', function (Blueprint $table) {
            $table->string('uid')->comment('uid присвоенный при создании');
            $table->string('title')->comment('наименование');
            $table->string('external_uid')->nullable()->comment('uid 1C');
            $table->string('features_retail_uid')->nullable()->comment('uid свойства');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_value_retail');
    }
}
