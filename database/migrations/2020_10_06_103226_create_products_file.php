<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsFile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_file', function (Blueprint $table) {
            $table->string('product_uid');
            $table->string('url');
            $table->integer('size');
            $table->string('mimetype');
            $table->string('type');
            $table->string('certificateValidityFrom');
            $table->string('certificateValidityTo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_file');
    }
}
