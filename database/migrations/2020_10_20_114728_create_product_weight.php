<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductWeight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_weight', function (Blueprint $table) {
            $table->string('product_uid')->nullable()->comment('uid товара');
            $table->decimal('length')->nullable()->comment('длина');
            $table->decimal('width')->nullable()->comment('ширина');
            $table->decimal('height')->nullable()->comment('высота');
            $table->decimal('volume')->nullable()->comment('объем');
            $table->decimal('weight')->nullable()->comment('масса');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_weight');
    }
}
