<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldFeaturesRetailToEtimCategoryRetailUid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('features_retail_to_etim', function (Blueprint $table) {
            $table->string('category_retail_uid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('etim_category_retail_uid', function (Blueprint $table) {
            //
        });
    }
}
