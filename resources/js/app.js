/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');
import Notifications from 'vue-notification'
import VModal from 'vue-js-modal/dist/index.nocss.js'
import 'vue-js-modal/dist/styles.css'

window.Vue = require('vue');

Vue.use(VModal);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Категории интернет магазина
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('category', require('./components/categoryRetail/category.vue').default);
Vue.component('nested', require('./components/categoryRetail/nested.vue').default);
Vue.component('etimrelated', require('./components/categoryRetail/etimRelated.vue').default);
Vue.component('create-actions', require('./components/actions/CreateActions.vue').default);
Vue.component('edit-actions', require('./components/actions/EditAction.vue').default);
Vue.component('banner', require('./components/banners').default);
//Свойства категорий интернет магазина
Vue.component('features', require('./components/FeaturesRetail/features.vue').default);

Vue.component('list', require('./components/list/list').default);
Vue.component('slider', require('./components/slider').default);
Vue.component('brand-display', require('./components/brandRaec/brandDisplay').default);
Vue.component('brand-delivery', require('./components/brandRaec/brandDeliveryTime').default);
Vue.component('storage-brand', require('./components/brandRaec/storageBrand').default);
Vue.component('phone-edit', require('./components/phoneEdit').default);
Vue.component('action-visible', require('./components/actions/visible').default);
Vue.component('article-visible', require('./components/articles/visible').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(Notifications)
Vue.use(require('vue-moment'));


Vue.config.devtools = true;

const app = new Vue({
    el: '#app',
});
