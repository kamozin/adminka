@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Обновить FAQ</h4>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="view-input-fields" class="active">
                                        <div class="row">
                                            <div class="col s12">
                                                <form action="{{route('telegramuser.update')}}" enctype="multipart/form-data"
                                                      method="POST">
                                                    {{csrf_field()}}

                                                    <input type="hidden" name="id" value="{{$data->id}}">
                                                    <div class="input-field col s12">
                                                        <input placeholder="Username" name="user" value="{{$data->user}}" id="title"
                                                               value=""
                                                               type="text" required="required">
                                                        <label for="title" class="active">User</label>
                                                    </div>
                                                    <div class="input-field col s12">
                                                        <select name="shop" id="">
                                                            @foreach($shop as $key=>$s)
                                                                <option @if($data->shop==$key) selected="selected" @endif value="{{$key}}">{{$s}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="input-field col s12">
                                                        <select name="role" id="">
                                                            <option value="admin">Роль</option>
                                                            @foreach($role as $r)
                                                                <option @if($data->role==$r) selected="selected" @endif value="{{$r}}">{{$r}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>



                                                    <div class="file-field input-field  col s12">
                                                        <button
                                                            class="create-user mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan">
                                                            Сохранить
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script src="/ckeeditor4/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editorText', {
            filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token() ])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>
@stop
