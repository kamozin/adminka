@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Пользователи телеграм</h4>
                                            </div>
                                            <div class="col s12 m6 l10">

                                                <p>
                                                    <a href="{{route('telegramuser.create')}}" class="btn waves-effect waves-light green darken-1">Добавить пользователя</a>
                                                </p>
                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table" >
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Пользователь</th>
                                                        <th>Магазин</th>
                                                        <th>Роль</th>
                                                        <th>Доступ</th>
                                                        <th>Удалить</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($data)>0)
                                                        @foreach($data as $d)
                                                            <tr>

                                                                <td><a href="/telegram-user/edit?id={{$d->id}}">{{$d->user}}</a></td>
                                                                <td>{{$shop[$d->shop]}}</td>
                                                                <td>{{$d->role}}</td>
                                                                <td>
                                                                    @if($d->disabled==0)
                                                                        <a href="/telegram-user/display?user_id={{$d->id}}&disabled=1">Запретить</a>
                                                                    @else
                                                                        <a href="/telegram-user/display?user_id={{$d->id}}&disabled=0">Разрешить</a>
                                                                    @endif
                                                                </td>
                                                                <td><a href="/telegram-user/delete?id={{$d->id}}">Удалить</a></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Пользователи отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
