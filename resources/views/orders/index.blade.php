@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">


                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">
                                            <div class="col s12">
                                                <p>
                                                    {{ $data->links() }}
                                                </p>
                                            </div>
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>№</th>
                                                        <th>Покупатель</th>
                                                        <th>Статус заказа</th>
                                                        <th>Статус оплаты</th>
                                                        <th>Сумма</th>
                                                        <th>Дата создания</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($data)>0)
                                                        @foreach($data as $d)
                                                            <tr>
                                                                <td>
                                                                    <a href="/orders/{{$d->id}}">{{$d->id}}</a>
                                                                </td>
                                                                <td>
                                                                    {{$d->name}}
                                                                </td>
                                                                <td>
                                                                    @if($d->status==0)
                                                                        Новый заказ
                                                                    @endif
                                                                    @if($d->status==1)
                                                                            В обработке
                                                                    @endif
                                                                    @if($d->status==2)
                                                                            Готов к получению
                                                                    @endif
                                                                    @if($d->status==3)
                                                                            Завершен
                                                                    @endif
                                                                    @if($d->status==4)
                                                                            Отменён
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if($d->paymeant=='3')
                                                                        @if($d->order_status_sberbank=='2')
                                                                            Оплачен
                                                                        @else
                                                                            Не оплачен
                                                                        @endif

                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    {{$d->total_price}}
                                                                </td>
                                                                <td>{{$d->created_at->format('d.m.Y')}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Заказы
                                                                    отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
