@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">


                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">
                                            <div class="col s12">
                                                <a class="btn btn-primary" title="Уведомить клиента о готовности"
                                                   href="/orders/success/sms?id={{$data->id}}">Заказ готов к
                                                    получению</a>
                                            </div>
                                            <div class="col s12">
                                                <table>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <b>№ заказа</b>
                                                        </td>
                                                        <td>
                                                            {{$data->id}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>Статус заказа</b>
                                                        </td>
                                                        <td>
                                                            @if($data->status==0)
                                                                Новый заказ
                                                            @endif
                                                            @if($data->status==1)
                                                                В обработке
                                                            @endif
                                                            @if($data->status==2)
                                                                Готов к получению
                                                            @endif
                                                            @if($data->status==3)
                                                                Завершен
                                                            @endif
                                                            @if($data->status==4)
                                                                Отменён
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>Имя</b>
                                                        </td>
                                                        <td>
                                                            {{$data->name}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>Почта</b>
                                                        </td>
                                                        <td>
                                                            {{$data->email}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>Телефон</b>
                                                        </td>
                                                        <td>
                                                            {{$data->phone}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <b>Оплата</b>
                                                        </td>
                                                        <td>
                                                            {{($data->paymeant=='1') ? 'При получении' : 'Оплата онлайн' }}
                                                        </td>
                                                    </tr>
                                                    @if($data->paymeant==3)
                                                        <tr>
                                                            <td>
                                                                <b>Статус оплаты</b>
                                                            </td>
                                                            <td>
                                                                @if($data->order_status_sberbank=='2')
                                                                    Оплачен
                                                                @else
                                                                    Не оплачен
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    <tr>
                                                        <td>
                                                            <b>Метод получения заказа</b>
                                                        </td>
                                                        <td>
                                                            {{($data->delivery==1) ? 'Самовывоз' : 'Доставка до адреса' }}
                                                        </td>
                                                    </tr>
                                                    @if($data->delivery==1)
                                                        <tr>
                                                            <td><b>Точка самовывоза</b></td>
                                                            <td>{{$data->point}}</td>
                                                        </tr>
                                                    @endif
                                                    <tr>
                                                        <td>
                                                            <b>Кол-во товаров</b>
                                                        </td>
                                                        <td>
                                                            {{sizeof($ordersProduct)}}
                                                        </td>
                                                    </tr>
                                                    @if(!empty($data->address))
                                                        <tr>
                                                            <td>
                                                                <b>Адрес</b>
                                                            </td>
                                                            <td>
                                                                {{$data->address}}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    <tr>
                                                        <td>
                                                            <b>Комментарий</b>
                                                        </td>
                                                        <td>
                                                            {{$data->cmnt}}
                                                        </td>
                                                    </tr>

                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col s12">
                                                @if($data->order_status_sberbank!='2' && $data->status<3)
                                                    <form method="POST" action="/orders/update/payment">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="id" value="{{$data->id}}">
                                                        <div class="input-field col s12">
                                                            <select name="paymeant" id="">
                                                                <option @if($data->paymeant=='1') selected @endif value="1">Оплата при получении</option>
                                                                <option @if($data->paymeant=='3') selected @endif value="3">Оплата онлайн</option>
                                                            </select>
                                                        </div>
                                                        <div class="input-field col s4">
                                                            <button class="btn btn-success">Изменить способ оплаты</button>
                                                        </div>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col s12">
                                              @if($data->paymeant=='3' && ($data->status==1 || $data->status==2))
                                                    <button class="btn btn-success">Отправить клиенту письмо на оплату</button>
                                                  @endif
                                            </div>
                                        </div>

                                        <br>
                                        <h5>Товары</h5>
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>

                                                        <th>Наименование</th>
                                                        <th>Цена</th>
                                                        <th>Кол-во</th>
                                                        <th>Итого</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($ordersProduct)>0)
                                                        @foreach($ordersProduct as $d)
                                                            <tr>
                                                                <td>
                                                                    {{$d->product->title}}
                                                                </td>
                                                                <td>
                                                                    {{$d->price_product}}
                                                                </td>
                                                                <td>
                                                                    {{$d->count_product}}
                                                                </td>
                                                                <td>
                                                                    {{$d->total_price_product}}
                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Заказы
                                                                    отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>
@stop
