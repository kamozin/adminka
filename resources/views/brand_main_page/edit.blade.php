@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Редактировать бренд</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <form class="form" method="POST" action="{{route('brand_main_page.update')}}"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$data->id}}">
                                        <div class="row">
                                            <div class="col s12">
                                                <div class="input-field col s12">
                                                    <input name="name" id="first_name" type="text" value="{{$data->name}}" class="validate">
                                                    <label for="first_name">Наименование</label>
                                                </div>

                                                <div class=" input-field col s12">
                                                    <div class="file-field input-field">
                                                        <div class="btn">
                                                            <span>Главное фото</span>
                                                            <input name="image" type="file">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" type="text">
                                                        </div>
                                                    </div>
                                                    <a href="{{$data->image}}">Ссылка на фото</a>
                                                </div>

                                                <div class="input-field col s4">
                                                    <button class="btn btn-success">Сохранить</button>
                                                </div>

                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

