@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Бренды на главной</h4>
                                            </div>
                                            <div class="col s12 m6 l10">
                                                <p>
                                                    <a href="{{route('brand_main_page.create')}}" class="btn waves-effect waves-light green darken-1">Добавить бренд</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="view-borderless-table" >
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Наименование</th>
                                                        <th>Ссылка на фото</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($data)>0)
                                                        @foreach($data as $d)
                                                            <tr>
                                                                <td><a href="{{route('brand_main_page.edit', ['id'=>$d->id])}}">{{$d->name}}</a></td>
                                                                <td><a target="_blank" href="{{$d->image}}">Фото бренда</a></td>
                                                                <td><a href="{{route('brand_main_page.drop', ['id'=>$d->id])}}">Удалить</a></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Бренды отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
