@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Статьи</h4>
                                            </div>
                                            <div class="col s12 m6 l10">

                                                <p>
                                                    <a href="{{route('articles.create')}}" class="btn waves-effect waves-light green darken-1">Добавить статью</a>
                                                </p>
                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table" >
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Наименование</th>
                                                        <th>Статус</th>
                                                        <th>Дата</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($articles)>0)
                                                        @foreach($articles as $d)
                                                            <tr>
                                                                <td><a href="{{route('articles.edit', ['id'=>$d->id])}}">{{$d->title}}</a></td>
                                                                <td>{{($d->display==1) ? 'Вкл' : 'Выкл'}}</td>
                                                                <td><article-visible :action="{{json_encode($d)}}"></article-visible></td>
                                                                <td>{{$d->dt}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Статьи отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
