@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">

                                                <h4 class="card-title">Свойства категории
                                                    - {{$data->title}}</h4>

                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">


                                            <div class="col s12">
                                                <features :features="{{$data}}"></features>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
