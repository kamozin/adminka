@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">

                                                    <h4 class="card-title">Свойства категории
                                                        - {{$data->title}}</h4>

                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">


                                            <div class="col s12">
                                                <features :features="{{$data}}" :options="{{json_encode($featuresArray)}}"></features>
{{--                                                <table>--}}
{{--                                                    <thead>--}}
{{--                                                    <tr>--}}
{{--                                                        <th>Наименование</th>--}}
{{--                                                        <th>Тип фильтрации</th>--}}
{{--                                                        <th>Вкл / Выкл свойства</th>--}}
{{--                                                        <th>Использовать как фильтр</th>--}}
{{--                                                        <th>Количество значений свойства</th>--}}
{{--                                                        <th>Дата создания</th>--}}
{{--                                                    </tr>--}}
{{--                                                    </thead>--}}
{{--                                                    <tbody>--}}
{{--                                                    @if(sizeof($data->features)>0)--}}
{{--                                                        @foreach($data->features as $d)--}}
{{--                                                            <tr>--}}

{{--                                                                <td>--}}
{{--                                                                    @if(empty($d->title))--}}
{{--                                                                        <a href="">Не заполнено</a>--}}
{{--                                                                    @else--}}
{{--                                                                        <a href="/features/show/{{$d->uid}}">{{$d->title}}</a>--}}
{{--                                                                    @endif--}}
{{--                                                                </td>--}}
{{--                                                                <td>--}}
{{--                                                                    {{sizeof($d->valueCategory)}}--}}
{{--                                                                </td>--}}
{{--                                                                <td>{{$d->created_at->format('d.m.Y')}}</td>--}}

{{--                                                            </tr>--}}
{{--                                                        @endforeach--}}
{{--                                                    @else--}}
{{--                                                        <tr>--}}
{{--                                                            <td style="text-align: center;" colspan="3"><b>Категории--}}
{{--                                                                    отсутствуют</b></td>--}}
{{--                                                        </tr>--}}
{{--                                                    @endif--}}
{{--                                                    </tbody>--}}
{{--                                                </table>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
