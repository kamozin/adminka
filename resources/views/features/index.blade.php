@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4>Свойства</h4>

                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">

                                            <div class="col s12">
                                                <p>
                                                    {{ $data->links() }}
                                                </p>
                                            </div>
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Наименование</th>
                                                        <th>Колличество категорий</th>
                                                        <th>Значения</th>
                                                        <th>Дата создания</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($data)>0)
                                                        @foreach($data as $d)
                                                            <tr>

                                                                <td>
                                                                    @if(empty($d->title))
                                                                        <a href="">Не заполнено</a>
                                                                    @else
                                                                        <a href="/features/show/{{$d->uid}}">{{$d->title}}</a>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    {{sizeof($d->category)}}
                                                                </td>
                                                                <td>
                                                                    {{sizeof($d->value)}}
                                                                </td>
                                                                <td>{{$d->created_at->format('d.m.Y')}}</td>

                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Категории
                                                                    отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>


                                            <div class="col s12">
                                                <p>
                                                    {{ $data->links() }}
                                                </p>
                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
