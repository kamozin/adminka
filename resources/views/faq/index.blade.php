@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">FAQ</h4>
                                            </div>
                                            <div class="col s12 m6 l10">

                                                <p>
                                                    <a href="{{route('faq.create')}}" class="btn waves-effect waves-light green darken-1">Добавить FAQ</a>
                                                </p>
                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table" >
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Вопрос</th>
                                                        <th>Дата создания</th>
                                                        <th>Удалить</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($faq)>0)
                                                        @foreach($faq as $d)
                                                            <tr>

                                                                <td><a href="{{route('faq.edit', ['id'=>$d->id])}}">{{$d->answer}}</a></td>
                                                                <td>{{$d->created_at->format('d.m.Y')}}</td>
                                                                <td><a href="/faq/drop?id={{$d->id}}">Удалить</a></td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>FAQ отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
