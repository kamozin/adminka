@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Обновить контакт</h4>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="view-input-fields" class="active">
                                        <div class="row">
                                            <div class="col s12">
                                                <form action="{{route('contacts.update')}}" enctype="multipart/form-data"
                                                      method="POST">
                                                    {{csrf_field()}}

                                                    <input type="hidden" name="id" value="{{$contact->id}}">

                                                    <div class="input-field col s12">
                                                        <select name="type">
                                                            <option value="" disabled selected>Выбрать тип
                                                            </option>
                                                            <option @if($contact->type=='Центральный офис') selected @endif value="Центральный офис">Центральный офис</option>
                                                            <option @if($contact->type=='Пункт выдачи') selected @endif value="Пункт выдач">Пункт выдачи</option>
                                                        </select>
                                                        <label>Тип контакта</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Адрес" name="address" id="address"
                                                               value="{{$contact->address}}"
                                                               type="text" required="required">
                                                        <label for="address" class="active">Адрес</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Телефон" name="phone" id="phone"
                                                               value="{{$contact->phone}}"
                                                               type="text" required="required">
                                                        <label for="phone" class="active">Телефон</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="E-mail" name="email" id="email"
                                                               value="{{$contact->email}}"
                                                               type="text" required="required">
                                                        <label for="email" class="active">E-mail</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Время работы" name="time_work" id="time_work"
                                                               value="{{$contact->time_work}}"
                                                               type="text" required="required">
                                                        <label for="time_work" class="active">Время работы</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Время работы" name="time_work_weekends"
                                                               id="time_work_weekends"
                                                               value="{{$contact->time_work_weekends}}"
                                                               type="text" required="required">
                                                        <label for="time_work_weekends" class="active">Время работы в
                                                            выходные</label>
                                                    </div>


                                                    <div class="file-field input-field  col s12">
                                                        <button
                                                            class="create-user mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan">
                                                            Сохранить
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editorDescription');
        CKEDITOR.replace('editorText');
    </script>
    @stop
