@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">

                                                <h4 class="card-title">Категории розницы @if(!empty($parent))
                                                        ({{$parent->title}}) @endif</h4>
                                            </div>
                                            <div class="col s12 m6 l10">

                                                <p>
                                                    @if(!empty($parent))
                                                        <a href="/category-retail"
                                                           class="btn waves-effect waves-light blue-grey darken-1">Вернуться
                                                            назад</a>
                                                    @endif
                                                    <a href="/category-retail/create"
                                                       class="btn waves-effect waves-light green darken-1">Добавить
                                                        категорию</a>
                                                    <a href="/category-retail/struct"
                                                       class="btn waves-effect waves-light green darken-1">Структура
                                                        категорий</a>

                                                </p>
                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">
                                            <div class="col s12">
                                                <category :category="{{json_encode($data)}}"></category>
                                                {{--
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Наименование</th>
                                                        <th>Подкатегорий</th>
                                                        <th>Дата создания</th>
                                                        @if(request()->uid)
                                                            <th>ETIM</th>
                                                            <th>Свойства</th>
                                                        @endif
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($data)>0)
                                                        @foreach($data as $d)
                                                            <tr>

                                                                <td>
                                                                    <a href="/category-retail/edit/{{$d->uid}}">{{$d->title}}</a>
                                                                </td>
                                                                <td>
                                                                    <a href="/category-retail?uid={{$d->uid}}">{{sizeof($d->categories)}}</a>
                                                                </td>
                                                                <td>{{$d->created_at->format('d.m.Y')}}</td>
                                                                @if(request()->uid)
                                                                    <td><a href="/category-retail/relation/etim/{{$d->uid}}">{{sizeof($d->etim)}}</a></td>
                                                                    <td><a href="/features?category={{$d->uid}}">{{sizeof($d->features)}}</a></td>

                                                                @endif
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Категории
                                                                    отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                                --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
