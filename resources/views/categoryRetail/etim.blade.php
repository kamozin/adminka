@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">

                            <h4>Присвоение ETIM классов категории - {{$category->title}}</h4>

                        <div class="col s12">
                                <a href="/category-retail?uid={{$category->parent_id}}"
                                   class="btn waves-effect waves-light blue-grey darken-1">Вернуться назад</a>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col s3"></div>
                        <etimrelated  category="{{$category->uid}}"  :etim='@json($data)'></etimrelated>
                        <div class="col s3">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
