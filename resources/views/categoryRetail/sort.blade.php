@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        @if(request()->uid)
                            <h4>Сортировка подкаткатегорий категории - {{$parent->title}}</h4>
                        @else
                            <h3>Сортировка первого уровня категорий</h3>
                        @endif
                        <div class="col s12">
                            @if(request()->uid)
                                <a href="/category-retail?uid={{$parent->uid}}"
                                   class="btn waves-effect waves-light blue-grey darken-1">Вернуться назад</a>
                            @else
                                <a href="/category-retail" class="btn waves-effect waves-light blue-grey darken-1">Вернуться
                                    назад</a>
                            @endif


                        </div>
                    </div>
                    <div class="row">

                        <div class="col s3"></div>
                        <category-sort :category='@json($data)'></category-sort>
                        <div class="col s3">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
