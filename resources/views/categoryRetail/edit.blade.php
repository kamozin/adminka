@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Обновить категорию</h4>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="view-input-fields" class="active">
                                        <div class="row">
                                            <div class="col s12">
                                                <form action="/category-retail/update" enctype="multipart/form-data"
                                                      method="POST">
                                                    {{csrf_field()}}


                                                    <input type="hidden" name="uid" value="{{$data->uid}}">

                                                    <div class="input-field col s12">
                                                        <input placeholder="Наименование" name="title" id="title"
                                                               value="{{$data->title}}"
                                                               type="text" required="required">
                                                        <label for="title" class="active">Наименование</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Наименование страницы" name="titleSeo"
                                                               id="titleSeo"
                                                               value="{{isset($data->seo->title) ? $data->seo->title : ''}}"
                                                               type="text" >
                                                        <label for="titleSeo" class="active">Наименование
                                                            страницы</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Описание страницы" name="descriptionSeo"
                                                               id="descriptionSeo"
                                                               value="{{isset($data->seo->description) ? $data->seo->description : ''}}"
                                                               type="text" >
                                                        <label for="descriptionSeo" class="active">Описание
                                                            страницы</label>
                                                    </div>
                                                    <div class="input-field col s12">
                                                        <input placeholder="Ключевые слова страницы" name="keywordsSeo"
                                                               id="keywordsSeo"
                                                               value="{{isset($data->seo->keywords) ? $data->seo->keywords : ''}}"
                                                               type="text" >
                                                        <label for="keywordsSeo" class="active">Ключевые слова
                                                            страницы</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <p>
                                                            <label>
                                                                <input name="display" @if($data->display==1) checked
                                                                       @endif value="1"  type="checkbox"/>
                                                                <span>Включить / Выключить</span>
                                                            </label>
                                                        </p>

                                                    </div>

                                                    <div class="file-field input-field col s12">

                                                        <div class="btn">
                                                            <span>Фото категории</span>
                                                            <input type="file" name="image">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" type="text">
                                                        </div>
                                                    </div>


                                                    <div class="file-field input-field col s12">
                                                        <div class="btn">
                                                            <span>Иконка категории</span>
                                                            <input type="file" name="icon">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" type="text">
                                                        </div>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <button
                                                            class="create-user mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan">
                                                            Сохранить
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editorText');
    </script>
@stop
