@extends('layouts.app')

@section('content')

    <nested :category='@json($data)' ></nested>

@stop
