@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Добавить категорию</h4>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="view-input-fields" class="active">
                                        <div class="row">
                                            <div class="col s12">
                                                <form action="/category-retail" enctype="multipart/form-data"
                                                      method="POST">
                                                    {{csrf_field()}}


                                                    <div class="input-field col s12">
                                                        <input placeholder="Наименование" name="title" id="title"
                                                               value=""
                                                               type="text" required="required">
                                                        <label for="title" class="active">Наименование</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Наименование страницы" name="titleSeo"
                                                               id="titleSeo"
                                                               value=""
                                                               type="text" required="required">
                                                        <label for="titleSeo" class="active">Наименование
                                                            страницы</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <input placeholder="Описание страницы" name="descriptionSeo"
                                                               id="descriptionSeo"
                                                               value=""
                                                               type="text" required="required">
                                                        <label for="descriptionSeo" class="active">Описание
                                                            страницы</label>
                                                    </div>


                                                    <div class="input-field col s12">
                                                        <input placeholder="Ключевые слова страницы" name="keywordsSeo"
                                                               id="keywordsSeo"
                                                               value=""
                                                               type="text" required="required">
                                                        <label for="keywordsSeo" class="active">Ключевые слова
                                                            страницы</label>
                                                    </div>

                                                    <div class="input-field col s12">
                                                        <p>
                                                            <label>
                                                                <input type="checkbox"/>
                                                                <span>Включить / Выключить</span>
                                                            </label>
                                                        </p>
                                                    </div>

                                                    <div class="file-field input-field col s12">
                                                        <div class="btn">
                                                            <span>Фото категории</span>
                                                            <input type="file">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" name="image" type="text">
                                                        </div>
                                                    </div>

                                                    <div class="file-field input-field  col s12">
                                                        <button
                                                            class="create-user mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan">
                                                            Сохранить
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editorText');
    </script>
@stop
