<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-light navbar-full sidenav-active-rounded">
    <div class="brand-sidebar">
        <h1 class="logo-wrapper"><a  style="    padding: 28px 12px;" class="brand-logo darken-1" href="/"><span class="logo-text hide-on-med-and-down">Авента</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
    </div>
    <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
        <li class="bold"><a class="collapsible-body" href="/category-retail" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Категории</span></a>
        </li>
        <li class="bold"><a class="collapsible-body" href="/brand" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Бренды</span></a>
        </li>
        <li class="bold"><a class="collapsible-body" href="/brand-main" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Бренды на главной</span></a>
        </li>
        <li class="bold"><a class="collapsible-body" href="/features" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Свойства</span></a>
        </li>
        <li class="active"><a class="collapsible-body" href="{{route('list.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Списки товаров</span></a>
        </li>
        <li class="active"><a class="collapsible-body" href="{{route('slider.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Слайдеры</span></a>
        </li>
        <li class="active"><a class="collapsible-body" href="{{route('banner.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Баннеры</span></a>
        </li>
        <li class="active"><a class="collapsible-body" href="{{route('actions.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Акции</span></a>
        </li>
        <li class="active"><a class="collapsible-body" href="{{route('news.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Новости</span></a>
        <li class="active"><a class="collapsible-body" href="{{route('articles.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Статьи</span></a>
        </li>
        <li class="active"><a class="collapsible-body" href="{{route('contacts.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Контакты</span></a>
        </li>
        <li class="active"><a class="collapsible-body" href="{{route('faq.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>FAQ</span></a>
        <li class="active"><a class="collapsible-body" href="{{route('orders.index')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Заказы</span></a>
        <li class="active"><a class="collapsible-body" href="{{route('raec.ListProductUpload')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Товары на загрузку</span></a>
        <li class="active"><a class="collapsible-body" href="{{route('raec.indexValues')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Значения на загрузку</span></a>
        <li class="active"><a class="collapsible-body" href="{{route('clients')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Клиенты</span></a>
        <li class="active"><a class="collapsible-body" href="{{route('telegram-user')}}" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Пользователи телеграм</span></a>
    </ul>
    <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
