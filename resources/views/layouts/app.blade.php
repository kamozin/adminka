<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
          content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
    <meta name="keywords"
          content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Dashboard Modern 2 | Materialize - Material Design Admin Template</title>
    <link rel="apple-touch-icon" href="/app-assets/images/favicon/apple-touch-icon-152x152.png">
    <link rel="shortcut icon" type="image/x-icon" href="/app-assets/images/favicon/favicon-32x32.png">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- BEGIN: VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/animate-css/animate.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/chartist-js/chartist.min.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/vendors/chartist-js/chartist-plugin-tooltip.css">
    <!-- END: VENDOR CSS-->
    <!-- BEGIN: Page Level CSS-->
    <link rel="stylesheet" type="text/css"
          href="/app-assets/css/themes/vertical-menu-nav-dark-template/materialize.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/themes/vertical-menu-nav-dark-template/style.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/dashboard-modern.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/login.css">
    <link rel="stylesheet" type="text/css" href="/app-assets/css/pages/intro.css">
    <!-- END: Page Level CSS-->
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/app-assets/css/custom/custom.css">
    <!-- END: Custom CSS-->

    @yield('css')
</head>
<!-- END: Head-->
<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-menu-nav-dark 2-columns  "
      data-open="click" data-menu="vertical-menu-nav-dark" data-col="2-columns">
<div id="app">

@if(\Illuminate\Support\Facades\Auth::user() && session()->get('authGoogle') == "authTrue")
    <!-- BEGIN: Header-->
    @include('layouts.header')
    <!-- END: Header-->
@endif

@if(\Illuminate\Support\Facades\Auth::user() && session()->get('authGoogle') == "authTrue")
    <!-- BEGIN: SideNav-->
    @include('layouts.aside')
    <!-- END: SideNav-->
@endif
<!-- BEGIN: Page Main-->

@yield('content')

<!-- END: Page Main-->
    <!-- BEGIN: Footer-->
    @if(\Illuminate\Support\Facades\Auth::user() && session()->get('authGoogle') == "authTrue")
        @include('layouts.footer')
    @endif
</div>
<!-- END: Footer-->
<script src="/js/app.js"></script>
<!-- BEGIN VENDOR JS-->
<script src="/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<script src="/app-assets/vendors/chartjs/chart.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/chartist-js/chartist.min.js" type="text/javascript"></script>
<script src="/app-assets/vendors/chartist-js/chartist-plugin-tooltip.js" type="text/javascript"></script>
<script src="/app-assets/vendors/chartist-js/chartist-plugin-fill-donut.min.js" type="text/javascript"></script>
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
{{--<script src="/app-assets/js/scripts/dashboard-modern.js" type="text/javascript"></script>--}}
<script src="/app-assets/js/scripts/intro.js" type="text/javascript"></script>
{{--<script src="https://unpkg.com/axios/dist/axios.min.js"></script>--}}


@yield('js')
<!-- END PAGE LEVEL JS-->
</body>
</html>
