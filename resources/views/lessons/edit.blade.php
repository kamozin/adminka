@extends('admin.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Обновить FAQ</h4>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="view-input-fields" class="active">
                                        <div class="row">
                                            <div class="col s12">
                                                <form action="{{route('faq.update')}}" enctype="multipart/form-data"
                                                      method="POST">
                                                    {{csrf_field()}}

                                                    <input type="hidden" value="{{$faq->id}}" name="id">
                                                    <div class="input-field col s12">
                                                        <input placeholder="Наименование" name="answer" id="title"
                                                               value="{{$faq->answer}}"
                                                               type="text" required="required">
                                                        <label for="title" class="active">Вопрос</label>
                                                    </div>
                                                    <div class="input-field col s12">
                                                        <textarea name="question" id="editorText" cols="30" rows="100">
                                                            {!! $faq->question !!}
                                                        </textarea>
                                                    </div>



                                                    <div class="file-field input-field  col s12">
                                                        <button
                                                            class="create-user mb-6 btn waves-effect waves-light gradient-45deg-light-blue-cyan">
                                                            Сохранить
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editorText');
    </script>
    @stop
