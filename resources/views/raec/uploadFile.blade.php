@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Загрузить файл</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <form class="form" method="POST"  action="/raec/upload/file/from/download/products"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col s12">

                                                <div class=" input-field col s12">
                                                    <div class="file-field input-field">
                                                        <div class="btn">
                                                            <span>Файл</span>
                                                            <input name="fileraec" type="file">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" type="text">
                                                        </div>
                                                    </div>
                                                </div>




                                                <div class="input-field col s4">
                                                    <button class="btn btn-success">Загрузить</button>
                                                </div>

                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')


@stop
