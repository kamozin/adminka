
@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="col s12 m6 l10">
                                            <h4 class="card-title">Значения на загрузку</h4>
                                        </div>
                                        <div class="row">
                                            <a class="btn btn-primary" href="/raec/file/upload/values">Добавить значения</a>
                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">
                                            <div class="col s12">
                                                <p>
                                                    {{ $data->links() }}
                                                </p>
                                            </div>
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Артикул</th>
                                                        <th>Статус</th>
                                                        <th>Дата создания</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if(sizeof($data)>0)
                                                        @foreach($data as $d)
                                                            <tr>
                                                                <td>
                                                                    {{   $d->sku}}
                                                                </td>
                                                                <td>
                                                                    {{($d->status==0) ? 'Не загружен' : 'Загружен'}}
                                                                </td>

                                                                <td>{{$d->created_at}}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3"><b>Значения
                                                                    отсутствуют</b></td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
