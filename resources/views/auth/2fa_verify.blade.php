@extends('layouts.app')
@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">

                            <div class="card-body">
                                <br>
                                <br>
                                <br>

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                Введите PIN:<br/><br/>
                                <form class="form-horizontal" action="{{ route('2faVerify') }}" method="POST">
                                    {{ csrf_field() }}
                                    <div
                                        class="form-group{{ $errors->has('one_time_password-code') ? ' has-error' : '' }}">
                                        <label for="one_time_password" class="control-label">PIN CODE</label>
                                        <input id="one_time_password" name="one_time_password"
                                               class="form-control col-md-4"
                                               type="text" required/>
                                    </div>
                                    <button class="btn btn-primary" type="submit">Войти</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
