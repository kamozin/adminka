@extends('layouts.app')
@section('content')
    <div class="login-bg">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div id="login-page" class="row">
                        <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">

                            <div class="card-body">
                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        {{ session('error') }}
                                    </div>
                                @endif
                                @if (session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                @endif

                                @if($data['user']->loginSecurity == null)
                                    <form class="form-horizontal" method="POST"
                                          action="{{ route('generate2faSecret') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">
                                                Generate Secret Key to Enable 2FA
                                            </button>
                                        </div>
                                    </form>
                                @elseif(!$data['user']->loginSecurity->google2fa_enable)
                                        <br>
                                    1. Отсканируйте QR code.
                                    <img style="margin: 12px auto; display: block;" src="{{$data['google2fa_url'] }}" alt="">
                                    <br/><br/>
                                    2. Введите pin код:<br/><br/>
                                    <form class="form-horizontal" method="POST" action="{{ route('enable2fa') }}">
                                        {{ csrf_field() }}
                                        <div class="form-group{{ $errors->has('verify-code') ? ' has-error' : '' }}">
                                            <label for="secret" class="control-label">Ваш код</label>
                                            <input id="secret" type="password" class="form-control col-md-4"
                                                   name="secret" required>
                                            @if ($errors->has('verify-code'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('verify-code') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                        <p></p>
                                        <button type="submit" class="btn btn-primary">
                                           Войти
                                        </button>
                                        <br>
                                        <br>
                                    </form>
                                @elseif($data['user']->loginSecurity->google2fa_enable)
                                    <div class="alert alert-success">
                                        2FA is currently <strong>enabled</strong> on your account.
                                    </div>
                                    <p>If you are looking to disable Two Factor Authentication. Please confirm your
                                        password and Click Disable 2FA Button.</p>
                                    <form class="form-horizontal" method="POST" action="{{ route('disable2fa') }}">
                                        {{ csrf_field() }}
                                        <div
                                            class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                            <label for="change-password" class="control-label">Current Password</label>
                                            <input id="current-password" type="password" class="form-control col-md-4"
                                                   name="current-password" required>
                                            @if ($errors->has('current-password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                        <button type="submit" class="btn btn-primary ">Disable 2FA</button>

                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
