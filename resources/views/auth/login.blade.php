@extends('layouts.app')



@section('content')

    <div class="login-bg">
        <div class="row">
            <div class="col s12">
                <div class="container"><div id="login-page" class="row">
                        <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                            <form method="POST" class="login-form" action="{{ route('login') }}">
                                @csrf
                                <div class="row">
                                    <div class="input-field col s12">
                                        <h5 class="ml-4">Вход</h5>
                                    </div>
                                </div>
                                <div class="row margin">
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix pt-2">person_outline</i>
                                        <input id="username"  name="email"    value="{{ old('email') }}" type="text">
                                        <label for="username" class="center-align">E-mail</label>
                                    </div>
                                </div>
                                <div class="row margin">
                                    <div class="input-field col s12">
                                        <i class="material-icons prefix pt-2">lock_outline</i>
                                        <input id="password" name="password"  required autocomplete="current-password" type="password">
                                        <label for="password">Пароль</label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="input-field col s12">
                                        <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">Login</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="content-overlay"></div>
            </div>
        </div>
    </div>



@endsection
