@extends('layouts.app')

@section('content')
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Добавить акцию</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <form class="form" method="POST" action="/actions/store"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="col s12">
                                                <div class="input-field col s12">
                                                    <input name="title" id="first_name" type="text" class="validate">
                                                    <label for="first_name">Наименование</label>
                                                </div>
                                                <div class="input-field col s12">
                                                    <input name="erid" id="first_name" type="text" class="validate">
                                                    <label for="first_name">Erid</label>
                                                </div>

                                                <div class="input-field col s6">
                                                    <input type="date" name="dt_start" id="dt_start" class="validate">
                                                    <label for="dt_start">Дата начала</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input type="date" name="dt_end" id="dt_end" class="validate">
                                                    <label for="dt_end">Дата окончания</label>
                                                </div>


                                                <div class=" input-field col s12">
                                                    <div class="file-field input-field">
                                                        <div class="btn">
                                                            <span>Главное фото</span>
                                                            <input name="imageMain" type="file">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" input-field col s12">
                                                    <div class="file-field input-field">
                                                        <div class="btn">
                                                            <span>Условия акции</span>
                                                            <input name="promotionalSupplement" type="file">
                                                        </div>
                                                        <div class="file-path-wrapper">
                                                            <input class="file-path validate" type="text">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="input-field col s12">
                                                <textarea id="textarea1" name="description"
                                                          class="materialize-textarea">Краткое описание</textarea>
                                                </div>
                                                <div class="input-field col s12">
                                                <textarea id="textarea2" name="text"
                                                          class="materialize-textarea">Текст</textarea>
                                                </div>
                                                <div class="input-field col s12">
                                                    <select name="listId" id="">
                                                        <option value="0">Выбрать лист товаров</option>
                                                        @foreach($list as $l)
                                                        <option value="{{$l->id}}">{{$l->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="input-field col s12">
                                                    <p>
                                                        <label>
                                                            <input name="display" value="1" type="checkbox"/>
                                                            <span>Включить / Выключить</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="input-field col s12">
                                                    <p>
                                                        <label>
                                                            <input name="main_page" value="1" type="checkbox"/>
                                                            <span>Выводить на главную</span>
                                                        </label>
                                                    </p>
                                                </div>


                                                <div class="input-field col s4">
                                                    <button class="btn btn-success">Сохранить</button>
                                                </div>

                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('js')
    <script src="/ckeeditor4/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('textarea1', {});
        CKEDITOR.replace('textarea2', {
            filebrowserUploadUrl: "{{route('upload.image', ['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form'
        });
    </script>

@stop
