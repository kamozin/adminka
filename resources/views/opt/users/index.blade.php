@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Оптовые пользователи</h4>
                                            </div>
                                            <div class="col s12 m6 l10">


                                            </div>


                                        </div>
                                    </div>
                                    <div id="view-borderless-table">
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Имя</th>
                                                        <th>Email</th>
                                                        <th>Дата и время создания</th>
                                                        <th>Отправить</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach($users as $u)

                                                            <tr>

                                                                <td>{{$u->name}}</td>
                                                                <td>{{$u->email}}</td>
                                                                <td>{{$u->created_at}}</td>
                                                                <td><a href="https://api-test.a-electro.pro/send/email/reg?email={{$u->email}}">Отправить почту</a></td>
                                                            </tr>

                                                    @endforeach

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
