@extends('layouts.app')

@section('content')

    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div id="input-fields" class="card card-tabs">
                                <div class="card-content">
                                    <div class="card-title">
                                        <div class="row">
                                            <div class="col s12 m6 l10">
                                                <h4 class="card-title">Клиенты</h4>
                                            </div>
                                            <div class="col s12 m6 l10">
                                            </div>
                                        </div>
                                    </div>

                                    <form method="GET" action="">
                                        <input type="email" name="email" class="form-control" value="" placeholder="Email">
                                        <button type="submit" class="btn btn-primary">Найти</button>
                                        <a href="{{route('clients')}}" class="btn btn-primary">сброс</a>
                                    </form>


                                    {{ $data->links() }}
                                    <div id="view-borderless-table" >
                                        <div class="row">
                                            <div class="col s12">
                                                <table>
                                                    <thead>
                                                    <tr>
                                                        <th>Имя</th>
                                                        <th>Телефон</th>
                                                        <th>E-mail</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($data as $d)
                                                            <tr>
                                                                <td>{{$d->name}}</td>
                                                                <td><phone-edit :user="{{json_encode($d)}}"></phone-edit></td>
                                                                <td>{{$d->email}}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    {{ $data->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop

@section('js')
    <script src="/app-assets/js/my.js"></script>

@stop
