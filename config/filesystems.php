<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],
        'aventa' => [
            'driver' => 'ftp',
//            'root' => 'DIR_PATH_TO_WHERE_IMAGE_STORE',
            'host' => '5.188.28.143',
            'username' => 'test',
            'password' => 'qK2xG0nN0w',
            'visibility' => 'public',
        ],
        'finder' => [
            'driver' => 'ftp',
//            'root' => 'DIR_PATH_TO_WHERE_IMAGE_STORE',
            'host' => 'ftp://ftp.datamaster.netcomponents.com',
            'username' => '1205642',
            'password' => '49013949',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => 'cd30299',
            'secret' => 'uy-uj8lxzxjtetk5bbdqrk3r0bkiqg0v',
            'region' => 'ru-1',
            'bucket' => 'cd30299-a60133e7-3d1f-42b8-bcb5-47bcb29698dc',
            'url' => 'https://s3.timeweb.com/cd30299-a60133e7-3d1f-42b8-bcb5-47bcb29698dc',
            'endpoint' => 'https://s3.timeweb.com',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
    ],

];
