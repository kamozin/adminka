<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;

class Units extends Model
{
    use Uuid;

    protected $table = 'units';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'uid';

    protected $fillable = [
        'title',
        'title_abb',
        'code',
        'external_id',
        'system'
    ];

    public function getUnitCode($code) {
        return self::where('code', $code)->first();
    }


}
