<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandRaec extends Model
{
    protected $table = 'raec_brand';

    protected $fillable = [
        'brand_id',
        'title',
        'description',
        'uidBrand',
        'deliveryTime',
        'display'
    ];

    public function storageBrand () {
        return $this->hasMany(StorageRaec::class, 'brand_id', 'brand_id');
    }
}
