<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'external_catalog';

    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'uid';

    protected $fillable = [
        'uid',
        'title',
        'parent_id',
        'system',
        'parent',
        'children'
    ];

    public function categoryRetail () {
        return $this->belongsToMany(CategoryRetail::class, 'catalog_to_retail', 'catalog_external_uid', 'category_retail_uid');
    }
}
