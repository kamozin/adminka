<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoProduct extends Model
{
    protected $table = 'product_seo';

    protected $fillable = [
        'product_uid',
        'title',
        'description',
        'keywords'
    ];


}
