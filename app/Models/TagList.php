<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagList extends Model
{
    protected $table='tags_list';

    protected $fillable = ['tag', 'list_id'];
}
