<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StorageRaec extends Model
{
    protected $table = 'raec_storage';

    protected $fillable = [
        'storage_id',
        'brand_id',
        'title',
        'display',
    ];
}
