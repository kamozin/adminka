<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
        'type',
        'address',
        'phone',
        'email',
        'time_work',
        'time_work_weekends'
    ];
}
