<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Features extends Model
{
    protected $table = 'features';

    protected $fillable = [
        'featureId',
        'descriptionRu',
        'descriptionEn',
        'required',
        'gold',
        'type',
        'unit_features_id'
    ];

    public static function createFeature ($data) {
        self::create($data);
    }
}
