<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RetailFeatureValueProduct extends Model
{
    protected $table = 'retail_feature_value_product';

    protected $fillable = [
        'category_uid',
        'product_uid',
        'feature_uid',
        'value_uid',
        'value',
        'system'
    ];
}
