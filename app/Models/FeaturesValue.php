<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturesValue extends Model
{

    protected $table='features_value';
    protected $fillable = [
        'valueId',
        'descriptionRu',
        'descriptionEn',
        'feature_id'
    ];
}
