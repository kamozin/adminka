<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandSeriesRaec extends Model
{
    protected $table = 'raec_brand_series';

    protected $fillable = [
        'seriaMarketingId',
        'brand_id',
        'title',
        'uidSeriaMarketing',
    ];
}
