<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EtimClass extends Model
{
    protected $table = 'etimclass';

    protected $fillable = [
        'classId',
        'groupId',
        'descriptionEn',
        'descriptionRu',
        'etimVersionStart',
        'etimVersionEnd',
        'category_retail_uid'
    ];
}
