<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListProductToProduct extends Model
{
    protected $table='list_product_to_product';

    protected $fillable=[
        "list_id",
        "product_uid",
        "skidka"
    ];


    public function product(){
        return $this->belongsTo(Product::class,  'product_uid', 'uid');
    }


}
