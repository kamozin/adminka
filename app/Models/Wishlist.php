<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Wishlist extends Model
{
    protected $table = 'whishlist';

    protected $fillable = [
        'product_uid',
        'user_id',
        'session_id'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function count()
    {
        if (Auth::user()) {
            $count = Wishlist::where('session_id', Auth::user()->id)->count();
        } else {
            $count = Wishlist::where('session_id', session()->getId())->count();
        }

        return $count;
    }
}
