<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryRetailToEtim extends Model
{
    protected $table = 'retail_to_etim';

    protected $fillable = [
        'category_retail_uid',
        'etimclass_classId'
    ];


}
