<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturesRetailToEtim extends Model
{
    protected $table = 'features_retail_to_etim';

    protected $fillable = [
        'features_retail_uid',
        'etimId',
        'category_retail_uid',
        'classId'
    ];


    public function feature () {
        return $this->hasOne(Features::class, 'featureId', 'etimId');
    }
}
