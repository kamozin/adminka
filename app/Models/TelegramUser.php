<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelegramUser extends Model
{
    protected $table = 'telegram_user';

    protected $fillable=[
        "user",
        "shop",
        "disabled",
        "role"
    ];
}
