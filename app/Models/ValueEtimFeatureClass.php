<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValueEtimFeatureClass extends Model
{
    protected $table = 'value_etim_feature_class';

    protected $fillable = [
        'valueId',
        'featureId',
        'classId'
    ];
}
