<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListProductTags extends Model
{
    protected $table = 'list_product_tags';

    protected $fillable = [
        'product_uid',
        'tag',
        'tag_id',
        'list_id',
    ];
}
