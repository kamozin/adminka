<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeStorage extends Model
{
    protected $table = 'storage';

    protected $fillable = [
        "title",
        "type",
    ];
}
