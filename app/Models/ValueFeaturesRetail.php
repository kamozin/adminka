<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;

class ValueFeaturesRetail extends Model
{


    Use Uuid;

    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'uid';


    protected $table = 'feature_value_retail';

    protected $fillable = [
        'title',
        'external_uid',
        'features_retail_uid',
    ];
}
