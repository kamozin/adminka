<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitFeatures extends Model
{

    protected $keyType = 'string';
    public $incrementing = false;


    protected $table = 'unit_features';

    protected $fillable = [
        "id",
        'descriptionRu',
        'descriptionEn'
    ];
}
