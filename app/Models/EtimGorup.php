<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EtimGorup extends Model
{
    protected $table = 'etimgroup';

    protected $fillable = [
        'groupId',
        'descriptionEn',
        'descriptionRu'
    ];

}
