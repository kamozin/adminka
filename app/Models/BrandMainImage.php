<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandMainImage extends Model
{
    protected $table = 'brand_main_page';

    protected $fillable = [
        'name',
        'image',
        'sort',
        'visible'
    ];
}
