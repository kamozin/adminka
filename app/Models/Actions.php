<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Actions extends Model
{

    use Sluggable;

    protected $table = 'actions';

    protected $fillable = [
        "title",
        "slug",
        "dt",
        "description",
        "text",
        "image",
        "created_at",
        "updated_at",
        "display",
        "main_page",
        "list_id",
        "dt_start",
        "dt_end",
        "uid_dok",
        "number_dok",
        "data_dok",
        "tip_dok",
        "skidka",
        "tip_cen",
        "promotionalSupplement",
        "erid"
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
