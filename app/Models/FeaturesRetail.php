<?php

namespace App\Models;

use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;

class FeaturesRetail extends Model
{

    Use Uuid;

    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'uid';

    protected $table = 'features_retail';

    protected $fillable = [
        'title',
        'external_uid',
        'unit_features',
        'type'
    ];

    public function category () {
        return $this->belongsToMany(CategoryRetail::class, 'features_retail_to_category_retail')
            ->withPivot(
                'features_retail_uid',
                'category_retail_uid',
                'display_category_retail',
                'sort_category_retail',
                'sort_category_retail_product',
                'gold',
                'queueFeature'
            );
    }

    public function value () {
        return $this->hasMany(ValueFeaturesRetail::class);
    }

    public function valueCategory () {
        return $this->hasMany(ValueFeatureToCategory::class, 'feature_retail_uid', 'uid');
    }

    public function relationEtim ($category) {
        return $this->hasMany(FeaturesRetailToEtim::class,  'features_retail_uid', 'uid')->with('feature')->where('category_retail_uid', $category);
    }


}
