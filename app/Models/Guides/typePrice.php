<?php

namespace App\Models\Guides;

use Illuminate\Database\Eloquent\Model;

class typePrice extends Model
{
    protected $table = 'type_price';

    protected $fillable = [
        'title',
        'type'
    ];


}
