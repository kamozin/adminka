<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banners_main';

    protected $fillable = [
        'name',
        'file',
        'type',
        'url'
    ];
}
