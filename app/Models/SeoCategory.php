<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeoCategory extends Model
{
    protected $table = 'category_retail_seo';

    protected $fillable = [
        'title',
        'description',
        'keywords',
        'category_retail_uid'
    ];

}
