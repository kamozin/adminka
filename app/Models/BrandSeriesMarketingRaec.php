<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandSeriesMarketingRaec extends Model
{
    protected $table = 'raec_brand_series_marketing';

    protected $fillable = [
        'seriaMarketingId',
        'brand_id',
        'title',
        'uidSeriaMarketing',
    ];
}
