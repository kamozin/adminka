<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StickerProduct extends Model
{
    protected $table = 'icons_product';

    protected $fillable = [
        'icon',
        'source',
        'product_uid',
        'source_id'
    ];


    public function store ($data) {
        $model=new self();
        $model->icon=$data['icon'];
        $model->source=$data['source'];
        $model->product_uid=$data['product_uid'];
        $model->source_id=$data['source_id'];
        $model->save();
    }

    public function deleteIcons ($product_uid, $source_id) {
        self::where('source_id', $source_id)
            ->where('product_uid', $product_uid)
            ->delete();
    }
}
