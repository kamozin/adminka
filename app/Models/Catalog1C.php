<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog1C extends Model
{
    protected $table = 'external_catalog';

    protected $fillable = [
        'title',
        'uid',
        'parent_id',
        'system',
        'parent',
        'children'
    ];


}
