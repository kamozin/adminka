<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use Sluggable;

    protected $table = 'news';

    protected $fillable = [
        'title',
        'slug',
        'description',
        'text',
        'image',
        'display',
        'list_id',
        "erid"
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
