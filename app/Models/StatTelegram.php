<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatTelegram extends Model
{
    protected $table = 'stat_telegram';

    protected $fillable = [
      'user_id',
      'text'
    ];
}
