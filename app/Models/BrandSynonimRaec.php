<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandSynonimRaec extends Model
{
    protected $table = 'raec_brand_synonyms';

    protected $fillable = [
        'brand_id',
        'uidBrand',
        'title',
    ];
}
