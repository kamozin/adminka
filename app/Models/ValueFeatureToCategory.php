<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ValueFeatureToCategory extends Model
{

    protected $table = 'value_feature_retail_to_category';

    protected $fillable = [
        'category_retail_uid',
        'feature_retail_uid',
        'feature_value_retail_uid'
    ];

    public function value () {
        return $this->hasOne(ValueFeaturesRetail::class, 'uid', 'feature_value_retail_uid');
    }
}
