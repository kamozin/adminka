<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileType extends Model
{
    protected $table = 'files_type';

    protected $fillable = [
        'type',
        'title'
    ];

    public static function type ($type) {
        $result=self::where('type', $type->id)->first();
        if(empty($result)){
            $m=new self();
            $m->type=$type->id;
            $m->title=$type->name;
            $m->save();
        }

        return $type->id;
    }
}
