<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ListProduct extends Model
{
    protected $table = 'list_product';

    protected $fillable = [
        "title",
        "display",
        "not",
        "sort",
        "view"
    ];

    public function productList()
    {
        $this->belongsToMany(ListProductToProduct::class, 'list_id', 'id');
    }

    public function product()
    {
        return $this->belongsToMany(Product::class, 'list_product_to_product', 'list_id', 'product_uid');
    }
}
