<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UploadProductsRaec extends Model
{
    protected $table = 'upload_products_raec';



    protected $fillable = [
        'sku',
        'seria',
        'status',
        'product_uid'
    ];

    public function getSkuAll ($status) {
        return self::where('status', $status)->get();
    }

    public function getSkuFromLoad () {
        return self::where('status', 0)->get();
    }

    public function updateRow($sku, $array){
        self::where('sku', $sku)->update($array);
    }

    public function saveUidProduct ($sku, $uid) {
        self::where('sku', $sku)
            ->update(
                [
                    "product_uid"=>$uid,
                    "status"=>1
                ]
            );
    }
}
