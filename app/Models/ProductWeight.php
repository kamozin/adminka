<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductWeight extends Model
{
    protected $table = 'product_weight';


    protected $fillable = [
        'product_uid',
        'length',
        'width',
        'height',
        'volume',
        'weight',
    ];
}
