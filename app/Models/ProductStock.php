<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    protected $table = 'stock_products';

    protected $fillable = [
        'storage_id',
        'stock',
        'product_uid',
        'available'
    ];

}
