<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use Sluggable;

    protected $table = 'articles';

    protected $fillable = [
        "title",
        "slug",
        "dt",
        "title_seo",
        "description_seo",
        "keywords_seo",
        "description",
        "text",
        "image",
        "created_at",
        "updated_at",
        "display",
        "main_page",
        "category_uid",
        'erid'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
