<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    protected $table = 'order_products';

    protected $fillable = [
        'order_id',
        'uid_product',
        'price_product',
        'count_product',
        'total_price_product'
    ];

    public function product () {
        return $this->belongsTo(Product::class,  'uid_product', 'uid');
    }
}
