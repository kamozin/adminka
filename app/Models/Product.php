<?php

namespace App\Models;


use App\Jobs\PriceProduct;
use App\Jobs\StockProduct;
use Cviebrock\EloquentSluggable\Sluggable;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Sluggable;
    use Uuid;

    protected $table = 'products';

    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'uid';


    protected $fillable = [
        'uid',
        'external_id',
        'raec_id',
        'code',
        'title',
        'slug',
        'sku',
        'brand_uid',
        'description',
        'parent_id',
        'attribute_bay',
        'multiplicity',
        'unit_id',
        'liquid_stock',
        'display',
        'system',
        'modelImage',
        'video',
        'imgLoad'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getProductRaecId ($raecId) {
        return self::where('raec_id', $raecId)->first();
    }

    public function burovaStock () {
        return $this->hasOne(ProductStock::class, 'product_uid', 'uid')->where('storage_id', 1);
    }
    public function priceShop()
    {
        return $this->hasOne(ProductPrice::class, 'products_uid', 'uid')->where('type_price', 6);
    }

    public function stock()
    {
        return $this->hasMany(ProductStock::class, 'product_uid', 'uid')->orderBy('storage_id', 'asc');
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'product_uid', 'uid');
    }
}
