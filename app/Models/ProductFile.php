<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFile extends Model
{
    protected $table = 'products_file';

    protected $fillable = [
        'product_uid',
        'url',
        'size',
        'mimetype',
        'type',
        'certificateValidityFrom',
        'certificateValidityTo'
    ];

}
