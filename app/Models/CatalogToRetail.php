<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatalogToRetail extends Model
{
    protected $table='catalog_to_retail';

    protected $fillable=[
        'category_retail_uid',
        'catalog_external_uid'
    ];
}
