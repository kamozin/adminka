<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'products_image';

    protected $fillable = [
        'name',
        'main',
        'product_uid',
        'display',
        'system',
        'url_raec',
        'trimStatus'
    ];

    public function product()
    {
        return $this->hasMany(Product::class, 'product_uid', 'uid');
    }
}
