<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    protected $fillable = [
        'user_id',
        'name',
        'email',
        'phone',
        'paymeant',
        'delivery',
        'point',
        'address',
        'count_product',
        'total_price',
        'uid',
        'status',
        'cmnt'
    ];
}
