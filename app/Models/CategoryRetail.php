<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class CategoryRetail extends Model
{
    use Sluggable;
    use Uuid;


    protected $table = 'category_retail';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'uid';


    protected $fillable = [
        'title',
        'slug',
        'uid',
        'parent_id',
        'display',
        'sort',
        'image',
        'icon'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function seo()
    {
        return $this->belongsTo('App\Models\SeoCategory', 'uid', 'category_retail_uid');
    }

    public function categories()
    {
        return $this->hasMany(CategoryRetail::class, 'parent_id', 'uid')->orderBy('sort', 'asc');
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class, 'uid', 'parent_id')->with('categories')->where('display', 1);
    }


    public function parentCategories()
    {
        return $this->hasMany(Category::class, 'ids', 'category_id')->where('display', 1);
    }

    public function etim()
    {
        return $this->hasMany(EtimClass::class, 'category_retail_uid', 'uid');
    }

    public function features()
    {
        return $this->belongsToMany(FeaturesRetail::class, 'features_retail_to_category_retail')
            ->withPivot(
                'features_retail_uid',
                'category_retail_uid',
                'display_category_retail',
                'sort_category_retail',
                'sort_category_retail_product',
                'useFilter',
                'gold',
                'queueFeature'
            );
    }
}
