<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturesToEtimClass extends Model
{
    protected $table='feature_etim';

    protected $fillable=[
        'featureId',
        'classId'
    ];


    public function featureInfo () {
        return $this->belongsTo(Features::class, 'featureId', 'featureId');
    }
}
