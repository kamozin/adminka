<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturesRetailToCategoryRetail extends Model
{
    protected $table = 'features_retail_to_category_retail';

    protected $fillable = [
        'features_retail_uid',
        'category_retail_uid',
        'display_category_retail',
        'useFilter',
        'sort_category_retail',
        'sort_category_retail_product',
        'gold',
        'queueFeature'
    ];
}
