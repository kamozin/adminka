<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $table = 'products_price';

    protected $fillable = [
        'products_uid',
        'price',
        'type_price',
        'dt_end'
    ];
}
