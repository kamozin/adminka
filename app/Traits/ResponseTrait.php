<?php

namespace App\Traits;


use App\Models\EtimClass;
use App\Models\Features;
use Illuminate\Support\Facades\Redis;
use Ixudra\Curl\Facades\Curl;
use App\Models\EtimGorup;

trait ResponseTrait
{
    public function __construct()
    {

    }

    public function generateResponse ($success, $errors) {
        return ['success'=>$success, 'errors'=>$errors];
    }

    public function getError ($uid, $message) {
        return ['uid'=>$uid, 'error'=>$message];
    }

}
