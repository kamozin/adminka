<?php

namespace App\Traits;


use App\Jobs\PriceProduct;
use App\Models\EtimClass;
use App\Models\Features;
use App\Models\Guides\typePrice;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductStock;
use App\Models\ProductWeight;
use App\Models\SeoProduct;
use App\Models\Units;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Ixudra\Curl\Facades\Curl;
use App\Models\EtimGorup;

trait ProductTrait
{
    public function __construct()
    {

    }

    public function seoProduct($title, $description, $keywords, $uid)
    {

        $data = [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'uid' => $uid
        ];

        SeoProduct::create($data);
    }

    public function getUnit($unit, $system)
    {
        if (empty($unit))
            return 0;

        $model = new Units();
        $unitData = $model->getUnitCode($unit->code);
        if (empty($unitData)) {
            $data = [
                "title" => $unit->title,
                "code" => $unit->code,
                "title_abb" => $unit->title_abb,
                "system" => $system,
                "external_id" => (isset($unit->uid)) ? $unit->uid : 0
            ];
            $newUnit = new Units();
            $newUnit->title = $data["title"];
            $newUnit->code = $data["code"];
            $newUnit->title_abb = $data["title_abb"];
            $newUnit->system = $data["system"];
            $newUnit->external_id = $data["external_id"];
            $newUnit->save();
            return $newUnit->uid;
        } else {
            return $unitData->uid;
        }
    }


    /**
     * @param $data
     * @return string
     * Загрузка фотографии
     *
     */
    public function uploadImage($uid, $data)
    {
        foreach ($data as $d) {
            $image_64 = $d->img; //your base64 encoded data
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

            $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
            $image = str_replace($replace, '', $image_64);
            $image = str_replace(' ', '+', $image);
            $imageName = md5(microtime()) . '.' . $extension;
            Storage::put('/products/' . $imageName, base64_decode($image));

            $this->storeImage($uid, $imageName);
        }
    }

    public function storeImage($uid, $name)
    {
        $model = new ProductImage();
        $model->uid = $uid;
        $model->name = $name;
        $model->main = 1;
        $model->display = 1;
    }

    public function updatePrice($item, $data)
    {
        try {
            $uidProduct = $this->getProductExternalId($item->uid, ['uid']);
            if (empty($uidProduct))
                Log::info('Товар с uid - ' . $item->uid . ' не найден. Цена не обновлена');

            if (empty($this->getPriceProduct($uidProduct->uid, $item->type))) {
                ProductPrice::insert([
                    'products_uid' => $uidProduct->uid,
                    'price' => $item->price,
                    'type_price' => $item->type,
                ]);
            } else {
                ProductPrice::where('products_uid', $uidProduct->uid)
                    ->where('type_price', $item->type)
                    ->delete();
                ProductPrice::insert([
                    'products_uid' => $uidProduct->uid,
                    'price' => $item->price,
                    'type_price' => $item->type,
                ]);
            }



        } catch (\Exception $e) {
            Log::error($e->getMessage() . ' ' . json_encode($item));
        }
    }

    public function getPriceProduct($uid, $type)
    {

        return
            ProductPrice::where('products_uid', $uid)
                ->where('type_price', $type)->whereNull('dt_end')->first();
    }

    public function getProductUid($uid)
    {
        return Product::where('uid', $uid)->firts();
    }

    public function getProductExternalId($uid, $fields = ['*'])
    {

        return Product::select(implode(',', $fields))->where('external_id', $uid)->first();
    }


    public function getTypesPrice()
    {
        return typePrice::all();
    }

    public function getTypePriceProduct($type)
    {
        return typePrice::where('type', $type)->first();
    }


    public function actionStock($uid, $s)
    {
        if (empty($this->getStockStorage($uid, $s->storage_id))) {
            $this->storeStock($uid, $s->stock, $s->storage_id);
        } else {
            $this->updateStock($uid, $s->stock, $s->storage_id);
        }
    }

//Обновление остатка
    public function updateStock($uid, $stock, $storageId)
    {
        ProductStock::where('product_uid', $uid)
            ->where('storage_id', $storageId)
            ->delete();
        $this->storeStock($uid, $stock, $storageId);
    }

//    Создание остатка для товара
    public function storeStock($uid, $stock, $storageId)
    {
        $m = new ProductStock();
        $m->storage_id = $storageId;
        $m->product_uid = $uid;
        $m->stock = $stock;
        if($stock>0){
            $m->available = 1;
        } else {
            $m->available = 0;
        }

        $m->save();
    }

//    получение остатка товара на определенном складе
    public function getStockStorage($uid, $storageId)
    {
        return ProductStock::where('product_uid', $uid)
            ->where('storage_id', $storageId)->first();
    }

//    Получение всех остатков по товару
    public function getAllStock($uid)
    {
        return ProductStock::select('storage_id', 'stock')->where('product_uid', $uid)
            ->get();
    }

    public function createWeghtProduct($uid, $data)
    {
        ProductWeight::where('product_uid', $uid)->delete();

        $model = new ProductWeight();
        $model->product_uid = $uid;
        $model->length = $data->length;
        $model->width = $data->width;
        $model->height = $data->height;
        $model->volume = $data->volume;
        $model->weight = $data->weight;
        $model->save();
    }

//    public function getProductRaecId($raecId)
//    {
//
//    }
}
