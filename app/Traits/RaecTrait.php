<?php

namespace App\Traits;

use App\Models\BrandSynonimRaec;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use Ixudra\Curl\Facades\Curl;
use App\Models\BrandRaec;
use App\Models\BrandSeriesMarketingRaec;
use App\Models\BrandSeriesRaec;

trait RaecTrait
{


    private $apiKey = 'ebb91621277e4915b63c6124f982fefc';
    private $baseUrl = 'https://catalog.raec.su/api';

    public function __construct()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
    }

    public function getBrand()
    {
        $response = Curl::to($this->baseUrl . '/brand')
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
        $result = json_decode($response);

        return $result;
    }

    public function storeBrand($data)
    {
        $brandRaec=new BrandRaec();
        $brandRaec->title = $data->name;
        $brandRaec->brand_id = $data->id;
        $brandRaec->description = $data->description;
        $brandRaec->save();
    }

    public function storeSynonimBrand($data, $brandId)
    {

        foreach ($data as $d) {
            $model=new BrandSynonimRaec();
            $model->title = $d;
            $model->brand_id = $brandId;
            $model->save();
        }
    }

    public function storeSeriaBrand($data, $brandId)
    {

        foreach ($data as $d) {
            $model=new BrandSeriesRaec();
            $model->seria_id=$d->id;
            $model->title = $d->name;
            $model->brand_id = $brandId;
            $model->save();
        }
    }

    public function storeSeriaMarketingBrand($data, $brandId)
    {
        foreach ($data as $d) {
            $model=new BrandSeriesMarketingRaec();
            $model->seriaMarketingId=$d->id;
            $model->title = $d->name;
            $model->brand_id = $brandId;
            $model->save();
        }
    }

    public function getStorage () {
        $response = Curl::to($this->baseUrl . '/storage')
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
        $result = json_decode($response);

        return $result;
    }

}
