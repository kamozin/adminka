<?php

namespace App\Traits;

use App\Models\CatalogToRetail;
use App\Models\Category;
use App\Models\CategoryRetail;

trait CategoryRetailTrait
{

    public function __construct()
    {

    }

//    Получение uid категории ИМ по внешнему uid
    public function getUidCategoryToExternal ($uid) {
      return  CatalogToRetail::where('catalog_external_uid', $uid)->first();
    }

    public function getCategory ($uid) {

    }

    public function getItemCatalogExternal ($field, $value) {
        return Category::where($field, $value)->first();
    }

}
