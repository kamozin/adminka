<?php

namespace App\Traits;


use App\Models\EtimClass;
use App\Models\Features;
use Illuminate\Support\Facades\Redis;
use Ixudra\Curl\Facades\Curl;
use App\Models\EtimGorup;

trait RaecEtimTrait
{


    private $apiKey = 'ebb91621277e4915b63c6124f982fefc';
    private $baseUrl = 'https://catalog.raec.su/api';

    public function __construct()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
    }


    public function getEtimGroup()
    {
        $response = Curl::to($this->baseUrl . '/etimgroup/')
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
        $result = json_decode($response);
        $this->storeEtimGroup($result);
    }

    /**
     * @param $data
     */
    public function storeEtimGroup($data)
    {
        foreach ($data as $d) {
            $etimGroup = new EtimGorup();
            $etimGroup->fill((array)$d);
            $etimGroup->save();
        }
    }

    public function getPages($method)
    {
        $response = Curl::to($this->baseUrl . '/' . $method . '/pages')
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
//        dd($response);
        $redis = Redis::connection();
        $redis->set('pages_' . $method, $response);


    }

    public function getEtimClass()
    {
        $pages = Redis::get('pages_etimclass');
        $currentPage = Redis::get('current_page_etimclass');
        if ($currentPage == null) {
            $currentPage = 1;
        }


        for ($i = $currentPage; $i < $pages + 1; $i++) {
            $response = Curl::to($this->baseUrl . '/etimclass/page-' . $i)
                ->withHeader('API-KEY:  ' . $this->apiKey)
                ->withHeader('format: json')
                ->get();
            $result = json_decode($response);

            $this->storeEtimClass($result);

            Redis::set('current_page_etimclass', $i);
        }
        Redis::del('current_page_etimclass');
        Redis::del('pages_etimclass');
    }

    private function storeEtimClass($data)
    {
        foreach ($data as $d) {
            if ($this->existsEtimClass($d->groupId, $d->classId) == true) {

            } else {
                $etimClass = new EtimClass();
                $etimClass->classId = $d->classId;
                $etimClass->groupId = $d->groupId;
                $etimClass->descriptionEn = $d->descriptionEn;
                $etimClass->descriptionRu = $d->descriptionRu;
                $etimClass->etimVersionStart = $d->etimVersionStart;
                $etimClass->etimVersionEnd = $d->etimVersionEnd;
                $etimClass->save();
            }
        }
    }

    private function existsEtimClass($groupId, $classId)
    {
        return EtimClass::where('groupId', $groupId)->where('classId', $classId)->exists();
    }

    public function getEtimClassId($id)
    {
        $response = Curl::to($this->baseUrl . '/etimclass/' . $id)
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
        $result = json_decode($response);

        return $result;
    }

    public function checkFeatureEtim ($id) {
        return Features::where('featureId', $id)->first();
    }


}
