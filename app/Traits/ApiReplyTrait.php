<?php

namespace App\Traits;


trait ApiReplyTrait
{


    public function successResponse($data = null)
    {
        if (isset($data))
            return response()->json(['result' => true, 'data' => $data], 200, [], JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

        return response()->json(['result' => true], 200, [], JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    public function errorResponse($error = null, $status=500)
    {
        if (isset($error))
            return response()->json(['result' => false, 'data' => $error], $status, [], JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

        return response()->json(['result' => false], $status, [], JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    public function paginateResponse($paginate)
    {
        if (!$paginate)
            return $this->errorResponse();

        return [
            'result' => true,
            'data' => [
                'count' => $paginate->count(),
                'hasMore' => $paginate->hasMorePages(),
                'items' => $paginate->items(),
            ],
        ];
    }

}
