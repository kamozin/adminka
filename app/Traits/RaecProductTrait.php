<?php

namespace App\Traits;


use App\Models\EtimClass;
use App\Models\Features;
use App\Models\FileType;
use App\Models\ProductFile;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductStock;
use App\Models\Units;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Ixudra\Curl\Facades\Curl;
use App\Models\EtimGorup;

trait RaecProductTrait
{


    private $apiKey = 'ebb91621277e4915b63c6124f982fefc';
    private $baseUrl = 'https://catalog.raec.su/api';

    public function __construct()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
    }


    public function getProductRaecId($raecId)
    {
        $response = Curl::to($this->baseUrl . '/product/' . $raecId)
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
        $result = json_decode($response);

        return $result;
    }

    public function getProductRql($filter)
    {
        $response = Curl::to($this->baseUrl . '/product')
            ->withData(array('rql' => $filter))
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
        $result = json_decode($response);

        return $result;
    }

    public function getProductRaecIdApi ($id) {
        $response = Curl::to($this->baseUrl . '/product/'.$id)
            ->withHeader('API-KEY:  ' . $this->apiKey)
            ->withHeader('format: json')
            ->get();
        $result = json_decode($response);

        return $result;
    }


    public function getTitle($data)
    {
        if (!empty($data->descriptionRu)) {
            return $data->descriptionRu;
        }
        if (!empty($data->descriptionShort)) {
            return $data->descriptionShort;
        }
        if (!empty($data->descriptionAutoYandex)) {
            return $data->descriptionAutoYandex;
        }
        if (!empty($data->naming)) {
            return $data->naming;
        }
        if (!empty($data->descriptionAuto)) {
            return $data->descriptionAuto;
        }
    }

    public function getCategoryRetail($classId)
    {
        $class = EtimClass::where('classId', $classId)->first();

        if (empty($class))
            return 0;
        return $class->category_retail_uid;
    }



    public function generateArrayPriceZero($uid)
    {

        ProductPrice::where('products_uid', $uid)->delete();
        for ($type = 1; $type < 7; $type++) {
            $model = new ProductPrice();
            $model->type_price = $type;
            $model->price = 0;
            $model->products_uid = $uid;
            $model->save();
        }
    }


    public function generateArrayStockZero($uid)
    {
       ProductStock::where('product_uid', $uid)->delete();

        for ($type = 1; $type < 7; $type++) {
            $model = new ProductStock();
            $model->storage_id = $type;
            $model->stock = 0;
            $model->product_uid = $uid;
            $model->save();
        }
    }

    public function files($data, $uid)
    {
        if (sizeof($data[0]->files) > 0) {
            ProductFile::where('product_uid', $uid)->delete();
            foreach ($data[0]->files as $f) {
                if(isset($f->url)){
                    $dataFile = [
                        'product_uid' => $uid,
                        'url' => $f->url,
                        'size' => (isset($f->size)) ? $f->size : 0,
                        'mimetype' => (isset($f->mimetype)) ? $f->mimetype : '',
                        'type' => FileType::type($f->type),
                        'certificateValidityFrom' => (isset($f->certificateValidityFrom)) ? $f->certificateValidityFrom : '',
                        'certificateValidityTo' => (isset($f->certificateValidityTo)) ? $f->certificateValidityTo : ''                     ];
                    ProductFile::create($dataFile);
                }

            }
        }
    }

    public function image($data)
    {

        $img = (array)$data;
        $image = '';
        if (isset($img['60'])) {
            $image = $img['60'];
        }
        if (isset($img['150'])) {
            $image = $img['150'];
        }

        if (isset($img['600'])) {
            $image = $img['600'];
        }

        if (isset($img['max'])) {
            $image = $img['max'];
        }

        if (!empty($image)) {
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );
            $url = $image;
            $contents = file_get_contents($url, false, stream_context_create($arrContextOptions));
            $name = substr($url, strrpos($url, '/') + 1);
            Storage::put('/images/' . $name, $contents);
            return $name;
        }
    }

    public function saveImage ($name, $uid) {
        $model=new ProductImage();
        $model->name=(empty($name)) ? 'no-photo.png' : $name;
        $model->main=1;
        $model->product_uid=$uid;
        $model->display=1;
        $model->system='RAEC';
        $model->save();
    }

}
