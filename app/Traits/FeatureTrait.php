<?php

namespace App\Traits;

use App\Models\CategoryRetail;
use App\Models\EtimClass;
use App\Models\Features;
use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\RetailFeatureValueProduct;
use App\Models\UnitFeatures;
use App\Models\ValueFeaturesRetail;
use App\Models\ValueFeatureToCategory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Ixudra\Curl\Facades\Curl;
use App\Models\EtimGorup;
use App\Models\FeaturesRetail;

trait FeatureTrait
{

    public function __construct()
    {

    }

    public function features($features, $productUid, $categoryUid, $system)
    {

        foreach ($features as $f) {
            $featureBD = $this->getFeatureExternalUid($f->uid);
            if (empty($featureBD)) {
                $featureUid = $this->storeFeature($f);

                $arrayValue = $this->valuesFeature($featureUid, $categoryUid, $f->values);
                foreach ($arrayValue as $arr) {
                    $this->setFeaturesRetailToCategory($featureUid, $categoryUid);
                    $this->saveValueFeatureToCategoryRetail($categoryUid, $featureUid, $arr['uid']);
                    $this->saveRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $arr['uid'], $arr['title'], $system);

                }
            } else {
                try {

                    $arrayValue = $this->valuesFeature($featureBD->uid, $categoryUid, $f->values);

                    foreach ($arrayValue as $arr) {
                        $this->saveValueFeatureToCategoryRetail($categoryUid, $featureBD->uid, $arr['uid']);
                        $this->saveRelateValueFeatureProduct($categoryUid, $productUid, $featureBD->uid, $arr['uid'], $arr['title'], $system);
                    }
                } catch (\Exception $e) {

                }

            }
        }
    }

    public function valuesFeature($featureUid, $categoryUid, $values)
    {

        $array = [];
        foreach ($values as $v) {
            $value = $this->getValueExternalUid($v->uid);

            if (empty($value)) {
                $arrayValue = $this->toArrayFeatureValue($v->value);

                $tempUnit = end($arrayValue);
                $unitFeature = $this->getUnitFeature($tempUnit);

                if (empty($unitFeature)) {
//                    Создаем значение
                    $modelValue = new ValueFeaturesRetail();
                    $modelValue->title = $v->value;
                    $modelValue->external_uid = $v->uid;
                    $modelValue->value_external = $v->value;
                    $modelValue->features_retail_uid = $featureUid;

                    $modelValue->save();

                } else {
//                    создаем значение и устанавливаем ед. измерения для фильтра
                    $this->setUnitFeatureRetail($unitFeature->id, $featureUid);
                    $modelValue = new ValueFeaturesRetail();
                    $modelValue->title = $arrayValue[0];
                    $modelValue->external_uid = $v->uid;
                    $modelValue->value_external = $v->value;
                    $modelValue->features_retail_uid = $featureUid;
                    $modelValue->save();


                }
                $arr = [
                    "uid" => $modelValue->uid,
                    "title" => $modelValue->title
                ];


                array_push($array, $arr);

            } else {
                $arr = [
                    "uid" => $value->uid,
                    "title" => $value->title
                ];

                array_push($array, $arr);
            }
        }
        return $array;
    }

    public function getValueExternalUid($uid)
    {
        return ValueFeaturesRetail::where('external_uid', $uid)->first();
    }

    public function storeFeature($data)
    {
        $model = new FeaturesRetail();
        $model->title = $data->title;
        $model->external_uid = $data->uid;
        $model->save();
        return $model->uid;
    }


    /**
     * Получение свойства по внешнему uid
     * @param $uid
     * @return mixed
     */
    public function getFeatureExternalUid($uid)
    {
        return FeaturesRetail::where('external_uid', $uid)->first();
    }

    /**
     * Получение свойства по uid
     * @param $uid
     * @return mixed
     */
    public function getFeature($uid)
    {
        return FeaturesRetail::find($uid);
    }

    /**
     * Получение значения по внешнему uid
     * @param $uid
     * @return mixed
     */
    public function getFeaturesValueExternal($uid)
    {
        return ValueFeaturesRetail::where('external_uid', $uid)->first();
    }

    /**
     * Привязка к категории
     * @param $category
     * @param $feature
     * @param $value
     */
    public function saveValueFeatureToCategoryRetail($category, $feature, $value)
    {
        if (empty($this->getValueFeatureToCategoryRetail($category, $feature, $value))) {
            $model = new ValueFeatureToCategory();
            $model->category_retail_uid = $category;
            $model->feature_retail_uid = $feature;
            $model->feature_value_retail_uid = $value;
            $model->save();
        }
    }

    public function getValueFeatureToCategoryRetail($category, $feature, $value)
    {
        return ValueFeatureToCategory::where('category_retail_uid', $category)
            ->where('feature_retail_uid', $feature)
            ->where('feature_value_retail_uid', $value)
            ->first();

    }

    public function getFeaturesCategoryRetail($category, $sort = null)
    {

        $data = CategoryRetail::where('uid', $category)
            ->with(['features' => function ($q) use ($sort) {
                $q->with(['valueCategory' => function ($query) {
                    $query->where('category_retail_uid', \request()->category);
                    $query->groupBy('feature_value_retail_uid');
                }]);

                if ($sort == 'filter') {
                    $q->orderBy('sort_category_retail', 'asc');
                } else if ($sort == 'product') {
                    $q->orderBy('sort_category_retail_product', 'asc');
                } else {
                    $q->orderBy('sort_category_retail', 'asc');
                }
            }])
            ->first();



        return $data;
    }

    public function getRelatedPropertyEtim($category, $f)
    {
        $array = [];
        if ($f->relationEtim($category)->count() > 0) {
            foreach ($f->relationEtim($category)->get() as $item) {
                $i = (object)[];
                $i->descriptionRu = $item->feature->descriptionRu;
                $i->classId = $item->classId;
                $i->featureId = $item->etimId;
                array_push($array, $i);
            }
        }
        return $array;
    }


    public function getRelatedPropertyValueEtim()
    {

    }


    public function saveRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system)
    {
        if (empty($this->checkRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system))) {
            RetailFeatureValueProduct::where('category_uid', '<>', $categoryUid)
                ->where('product_uid', $productUid)
                ->delete();
            $model = new RetailFeatureValueProduct ();
            $model->category_uid = $categoryUid;
            $model->product_uid = $productUid;
            $model->feature_uid = $featureUid;
            $model->value_uid = $valueUid;
            $model->value = $value;
            $model->system = $system;
            $model->save();
        }
    }

    public function setFeaturesRetailToCategory($uidFeature, $uidCategory)
    {

        if (empty($this->checkFeatureCategory($uidFeature, $uidCategory))) {
            $model = new FeaturesRetailToCategoryRetail();
            $model->features_retail_uid = $uidFeature;
            $model->category_retail_uid = $uidCategory;
            $model->useFilter = 1;
            $model->useFilter = 1;
            $model->display_category_retail = 1;
            $model->sort_category_retail = 0;
            $model->sort_category_retail_product = 0;
            $model->save();
        }
    }

    public function checkFeatureCategory($uidFeature, $uidCategory)
    {
        return FeaturesRetailToCategoryRetail::where('features_retail_uid', $uidFeature)
            ->where('category_retail_uid', $uidCategory)
            ->first();
    }

    public function checkRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system)
    {
        return RetailFeatureValueProduct::where('category_uid', $categoryUid)
            ->where('product_uid', $productUid)
            ->where('feature_uid', $featureUid)
            ->where('value_uid', $valueUid)
            ->where('value', $value)
            ->where('system', $system)
            ->first();
    }

    public function toArrayFeatureValue($value)
    {
        return explode(' ', $value);
    }

    public function getUnitFeature($unit)
    {
        return UnitFeatures::where('descriptionRu', trim($unit))->first();
    }

    public function setUnitFeatureRetail($unitId, $featureUid)
    {
        try {
            $data = FeaturesRetail::find($featureUid);


            $data->unit_features = $unitId;
            $data->save();
            return true;
        } catch (\Exception $e) {
            Log::error('Не присвоилась ед. измерения ' . $unitId . ' для фильтра ' . $featureUid);
            return false;
        }
    }

    public function setType($type, $featureUid)
    {
        try {
            $data = FeaturesRetail::find($featureUid);
            $data->type = $type;
            $data->save();
            return true;
        } catch (\Exception $e) {
            Log::error('Не присвоился тип ' . $type . ' для фильтра ' . $featureUid);
            return false;
        }
    }

}

