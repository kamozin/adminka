<?php

namespace App\Traits;

use App\SMSRU\SMSRU;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

trait SmsTrait
{

    public function smsSend ($phone, $text) {
        try {
            //$body = file_get_contents("https://sms.ru/sms/send?api_id=09680556-7A6A-D840-E479-D53124BE68B1&to='.$phone.'&text=" . urlencode("Ваш заказ №" . $id . " принят. Сумма заказа " . $total_price . " р.")); # Если приходят крякозябры, то уберите iconv и оставьте только "Привет!"
          //  $smsru = new SMSRU('09680556-7A6A-D840-E479-D53124BE68B1'); // Ваш уникальный программный ключ, который можно получить на главной странице
            $data = (object)[];
            $data->to = $phone;
            $data->text = $text; // Текст сообщения
//            dd($data);
            $sms = $this->send_one($data); // Отправка сообщения и возврат данных в переменную
        } catch (\Exception $e) {
            Log::error('Смс не отправлена - '.$phone.' текст - '.$text);
        }
    }

    private function send_one($data)
    {
//        $dataSms = (object)[];
//        $dataSms->messages = [
//            (object)[
//                "content" => (object)[
//                    "short_text" => $data->text
//                ],
//                "to" => (object)[
//                    "msisdn" => $data->to
//                ]
//            ]
//        ];
//        $dataSms->options = (object)[
//            "from" => (object)[
//                "sms_address" => "shop-aventa"
//            ]
//        ];
//        $req = json_encode($dataSms, JSON_FORCE_OBJECT);


        $response = Curl::to('https://omnichannel.mts.ru/http-api/v1/messages')
            ->withTimeout(60)
            ->withContentType('application/json')
            ->withAuthorization('Basic Z3dfbkVaR3RwclZXeWhxOllobXM3Z2hS')
            ->withData('{"messages":[{"content":{"short_text":"'.$data->text.'"},"to":[{"msisdn":"'.$data->to.'"}]}],"options":{"from":{"sms_address":"shop-aventa"}}}')
            ->post();
    }

}
