<?php

namespace App\Console\Commands;

use App\Http\Controllers\LoadImage;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class LoadImageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'route:loadimage {uri}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'php artsian route:loadimage /loadimage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      $controller = new LoadImage(); // make sure to import the controller
      $controller->start();

    }
}
