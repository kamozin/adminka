<?php

namespace App\Console\Commands;

use App\Http\Controllers\AnalogController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\ValueRetailFeatureValueProductDropDuplicateController;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class ValueRetailFeatureValueProductDropDuplicateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'route:valueretailfeaturevalueproductdropduplicate {uri}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'php artisan route:valueretailfeaturevalueproductdropduplicate /valueretailfeaturevalueproductdropduplicate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new ValueRetailFeatureValueProductDropDuplicateController(); // make sure to import the controller
        $controller->start();

    }
}
