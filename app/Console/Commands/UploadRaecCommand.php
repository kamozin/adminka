<?php

namespace App\Console\Commands;

use App\Http\Controllers\CheckImageController;
use App\Http\Controllers\LoadImage;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class UploadRaecCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'route:uploadimageraec {uri}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'php artsian route:uploadimageraec /uploadimageraec';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new LoadImage(); // make sure to import the controller
        $controller->startRaec();

    }
}
