<?php

namespace App\Console\Commands;

use App\Http\Controllers\FileController;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class LoadFileRaecCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'route:loadfile {uri}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'php artisan route:loadfile /loadfile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new FileController(); // make sure to import the controller
        $controller->index();

    }
}
