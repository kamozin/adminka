<?php

namespace App\Console\Commands;

use App\Http\Controllers\AnalogController;
use App\Http\Controllers\FileController;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class UploadRelatedProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'route:relatedproduct {uri}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'php artisan route:relatedproduct /relatedproduct';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $controller = new AnalogController(); // make sure to import the controller
        $controller->index();

    }
}
