<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductStock;
use App\Models\TypePrice;
use App\Models\TypeStorage;
use App\Traits\ApiReplyTrait;
use App\Traits\FeatureTrait;
use App\Traits\ProductTrait;
use App\Traits\ResponseTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StoreProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ApiReplyTrait, ProductTrait, FeatureTrait, ResponseTrait;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($products)
    {
        $this->data = $products;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \DB::beginTransaction();
        $typePrices = TypePrice::all();
        $typeStorage = TypeStorage::all();
        foreach ($this->data as $d) {
            try {
                $dataProduct = $this->generateProductArray($d, '1C');
                $model = new Product();
                $product = $model->where('external_id', $dataProduct['external_id'])->first();
                if (empty($product)) {
                    $product = Product::create($dataProduct);
                    $this->createWeghtProduct($product->uid, $d->productWeight);
                    $this->storePriceZero($product->uid, $typePrices);
                    $this->storeZeroStock($product->uid, $typeStorage);
                    $this->imageProduct($product->uid, $d->images);
                } else {
                    $dataProduct['uid'] = $product->uid;
                    Product::where('uid', $product->uid)->update($dataProduct);
                    $this->createWeghtProduct($product->uid, $d->productWeight);
                    $this->imageProduct($product->uid, $d->images);
                }
            } catch (\Exception $e) {
                Log::error('Ошибка при создании товара- ' . $d->uid . ' ' . $e->getMessage() . ' ' . json_encode($d));
            }
        }
        \DB::commit();

    }

    private function imageProduct($uid, $images)
    {
//

        foreach ($images as $i) {

            ProductImage::where('product_uid', $uid)
                ->where('name', $i)
                ->delete();

            if (empty($check)) {
                $m = new ProductImage ();
                $m->name = $i;
                $m->product_uid = $uid;
                $m->main = 1;
                $m->display = 1;
                $m->save();
            }
        }
    }


    private function storeZeroStock($uid, $typeStorage)
    {
        foreach ($typeStorage as $t) {
            $stockModel = new ProductStock();
            $stockModel->product_uid = $uid;
            $stockModel->stock = 0;
            $stockModel->storage_id = $t->type;
            $stockModel->save();
        }
    }

    private function storePriceZero($uid, $typePrice)
    {
        foreach ($typePrice as $t) {
            $priceModel = new ProductPrice();
            $priceModel->products_uid = $uid;
            $priceModel->price = 0;
            $priceModel->type_price = $t->type;
            $priceModel->save();
        }
    }


    private function generateProductArray($data, $system)
    {

        $product = [];
        if (isset($data->raecId))
            $product['raec_id'] = $data->raecId;

        $product['title'] = $data->title;
        $product['code'] = $data->code;
        $product['external_id'] = $data->uid;
        $product['sku'] = $data->sku;
        $product['brand_uid'] = $data->brand_uid;
        $product['description'] = $data->description;
        $product['parent_id'] = $this->getParentId($data->parent_id);

        if ($data->multiplicity == 0) {
            $product['multiplicity'] = 1;
        } else {
            $product['multiplicity'] = $data->multiplicity;
        }

        $product['unit_id'] = (isset($data->orderUnit)) ? $this->getUnit($data->orderUnit, '1С') : 0;
        $product['display'] = $data->display;
        $product['liquid_stock'] = $data->liquid_stock;
        $product['attribute_bay'] = $data->attribute_bay;
        $product['system'] = $system;

        return $product;
    }

    private function getParentId($uid)
    {
        $data = DB::table('catalog_to_retail')->where('catalog_external_uid', $uid)->first();

        if (empty($data)) {
            return "00000000-0000-0000-0000-000000000000";
        }

        return $data->category_retail_uid;
    }

    public function loadVideo($uid)
    {

    }


}
