<?php

namespace App\Jobs;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class setPriceShopZero implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('products_price')
            ->leftJoin('stock_products', 'products_price.products_uid', '=', 'stock_products.product_uid')
            ->where('stock_products.storage_id', '=', 1)
            ->where('stock_products.stock', '=', 0)
            ->where('products_price.type_price', '=', 6)
            ->update(
                [
                    "products_price.price"=>0
                ]
            );
    }
}
