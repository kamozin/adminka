<?php

namespace App\Jobs;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class setJobLoadImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $getImgProd = DB::table('retail_feature_value_product')
            ->where('value', 'Arlight')
            ->where('system', 'RAEC')
            ->get();
        foreach ($getImgProd as $i) {
            $data = Product::where('uid', $i->product_uid)
                ->first();
            if(!empty($data)){
                if ($data->system == 'RAEC') {
                    RaecUploadImage::dispatch($data);
                }
            }
        }


    }
}
