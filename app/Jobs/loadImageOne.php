<?php

namespace App\Jobs;

use App\Models\ProductImage;
use App\Traits\RaecProductTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class loadImageOne implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use RaecProductTrait;

    protected $product;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->product=$data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dataProduct=$this->getProductRaecId($this->product->raec_id);

        $images = ProductImage::where('product_uid', $this->product->uid)->get();
        if (sizeof($images) == 0 || (sizeof($images) == 1 && $images[0]->name == 'no-photo.png')) {
            if (!empty($dataProduct->image)) {
                $name = $this->image($dataProduct->image);
                $this->saveImage($name, $this->product->uid);
            }


            if (sizeof($dataProduct->gallery) > 0) {
                foreach ($dataProduct->gallery as $g) {
                    $name = $this->image($g);
                    $this->saveImage($name, $this->product->uid);
                }
            }
        }
    }
}
