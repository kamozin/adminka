<?php

namespace App\Jobs;

use App\Models\Product;
use App\Models\ProductImage;
use App\Traits\RaecProductTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class LoadImageBaseProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use RaecProductTrait;
    protected $raecId;
    protected $uid;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($raecId, $uid)
    {
        $this->raecId=$raecId;
        $this->uid=$uid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
           $data=$this->getProductRaecId($this->raecId);

                try {
                        $p=Product::where('uid', $this->uid)->first();
                        if (sizeof($data->gallery) > 0) {
                            foreach ($data->gallery as $g) {
                                $arrImage = $this->image($g);
                                if (!isset($arrImage['name']) && !isset($arrImage['url'])) {
                                    continue;
                                }
                                if (!$this->checkImage($p->uid, $arrImage)) {
                                    if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                                        try {
                                            $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                                            $id = $this->saveImage($arrImage, $p->uid);
                                            $this->trim($arrImage['name'], $id);
                                        } catch (\Exception $e) {

                                        }
                                    }
                                }
                            }

                        } else {
                            $arrImage = $this->image($data->image);


                            if (!$this->checkImage($p->uid, $arrImage)) {
                                if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                                    try {
                                        $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                                        $id = $this->saveImage($arrImage, $p->uid);
                                        $this->trim($arrImage['name'], $id);

                                    } catch (\Exception $e) {

                                    }
                                }
                            }
                        }
                        Product::where('uid', $p->uid)
                            ->update([
                                'imgLoad'=>1
                            ]);
                } catch (\Exception $e) {
                    Product::where('uid', $p->uid)
                        ->update([
                            'imgLoad'=>2
                        ]);
                }
    }

    private function uploadRaecImage($url, $name)
    {

        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $contents = file_get_contents($url, false, stream_context_create($arrContextOptions));
        Storage::disk('s3')->put('files/products/' . $name, $contents);

    }

    private function trim($name, $id)
    {
        // URL оригинального изображения на Amazon S3
        $originalImageUrl = 'https://s3.timeweb.com/1dafdd54-ac71a2de-0e06-4dc9-8dbc-1992d37c2815/files/products/' . $name;

        // Получаем оригинальное изображение с Amazon S3
        $response = Http::get($originalImageUrl);

        // Проверяем успешность запроса
        if ($response->successful()) {
            // Открываем изображение с помощью Intervention Image
            $image = Image::make($response->body());

            // Получаем имя файла без пути
            $originalFileName = pathinfo($name, PATHINFO_BASENAME);

            // Нарезка и сохранение изображения для разных разрешений
            $resolutions = [400, 60, 80]; // Здесь вы можете указать нужные вам разрешения
            $resolutionsCart = [80, 60];
            $resizedImage = $image->backup();
            $width = $resizedImage->width();
            $original = $image->backup();
            $path = 'test' . '/original/' . $originalFileName;
            $extension = pathinfo($originalImageUrl, PATHINFO_EXTENSION);
            Storage::disk('s3')->put($path, (string)$original->encode($extension, 50));
            if ($width >= 600) {
                foreach ($resolutions as $resolution) {
                    $original = $image->backup();
                    $original->resize($resolution, $resolution, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $path = 'test/' . $resolution . '/' . $originalFileName;
                    Storage::disk('s3')->put($path, (string)$original->encode($extension, 75));
                }
            } else {
                foreach ($resolutionsCart as $resolutionC) {
                    $original = $image->backup();
                    $original->resize($resolutionC, $resolutionC, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    // Сохранение нарезанного изображения на Amazon S3
                    $path = 'test/' . $resolutionC . '/' . $originalFileName;
                    Storage::disk('s3')->put($path, (string)$image->encode($extension, 75));
                }
            }
            ProductImage::where('id', $id)
                ->update([
                    "trimStatus" => 1
                ]);

        } else {
            ProductImage::where('id', $id)
                ->update([
                    "trimStatus" => 2
                ]);
        }
    }

    private function checkBlackList($raec_id, $uid_product, $image)
    {
        return DB::table('blacklist_image')
            ->where('raec_id', $raec_id)
            ->where('uid_product', $uid_product)
            ->where('image', $image)
            ->first();
    }

    private function saveImage($arr, $uid)
    {
        $model = new ProductImage();
        $model->name = $arr['name'];
        $model->main = 1;
        $model->product_uid = $uid;
        $model->display = 1;
        $model->system = 'RAEC';
        $model->url_raec = $arr['url'];
        $model->save();
        return $model->id;
    }

    private function image($data): array
    {
        $image = '';
        if (isset($data->{'60'})) {
            $image = $data->{'60'};
        }
        if (isset($data->{'150'})) {
            $image = $data->{'150'};
        }
        if (isset($data->{'600'})) {
            $image = $data->{'600'};
        }
        if (isset($data->{'max'})) {
            $image = $data->{'max'};
        }
        if (!empty($image)) {
            $url = $image;
            $name = substr($url, strrpos($url, '/') + 1);
            return [
                "name" => $name,
                "url" => $url
            ];
        }
        return [];
    }

    private function checkImage($uid, $arr)
    {
        return ProductImage::where('product_uid', $uid)
            ->where('url_raec', $arr['url'])->first();
    }
}
