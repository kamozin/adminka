<?php

namespace App\Jobs;

use App\Models\FeaturesRetail;
use App\Models\Product;
use App\Models\ValueFeaturesRetail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class loadValueExcel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $sku;
    protected $filter;
    protected $value;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->sku=$data->sku;
        $this->filter=$data->filter;
        $this->value=$data->value;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = DB::table('upload_products_value_raec')
            ->where('sku', $this->sku)
            ->where('filter', $this->filter)
            ->where('value', $this->value)
            ->get();

        foreach ($data as $d) {
            $product = $this->getProduct($d->sku);
            if (!empty($product)) {
                $filter = $this->getFilter($d->filter);
                $value = $this->getValue($d->value, $filter->uid);
                $this->recordValueFeatureRetailToCategory($product->parent_id, $filter->uid, $value->uid);
                $this->recordRetailFeatureValueProduct($product->parent_id, $filter->uid, $value->uid, $product->uid, $value->title);
            }
            DB::table('upload_products_value_raec')
                ->where('sku', $d->sku)
                ->where('filter', $this->filter)
                ->where('value', $this->value)
                ->update([
                    "status" => 1
                ]);
        }
    }

    private function getProduct($sku)
    {
        return Product::where('sku', $sku)->first();
    }

    private function getFilter($filter)
    {
        return FeaturesRetail::where('title', trim($filter))->first();
    }

    private function getValue($value, $featureUid)
    {
        $data = ValueFeaturesRetail::where('title', trim($value))
            ->where('features_retail_uid', $featureUid)
            ->first();
        if (empty($data)) {
            $this->storeValue($value, $featureUid);
            $data = ValueFeaturesRetail::where('title', trim($value))
                ->where('features_retail_uid', $featureUid)
                ->first();
        }
        return $data;
    }

    private function storeValue($value, $featureUid)
    {
        $m = new ValueFeaturesRetail();
        $m->title = $value;
        $m->features_retail_uid = $featureUid;
        $m->value_external = $value;
        $m->save();
    }

    private function recordValueFeatureRetailToCategory($c, $f, $v)
    {
        $check = DB::table('value_feature_retail_to_category')
            ->where('category_retail_uid', $c)
            ->where('feature_retail_uid', $f)
            ->where('feature_value_retail_uid', $v)
            ->first();
        if (empty($check)) {
            DB::table('value_feature_retail_to_category')
                ->insert([
                    'category_retail_uid' => $c,
                    'feature_retail_uid' => $f,
                    'feature_value_retail_uid' => $v,
                ]);
        }
    }

    private function recordRetailFeatureValueProduct($c, $f, $v, $p, $value)
    {
        $check = DB::table('retail_feature_value_product')
            ->where('category_uid', $c)
            ->where('feature_uid', $f)
            ->where('product_uid', $p)
            ->where('value_uid', $v)
            ->where('value', $value)
            ->first();
        if (empty($check)) {
            DB::table('retail_feature_value_product')
                ->insert([
                    'category_uid' => $c,
                    'feature_uid' => $f,
                    'product_uid' => $p,
                    'value_uid' => $v,
                    'value' => $value,
                ]);
        }
    }
}
