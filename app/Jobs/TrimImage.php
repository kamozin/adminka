<?php

namespace App\Jobs;

use App\Models\ProductImage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TrimImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $name;
    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($name, $id)
    {
        $this->name = $name;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // URL оригинального изображения на Amazon S3
        $originalImageUrl = 'https://s3.timeweb.com/1dafdd54-ac71a2de-0e06-4dc9-8dbc-1992d37c2815/files/products/' . $this->name;


        // Получаем оригинальное изображение с Amazon S3
        $response = Http::get($originalImageUrl);
//        dd($response, $this->name);

        // Инициализируем cURL сеанс
        $ch = curl_init($originalImageUrl);
// Устанавливаем параметр CURLOPT_NOBODY для запроса без тела ответа
        curl_setopt($ch, CURLOPT_NOBODY, true);
// Выполняем запрос, чтобы получить заголовки ответа
        curl_exec($ch);
// Получаем информацию о размере изображения из заголовков ответа
        $fileSizeInBytes = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
// Закрываем cURL сеанс
        curl_close($ch);

        // Проверяем успешность запроса
        if ($response->successful()) {
            // Открываем изображение с помощью Intervention Image
            $image = Image::make($response->body());

            // Получаем имя файла без пути
            $originalFileName = pathinfo($this->name, PATHINFO_BASENAME);

            // Нарезка и сохранение изображения для разных разрешений
            $resolutions = [400, 60, 80]; // Здесь вы можете указать нужные вам разрешения
            $resolutionsCart=[80, 60];
            $resizedImage = $image->backup();
            $width=$resizedImage->width();
            $original=$image->backup();
            $path = 'test' . '/original/' . $originalFileName;
            $extension = pathinfo($originalImageUrl, PATHINFO_EXTENSION);
            Storage::disk('s3')->put($path, (string)$original->encode($extension, 50));
            if($width>=800){
                foreach ($resolutions as $resolution) {
                    $resizedImage->resize($resolution, $resolution, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    // Сохранение нарезанного изображения на Amazon S3
                    $path = 'test' . '/'.$resolution.'/' . $originalFileName;
                    Storage::disk('s3')->put($path, (string)$resizedImage->encode('jpg', 80));
                }
                $path = 'test' . '/original/' . $originalFileName;
                Storage::disk('s3')->put($path, (string)$image->encode('png', 75));
            }else{
                foreach ($resolutionsCart as $resolution) {
                    $resizedImage->resize($resolution, $resolution, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    // Сохранение нарезанного изображения на Amazon S3
                    $path = 'test' . '/'.$resolution.'/' . $originalFileName;
                    Storage::disk('s3')->put($path, (string)$image->encode('png', 75));
                }
            }

            ProductImage::where('id', $this->id)
                ->update([
                    "trimStatus" => 1
                ]);

        } else {
            ProductImage::where('id', $this->id)
                ->update([
                    "trimStatus" => 2
                ]);
        }
    }
}
