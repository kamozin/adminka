<?php

namespace App\Jobs;

use App\Models\FeaturesRetail;
use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\Product;
use App\Models\RetailFeatureValueProduct;
use App\Models\ValueFeaturesRetail;
use App\Models\ValueFeatureToCategory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FeatureRetail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($f)
    {
        $this->data = $f;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $d) {

            $product = $this->getInfoProduct($d->uid);
            if (!empty($product)) {
                $this->features($product, $d->features);
            }
        }
    }

    private function features($product, $features)
    {
//        Проходим массив характеристик
        $this->deleteRelateValueFeatureProducAll($product->parent_id, $product->uid);
        foreach ($features as $f) {
            $featureInfo = $this->checkfeatures($f->uid);
            if (empty($featureInfo)) {
                $uidFeature = $this->storeFeature($f);
            } else {
                $uidFeature = $featureInfo->uid;
            }
            if (sizeof($f->values) > 0) {
                $this->values($product->uid, $product->parent_id, $uidFeature, $f->values);
            }
        }
    }

    private function getInfoProduct($uidProduct)
    {
        return Product::where('external_id', $uidProduct)->first();
    }

    private function checkfeatures($uid)
    {
        $data = FeaturesRetail::where('external_uid', $uid)->first();
        return $data;
    }

    private function storeFeature($data)
    {
        $model = new FeaturesRetail();
        $model->title = $data->title;
        $model->external_uid = $data->uid;
        $model->save();
        return $model->uid;
    }

    private function values($uidProduct, $uidCategory, $uidFeature, $values)
    {
        foreach ($values as $v) {
            $value = $this->checkValue($v->uid);
            if (empty($value)) {
                $valueUid = $this->storeValue($v, $uidFeature);
            } else {
                $valueUid = $value->uid;
            }

            $this->checkPopulateCategory($uidFeature, $uidCategory);
            $this->saveRelateValueFeatureProduct($uidCategory, $uidProduct, $uidFeature, $valueUid, $v->value, "1С");
            $this->saveValueFeatureToCategoryRetail($uidCategory, $uidFeature, $valueUid);
        }

    }

    private function storeValue($v, $featureUid)
    {
        $modelValue = new ValueFeaturesRetail();
        $modelValue->title = $v->value;
        $modelValue->external_uid = $v->uid;
        $modelValue->value_external = $v->value;
        $modelValue->features_retail_uid = $featureUid;
        $modelValue->save();

        return $modelValue->uid;
    }

    private function checkValue($uid)
    {
        return ValueFeaturesRetail::where('external_uid', $uid)->first();
    }

    private function checkPopulateCategory($featureUid, $categoryUid)
    {
        if (empty($this->checkFeatureCategory($featureUid, $categoryUid))) {
            $model = new FeaturesRetailToCategoryRetail();
            $model->features_retail_uid = $featureUid;
            $model->category_retail_uid = $categoryUid;
            $model->useFilter = 1;
            $model->useFilter = 1;
            $model->display_category_retail = 1;
            $model->sort_category_retail = 0;
            $model->sort_category_retail_product = 0;
            $model->save();
        }
    }

    private function checkFeatureCategory($uidFeature, $uidCategory)
    {
        return FeaturesRetailToCategoryRetail::where('features_retail_uid', $uidFeature)
            ->where('category_retail_uid', $uidCategory)
            ->first();
    }

    private function saveRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system)
    {

        $data = FeaturesRetail::where('uid', $featureUid)->first();
        $title=$value;
        if(!empty($data->unit_features)) {
            $title=trim(substr($value, 0, strpos($value, $data->unit_features)));
        }


        if (empty($this->checkRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system))) {
            $model = new RetailFeatureValueProduct ();
            $model->category_uid = $categoryUid;
            $model->product_uid = $productUid;
            $model->feature_uid = $featureUid;
            $model->value_uid = $valueUid;
            $model->value = $title;
            $model->system = $system;
            $model->save();
        }else{
            $this->deleteRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system);
            $model = new RetailFeatureValueProduct ();
            $model->category_uid = $categoryUid;
            $model->product_uid = $productUid;
            $model->feature_uid = $featureUid;
            $model->value_uid = $valueUid;
            $model->value = $title;
            $model->system = $system;
            $model->save();
        }
    }

    private function checkRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system)
    {
        return RetailFeatureValueProduct::where('category_uid', $categoryUid)
            ->where('product_uid', $productUid)
            ->where('feature_uid', $featureUid)
            ->where('value_uid', $valueUid)
//            ->where('value', $value)
            ->where('system', $system)
            ->first();
    }

    private function deleteRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system)
    {
        return RetailFeatureValueProduct::where('category_uid', $categoryUid)
            ->where('product_uid', $productUid)
            ->where('feature_uid', $featureUid)
            ->where('value_uid', $valueUid)
//            ->where('value', $value)
            ->where('system', $system)
            ->delete();
    }

    private function deleteRelateValueFeatureProducAll ($categoryUid, $productUid) {
        RetailFeatureValueProduct::where('product_uid', $productUid)
            ->delete();
    }

    private function saveValueFeatureToCategoryRetail($category, $feature, $value)
    {
        if (empty($this->getValueFeatureToCategoryRetail($category, $feature, $value))) {
            $model = new ValueFeatureToCategory();
            $model->category_retail_uid = $category;
            $model->feature_retail_uid = $feature;
            $model->feature_value_retail_uid = $value;
            $model->save();
        }
    }

    private function getValueFeatureToCategoryRetail($category, $feature, $value)
    {
        return ValueFeatureToCategory::where('category_retail_uid', $category)
            ->where('feature_retail_uid', $feature)
            ->where('feature_value_retail_uid', $value)
            ->first();

    }
}
