<?php

namespace App\Jobs;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use App\Jobs\loadValueExcel;

class uploadFileValue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $collection;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        foreach ($this->collection as $c) {
            $arr = [];
            $i = 0;
            foreach ($c as $key => $col) {
                if ($key == 'Код производителя') {
                    $arr['sku'] = $col;
                }
                if ($key == 'Filter') {
                    $arr['filter'] = $col;
                }
                if ($key == 'Value') {
                    $arr['value'] = $col;
                }
            }

            DB::table('upload_products_value_raec')
                ->insert([
                    "sku" => $arr['sku'],
                    "filter" => $arr['filter'],
                    "value" => $arr['value'],
                    "status" => 0,
                    "created_at" => Carbon::now()
                ]);
            $data = (object)[];
            $data->sku = $arr['sku'];
            $data->filter = $arr['filter'];
            $data->value = $arr['value'];
            loadValueExcel::dispatch($data);
        }
    }
}
