<?php

namespace App\Jobs;

use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\Product;
use App\Models\RetailFeatureValueProduct;
use App\Models\ValueFeatureToCategory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckProductsFeatures implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $category;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($category)
    {
        $this->category=$category;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->checkProducts();
        $this->checkFeutures();
        $this->checkValueFeaturesCategory();
    }

    private function checkProducts() {
        $products=Product::where('parent_id', $this->category)->where('display', 1)->get();

        foreach ($products as $p) {

            $dataProdpertyProduct = RetailFeatureValueProduct::where('product_uid', $p->uid)->get();
//            if(sizeof($dataProdpertyProduct)==0) {
//                dd($p);
//            }
            foreach($dataProdpertyProduct as $d){
                if($d->category_uid!=$p->parent_id) {
//                    dd($d, $p);

                    $fCheck=FeaturesRetailToCategoryRetail::where('category_retail_uid', $p->parent_id)
                        ->where('features_retail_uid', $d->feature_uid)->first();

                    if(empty($fCheck)) {
                        RetailFeatureValueProduct::where('category_uid', $d->category_uid)
                            ->where('product_uid', $d->product_uid)
                            ->where('feature_uid', $d->feature_uid)
                            ->where('value_uid', $d->value_uid)
                            ->where('value', $d->value)
                            ->delete();
                    }else{
                        RetailFeatureValueProduct::where('category_uid', $d->category_uid)
                            ->where('product_uid', $d->product_uid)
                            ->where('feature_uid', $d->feature_uid)
                            ->where('value_uid', $d->value_uid)
                            ->where('value', $d->value)
                            ->update([
                                'category_uid'=>$p->parent_id
                            ]);
                    }
                }
            }
        }
    }

    private function checkFeutures () {
        $data=FeaturesRetailToCategoryRetail::where('category_retail_uid', $this->category)->get();
        foreach ($data as $d) {
            $values=RetailFeatureValueProduct::where('category_uid', $this->category)
                ->where('feature_uid', $d->features_retail_uid)->get();
            if (sizeof($values)==0) {
                FeaturesRetailToCategoryRetail::where('category_retail_uid', $this->category)
                    ->where('features_retail_uid', $d->features_retail_uid)->delete();
            }
        }
    }

    private function checkValueFeaturesCategory () {
        $data=ValueFeatureToCategory::where('category_retail_uid', $this->category)->get();
        foreach ($data as $d) {
            $values=RetailFeatureValueProduct::where('category_uid', $this->category)
                ->where('feature_uid', $d->feature_retail_uid)
                ->where('value_uid', $d->feature_value_retail_uid)->get();
            if(sizeof($values)==0) {
                ValueFeatureToCategory::where('category_retail_uid', $this->category)
                    ->where('feature_retail_uid', $d->feature_retail_uid)
                    ->where('feature_value_retail_uid', $d->feature_value_retail_uid)
                    ->delete();
            }
        }
    }
}
