<?php

namespace App\Jobs;

use App\Traits\ApiReplyTrait;
use App\Traits\ProductTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PriceProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ProductTrait;
    use ApiReplyTrait;
    protected $data;
//
//Закупочная	1
//Мелкооптовая	2
//Крупнооптовая	3
//Розничная	4
//Базовая (опт)	5
//Цена И-М	6

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($prices)
    {
        $this->data=$prices;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        foreach ($this->data as $item) {
            try {
                if (empty($this->getTypePriceProduct($item->type))) {
                    Log::info('У товара с uid ' . $item->uid . ' обновление цены завершилось с ошибкой. Типа цены ' . $item->type . ' не обнаруженно', $item);
                } else {
                    $this->updatePrice($item, $this->data);
                }
            } catch (\Exception $e) {
                Log::error($item->uid . ' -  ошибка при обновлении цены' . $e->getMessage() . ' ' . json_encode($item));

            }
        }


    }
}
