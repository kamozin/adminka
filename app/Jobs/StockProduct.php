<?php

namespace App\Jobs;

use App\Traits\ApiReplyTrait;
use App\Traits\ProductTrait;
use App\Traits\ResponseTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class StockProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use ApiReplyTrait, ProductTrait, ResponseTrait;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($stocks)
    {
        $this->data=$stocks;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->data as $d) {
            try {
                $uidProduct = $this->getProductExternalId($d->uid, ['uid']);
                if (empty($uidProduct)) {
                    Log::error('Ошибка при обновлении остатков товар не найден - ' . $d->uid.' '.json_encode($d));
                }else{
                    $this->actionStock($uidProduct->uid, $d);
                }


            } catch (\Exception $e) {
                Log::error('Ошибка при обновлении остатков - ' . $d->uid.' '.$e->getMessage().' '.json_encode($d));
            }
        }
    }
}
