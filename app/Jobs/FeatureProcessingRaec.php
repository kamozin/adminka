<?php

namespace App\Jobs;

use App\Models\FeaturesRetail;
use App\Models\RetailFeatureValueProduct;
use App\Models\ValueFeaturesRetail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class FeatureProcessingRaec implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $f;
    protected $c;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($f, $c)
    {
        $this->c = $c;
        $this->f = $f;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $this->featureProcessingCategory($this->f, $this->c);
        $this->reduceToNumber($this->f, $this->c);
    }

    private function featureProcessingCategory($f, $c)
    {

        $values = ValueFeaturesRetail::where('features_retail_uid', $f->uid)
            ->get();

        $category = $c;

        foreach ($values as $v) {
            try {
                if (strpos($v->title, $f->unit_features) === false) {
                    DB::table('retail_feature_value_product')
                        ->where('category_uid', $category)
                        ->where('feature_uid', $f->uid)
                        ->where('value_uid', $v->uid)
                        ->update([
                            "value" => $v->title
                        ]);
                } else {

                    $title = trim(substr($v->title, 0, strpos($v->title, $f->unit_features)));

                    ValueFeaturesRetail::where('uid', $v->uid)->update(
                        [
                            "title" => $title
                        ]
                    );



                    DB::table('retail_feature_value_product')
                        ->where('category_uid', $category)
                        ->where('feature_uid', $f->uid)
                        ->where('value_uid', $v->uid)
                        ->update([
                            "value" => $title
                        ]);
                }
            } catch (\Exception $e) {

            }
        }
        $values = ValueFeaturesRetail::where('features_retail_uid', $f->uid)
            ->get();


        foreach ($values as $v) {
            try {
                if ($v->system == 'RAEC') {
                    $check = RetailFeatureValueProduct::where('value', $v->title)
                        ->where('feature_uid', $f->uid)
                        ->where('category_uid', $category)
                        ->where('system', '1С')
                        ->first();

                    if (!empty($check)) {

                        DB::table('retail_feature_value_product')
                            ->where('category_uid', $category)
                            ->where('feature_uid', $f->uid)
                            ->where('value', $v->title)
                            ->where('system', 'RAEC')
                            ->update([
                                "value_uid" => $check->value_uid
                            ]);
                    }
                }
            } catch (\Exception $e) {

            }

        }
    }

//    Приводим к числу uid фильтра uid категории
    private function reduceToNumber($f, $c)
    {
        $f = FeaturesRetail::where('uid', $f->uid)
            ->first();

        $values = RetailFeatureValueProduct::where('category_uid', $c)
            ->where('feature_uid', $f->uid)
            ->where('system', 'RAEC')
            ->get();

        if ($f->type == 'N') {
            foreach ($values as $v) {
                try {
                    RetailFeatureValueProduct::where('category_uid', $c)
                        ->where('feature_uid', $f->uid)
                        ->where('value_uid', $v->value_uid)
                        ->where('system', 'RAEC')
                        ->update([
                            'value' => intval($v->value)
                        ]);
                } catch (\Exception $e) {

                }


            }
        }
    }

}
