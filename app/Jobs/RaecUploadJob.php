<?php

namespace App\Jobs;

use App\Models\Features;
use App\Models\FeaturesRetail;
use App\Models\FeaturesRetailToEtim;
use App\Models\Product;
use App\Models\RetailFeatureValueProduct;
use App\Models\UploadProductsRaec;
use App\Models\ValueFeaturesRetail;
use App\Models\ValueFeatureToCategory;
use App\Traits\ProductTrait;
use App\Traits\RaecProductTrait;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RaecUploadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use RaecProductTrait, ProductTrait;

    protected $dataSku;
    protected $brand;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->dataSku = $data->sku;
        $this->brand = $data->brand;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $filter = 'Бренд = '.trim($this->brand).' AND КодПоставщика = ' . trim($this->dataSku) . '';
            $product = $this->getProductRql($filter);
            if(sizeof($product)==0) {
                $filter = 'Бренд = '.trim($this->brand).' AND АльтКодПоставщика = ' . trim($this->dataSku) . '';
                $product = $this->getProductRql($filter);
            }
            $modelProduct = new Product();
            if (sizeof($product) == 1) {

                $data = $modelProduct->getProductRaecId($product[0]->raecId);
                if (empty($data)) {
                    $data = $this->generateProductArray($product[0], 'RAEC');
                    $dataProduct = Product::create($data);

                    $category = $this->getCategoryRetail($product[0]->etimclass->id);

                    if (!empty($product[0]->brand)) {
                        $this->storeBrand('33459724-c5b1-4d03-a314-b2ae5f028d17', $category, $product[0]->brand->name, $dataProduct->uid);
                    }
                    if (!empty($product[0]->series)) {
                        $this->storeBrand('4a81d131-3947-4647-b060-3290802b72bb', $category, $product[0]->series->name, $dataProduct->uid);
                    }
                    if (!empty($product[0]->etimclass)) {
                        $this->storeBrand('09153d2a-6cf8-4e69-84ff-c8e3a1d7b980', $category, $product[0]->etimclass->id, $dataProduct->uid);
                    }

                    if (sizeof($product[0]->features) > 0) {
                        $this->featuresRaec($product[0]->features, $category, $dataProduct->uid);
                    }


                    $this->generateArrayPriceZero($dataProduct->uid);
                    $this->generateArrayStockZero($dataProduct->uid);


                    $modelUploadSku = new UploadProductsRaec ();
                    $modelUploadSku->saveUidProduct($this->dataSku, $dataProduct->uid);
                } else {
                    if ($data->system == '1C') {
                        $modelUploadSku = new UploadProductsRaec ();
                        $modelUploadSku->saveUidProduct($this->dataSku, $data->uid);
                    } else {
                        if (sizeof($product[0]->features) > 0) {
                            $category = $this->getCategoryRetail($product[0]->etimclass->id);
                            $this->featuresRaec($product[0]->features, $category, $data->uid);

                        }

                        $this->generateArrayPriceZero($data->uid);
                        $this->generateArrayStockZero($data->uid);

                        if (!empty($product[0]->brand)) {
                            $this->storeBrand('33459724-c5b1-4d03-a314-b2ae5f028d17', $category, $product[0]->brand->name, $data->uid);
                        }
                        if (!empty($product[0]->series)) {
                            $this->storeBrand('4a81d131-3947-4647-b060-3290802b72bb', $category, $product[0]->series->name, $data->uid);
                        }
                        if (!empty($product[0]->etimclass)) {
                            $this->storeBrand('09153d2a-6cf8-4e69-84ff-c8e3a1d7b980', $category, $product[0]->etimclass->id, $data->uid);
                        }


                        $modelUploadSku = new UploadProductsRaec ();
                        $modelUploadSku->saveUidProduct($this->dataSku, $data->uid);
                    }
                }
            } else {
                DB::table('upload_products_raec')->where('sku', $this->dataSku)->update([
                    "error" => 'Товар в РАЭК не найден артикул - '.$this->dataSku.', бренд - '.$this->brand
                ]);

            }

        } catch (\Exception $e) {
            DB::table('upload_products_raec')->where('sku', $this->dataSku)->update([
                "error" => $e->getMessage().' '.$this->dataSku
            ]);
        }
    }

    private function generateProductArray($data, $system)
    {
        $title = $this->getTitle($data);
        $product = [];
        $product['raec_id'] = $data->raecId;
        $product['title'] = (empty($title)) ? 'Нет наименования' : $title;
        $product['sku'] = $data->supplierId;
        $product['brand_uid'] = '17b9ddde-52fb-418e-bee3-10ebe2dbc672';
        $product['description'] = $data->descriptionAuto;
        $product['parent_id'] = $this->getCategoryRetail($data->etimclass->id);
        $product['multiplicity'] = $data->multyplicity;
        $product['unit_id'] = (isset($data->orderUnit)) ? $this->getUnit($data->orderUnit, 'RAEC') : 0;
        $product['display'] = 1;
        $product['system'] = $system;

        return $product;
    }

    private function featuresRaec($data, $category, $productUid)
    {
        foreach ($data as $d) {

            $check = FeaturesRetailToEtim::where('etimId', $d->id)->where('category_retail_uid', $category)
                ->first();

            if ($check) {
//              dd($check, $d, $category, $productUid);
                if ($d->type == 'A') {
                    $this->setType($check->features_retail_uid, 'A');
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    $value = $this->getValue($d->valueDescriptionRu, $check->features_retail_uid);
                    if ($value) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $value->uid, $d->valueDescriptionRu);
                    } else {
                        $valueUid = $this->storeValue($d->valueDescriptionRu, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $d->valueDescriptionRu);
                    }

                }
                if ($d->type == 'N') {
                    $this->setType($check->features_retail_uid, 'N');
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    $value = $this->getValue($d->value, $check->features_retail_uid);
                    if ($value) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $value->uid, $d->value);
                    } else {
                        $valueUid = $this->storeValue($d->value, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $d->value);
                    }
                }
                if ($d->type == 'L') {
                    $this->setType($check->features_retail_uid, 'L');
                    if ($d->value == 'false') {
                        $value = 'Нет';
                    } else if ($d->value == 'true') {
                        $value = 'Да';
                    }
                    $valueRes = $this->getValue($value, $check->features_retail_uid);
                    if ($valueRes) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueRes->uid, $value);
                    } else {
                        $valueUid = $this->storeValue($value, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $value);
                    }
                }
                if ($d->type == 'R') {
                    $this->setType($check->features_retail_uid, 'R');
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    $value = $this->getValue($d->value, $check->features_retail_uid);
                    if ($value) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $value->uid, $d->value);
                    } else {
                        $valueUid = $this->storeValue($d->value, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $d->value);
                    }
                }
            }


        }
    }

    private function setType($f, $type)
    {
        FeaturesRetail::where('uid', $f)
            ->update([
                "type" => $type
            ]);
    }

    private function setUnit($f, $unit)
    {
        FeaturesRetail::where('uid', $f)
            ->update([
                "unit_features" => $unit
            ]);
    }

    private function storeValue($title, $f, $id, $s)
    {
        $m = new ValueFeaturesRetail();
        $m->title = $title;
        $m->features_retail_uid = $f;
        $m->external_uid = $id;
        $m->system = $s;
        $m->value_external = $title;
        $m->save();
        return $m->uid;

    }

    private function storeFilter($feuture, $category, $product, $valueUid, $value)
    {
        $m = new ValueFeatureToCategory ();
        $m->category_retail_uid = $category;
        $m->feature_retail_uid = $feuture;
        $m->feature_value_retail_uid = $valueUid;
        $m->save();

        $model = new RetailFeatureValueProduct();
        $model->category_uid = $category;
        $model->product_uid = $product;
        $model->feature_uid = $feuture;
        $model->value_uid = $valueUid;
        $model->value = $value;
        $model->system = 'RAEC';
        $model->save();
    }


    private function storeBrand($feutureId, $category, $value, $productUid)
    {


        $checkValue = DB::table('feature_value_retail')->where('title', $value)
            ->where('features_retail_uid', $feutureId)->first();
        if (empty($checkValue)) {
            $model = new ValueFeaturesRetail();
            $model->title = $value;
            $model->features_retail_uid = $feutureId;
            $model->value_external = $value;
            $model->system = 'RAEC';
            $model->save();

            DB::table('retail_feature_value_product')->insert([
                "category_uid" => $category,
                "product_uid" => $productUid,
                "feature_uid" => $feutureId,
                "value_uid" => $model->uid,
                "value" => $value,
                "system" => 'RAEC'
            ]);
            DB::table('value_feature_retail_to_category')->insert([
                "category_retail_uid" => $category,
                "feature_retail_uid" => $feutureId,
                "feature_value_retail_uid" => $model->uid,
            ]);

        } else {
            if (empty($this->checkValueRelation($category, $productUid, $feutureId, $checkValue->uid))) {
                DB::table('retail_feature_value_product')->insert([
                    "category_uid" => $category,
                    "product_uid" => $productUid,
                    "feature_uid" => $feutureId,
                    "value_uid" => $checkValue->uid,
                    "value" => $checkValue->title,
                    "system" => 'RAEC'
                ]);
            }

            if (empty($this->checkValueRelationCategory($category, $feutureId, $checkValue->uid))) {
                DB::table('value_feature_retail_to_category')->insert([
                    "category_retail_uid" => $category,
                    "feature_retail_uid" => $feutureId,
                    "feature_value_retail_uid" => $checkValue->uid,
                ]);
            }

        }
    }

    private function checkValueRelationCategory($category, $feutureId, $valueUid)
    {
        $checkValueRelCategory = DB::table('value_feature_retail_to_category')
            ->where("category_retail_uid", $category)
            ->where("feature_retail_uid", $feutureId)
            ->where("feature_value_retail_uid", $valueUid)
            ->first();
        return $checkValueRelCategory;
    }

    private function checkValueRelation($category, $productUid, $feutureId, $valueUid)
    {
        $checkValueRel = DB::table('retail_feature_value_product')
            ->where("category_uid", $category)
            ->where("product_uid", $productUid)
            ->where("feature_uid", $feutureId)
            ->where("value_uid", $valueUid)
            ->first();
    }


    private function getValue($value, $feature)
    {
        return DB::table('feature_value_retail')->where('features_retail_uid', $feature)
            ->where('title', $value)->first();
    }
}
