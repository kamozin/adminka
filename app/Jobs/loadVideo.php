<?php

namespace App\Jobs;

use App\Models\Product;
use App\Traits\ProductTrait;
use App\Traits\RaecProductTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class loadVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use RaecProductTrait, ProductTrait;
    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dataProduct=$this->getProductRaecId($this->data->raec_id);
            Product::where('uid', $this->data->uid)
                ->update([
                    "video"=>$dataProduct->youtubeCode
                ]);
    }
}
