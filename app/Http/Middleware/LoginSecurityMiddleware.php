<?php

namespace App\Http\Middleware;

use App\Support\Google2FAAuthenticator;
use Closure;
use Illuminate\Support\Facades\Auth;

class LoginSecurityMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $authenticator = app(Google2FAAuthenticator::class)->boot($request);

        if ($authenticator->isAuthenticated()) {
            if (isset(Auth::user()->loginSecurity->google2fa_enable) && Auth::user()->loginSecurity->google2fa_enable == true) {
                session()->put('authGoogle', "authTrue");
            }
            return $next($request);
        }

        return $authenticator->makeRequestOneTimePasswordResponse();
    }
}
