<?php

namespace App\Http\Middleware;

use App\Traits\ApiReplyTrait;
use Closure;
use Illuminate\Support\Facades\Log;

class BasicAuth
{
    use ApiReplyTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('Authorization');

        $auth='Basic '.base64_encode(config('baseauthuser.users')[0][0].':'.config('baseauthuser.users')[0][1]);

        if ($auth===$header) {
            return $next($request);
        }
        return  $this->errorResponse('Доступ запрещен', 403);
    }
}
