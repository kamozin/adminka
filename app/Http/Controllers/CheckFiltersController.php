<?php

namespace App\Http\Controllers;

use App\Jobs\CheckProductsFeatures;
use App\Models\CategoryRetail;
use App\Models\FeaturesRetail;
use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\Product;
use App\Models\RetailFeatureValueProduct;
use App\Models\ValueFeatureToCategory;
use Illuminate\Http\Request;

class CheckFiltersController extends Controller
{
    public function __construct () {

    }


    public function setTaskCheckProducts () {
        $categories=CategoryRetail::where('parent_id', '<>', 0)->get();

        foreach ($categories as $category) {
            CheckProductsFeatures::dispatch($category->uid);
        }
    }


    public function checkProducts (Request $request) {

        $products=Product::where('parent_id', $request->category)->where('display', 1)->get();

        foreach ($products as $p) {

            $dataProdpertyProduct = RetailFeatureValueProduct::where('product_uid', $p->uid)->get();
//            if(sizeof($dataProdpertyProduct)==0) {
//                dd($p);
//            }
            foreach($dataProdpertyProduct as $d){
                if($d->category_uid!=$p->parent_id) {
//                    dd($d, $p);

                    $fCheck=FeaturesRetailToCategoryRetail::where('category_retail_uid', $p->parent_id)
                        ->where('features_retail_uid', $d->feature_uid)->first();

                    if(empty($fCheck)) {
                        RetailFeatureValueProduct::where('category_uid', $d->category_uid)
                            ->where('product_uid', $d->product_uid)
                            ->where('feature_uid', $d->feature_uid)
                            ->where('value_uid', $d->value_uid)
                            ->where('value', $d->value)
                            ->delete();
                    }else{
                        RetailFeatureValueProduct::where('category_uid', $d->category_uid)
                            ->where('product_uid', $d->product_uid)
                            ->where('feature_uid', $d->feature_uid)
                            ->where('value_uid', $d->value_uid)
                            ->where('value', $d->value)
                            ->update([
                                'category_uid'=>$p->parent_id
                            ]);
                    }


                }
            }
        }
    }

    public function checkFeutures (Request $request) {
        $data=FeaturesRetailToCategoryRetail::where('category_retail_uid', $request->category)->get();
        foreach ($data as $d) {
            $values=RetailFeatureValueProduct::where('category_uid', $request->category)
                ->where('feature_uid', $d->features_retail_uid)->get();
            if (sizeof($values)==0) {
                FeaturesRetailToCategoryRetail::where('category_retail_uid', $request->category)
                    ->where('features_retail_uid', $d->features_retail_uid)->delete();
            }
        }
    }

    public function checkValueFeaturesCategory (Request $request) {
        $data=ValueFeatureToCategory::where('category_retail_uid', $request->category)->get();
        foreach ($data as $d) {
            $values=RetailFeatureValueProduct::where('category_uid', $request->category)
                ->where('feature_uid', $d->feature_retail_uid)
                ->where('value_uid', $d->feature_value_retail_uid)->get();
            if(sizeof($values)==0) {
                ValueFeatureToCategory::where('category_retail_uid', $request->category)
                    ->where('feature_retail_uid', $d->feature_retail_uid)
                    ->where('feature_value_retail_uid', $d->feature_value_retail_uid)
                    ->delete();
            }
        }
    }

    public function dropValueFeaturesProductNotDisplay () {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        $products=Product::where('display', 0)->get();
        foreach ($products as $product) {
            RetailFeatureValueProduct::where('product_uid', $product->uid)
                ->delete();
        }
    }
}
