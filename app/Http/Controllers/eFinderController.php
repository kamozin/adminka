<?php

namespace App\Http\Controllers;

use App\Mail\OrderEmailAdmin;
use App\Mail\sendAbb;
use App\Models\Category;
use App\Models\CategoryRetail;
use App\Models\Product;
use App\Models\RetailFeatureValueProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Rap2hpoutre\FastExcel\FastExcel;

class eFinderController extends Controller
{
    public function __construct () {

    }

    public function index () {
        $filename=time().'.xls';
        // Export all users
        (new FastExcel($this->generateArray($this->getProducts())))->export($filename);
//        Storage::disk('finder')->put($filename, fopen($filename, 'r+'));
        Mail::to(
            [ 'hrolenkov@yandex.ru', 'datamaster@netcomponents.com']
        )->send(new sendAbb ($filename));
    }

    private function getProducts () {
        $data=RetailFeatureValueProduct::where('value_uid', '93278606-55d2-4255-a222-4d1ee3cd0e1d')
            ->groupBy('product_uid')->get()->pluck('product_uid');
        $query=Product::query();
        $query->whereIn('uid', $data);
        $query->with('burovaStock');
        return $query->get();
    }

    private function generateArray ($data) {
        $array=[];
        foreach ($data as $d) {
            if($d->burovaStock->stock>0){
                $arr=[];
                $arr['Manufacturer']='ABB';
                $arr['Part Number']=$d->sku;
                $arr['Quantity']=$d->burovaStock->stock;
                $arr['URL']='https://shop-aventa.ru/catalog/'.$this->generateLinkProduct($d);
                array_push($array, $arr);
            }
        }
        return $array;
    }

    private function generateLinkProduct ($product) {
        $category=CategoryRetail::where('uid', $product->parent_id)->first();
        $cat=CategoryRetail::where('uid', $category->parent_id)->first();
        return $cat->slug.'/'.$category->slug.'/'.$product->slug;
    }
}
