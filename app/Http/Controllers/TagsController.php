<?php

namespace App\Http\Controllers;

use App\Models\ListProductTags;
use App\Models\Tag;
use App\Models\TagList;
use App\Traits\ApiReplyTrait;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    use ApiReplyTrait;

    protected $modelTagList;

    public function __construct(TagList $model)
    {
        $this->modelTagList = $model;
    }

    public function saveTags(Request $request)
    {

        $dataRequest = $request->all();


        $tags = $this->modelTagList->select('tag')->where('list_id', $dataRequest['list'])->get()->pluck('tag');

        $tagsInput = [];
        foreach ($dataRequest['tags'] as $tag) {
            array_push($tagsInput, $tag['text']);
        }

        foreach ($tags as $tag) {
            if (!in_array($tag, $tagsInput)) {
//                dd($dataRequest['list'], $tag);
                $tagCheck = TagList::where('tag', trim($tag))
                    ->where('list_id', $dataRequest['list'])
                    ->first();

                TagList::where('tag', trim($tag))
                    ->where('list_id', $dataRequest['list'])
                    ->delete();

//                Удалить связку у товаров списка
                ListProductTags::where('tag', $tag)
                    ->where('list_id', $dataRequest['list'])
                    ->delete();
            }
        }


        foreach ($dataRequest['tags'] as $tag) {

            $check = $this->modelTagList->where('tag', trim($tag['text']))
                ->where('list_id', $dataRequest['list'])->first();
            if (empty($check)) {
                $modelTagList = new TagList();
                $modelTagList->tag = trim($tag['text']);
                $modelTagList->list_id = $dataRequest['list'];
                $modelTagList->save();
            }
        }
        return $this->successResponse();
    }

    public function saveTagsProduct(Request $request)
    {
        $dataRequest = $request->all();
//        dd($dataRequest);
        ListProductTags::where('list_id', $dataRequest['list_id'])
            ->where('product_uid', $dataRequest['product_uid'])
            ->delete();

        foreach ($dataRequest['tags'] as $tag) {
            $m = new ListProductTags();
            $m->product_uid = $dataRequest['product_uid'];
            $m->tag = $tag['text'];
            $m->list_id = $dataRequest['list_id'];
            $m->save();
        }
        return $this->successResponse();
    }
}
