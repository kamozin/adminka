<?php

namespace App\Http\Controllers;

use App\Models\FileType;
use App\Models\Product;
use App\Models\ProductFile;
use App\Traits\RaecProductTrait;
use Illuminate\Http\Request;

class FileController extends Controller
{
    use RaecProductTrait;

    public function __construct()
    {

    }

    public function index()
    {
        $products = Product::where('brandName', '<>', 'Не указан')->where('raec_id', '>', 0)
            ->where('sku', '<>', '')
            ->get();
        foreach ($products as $p) {
            $filter = 'Бренд = ' . trim($p->brandName) . ' AND КодПоставщика = ' . trim($p->sku) . '';
            $data = $this->getProductRql($filter);

            try{
                if (isset($data[0]->files) && sizeof($data[0]->files) > 0) {
                    ProductFile::where('product_uid', $p->uid)->delete();
                    foreach ($data[0]->files as $f) {
                        if (isset($f->url)) {
                            $dataFile = [
                                'product_uid' => $p->uid,
                                'url' => $f->url,
                                'size' => (isset($f->size)) ? $f->size : 0,
                                'mimetype' => (isset($f->mimetype)) ? $f->mimetype : '',
                                'type' => FileType::type($f->type),
                                'certificateValidityFrom' => (isset($f->certificateValidityFrom)) ? $f->certificateValidityFrom : '',
                                'certificateValidityTo' => (isset($f->certificateValidityTo)) ? $f->certificateValidityTo : ''];
                            ProductFile::create($dataFile);
                        }
                    }
                }
            }catch (\Exception $e){
                dd($p, $data);
            }

        }
    }
}
