<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{

    private $pathView='faq.';

    public function __construct()
    {

    }

    public function index () {

        $faq=Faq::all();

        return view($this->pathView.'index', compact('faq'));
    }

    public function create () {

        return view($this->pathView.'create');
    }

    public function store (Request $request) {
        $faq=new Faq();
        $faq->answer=$request->answer;
        $faq->question=$request->question;
        $faq->save();
        return redirect()->route('faq.index')->with('messageSuccess', 'FAQ добавлен');
    }

    public function edit (Request $request) {

        $faq=Faq::find($request->id);
        return view($this->pathView.'edit', compact('faq'));
    }

    public function update (Request $request) {

        $faq=Faq::find($request->id);
        $faq->answer=$request->answer;
        $faq->question=$request->question;
        $faq->save();
        return redirect()->route('faq.index')->with('messageSuccess', 'FAQ обнавлен');
    }

    public function drop (Request $request) {
        Faq::where('id', $request->id)->delete();
        return redirect()->route('faq.index')->with('messageSuccess', 'FAQ удален');
    }
}
