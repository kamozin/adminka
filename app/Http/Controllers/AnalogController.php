<?php

namespace App\Http\Controllers;

use App\Jobs\RaecUploadJob;
use App\Models\FileType;
use App\Models\Product;
use App\Models\ProductFile;
use App\Traits\RaecProductTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnalogController extends Controller
{
    use RaecProductTrait;

    public function index()
    {
        $products = Product::where('brandName', '<>', 'Не указан')->where('raec_id', '>', 0)
            ->where('sku', '<>', '')
            ->get();
        foreach ($products as $p) {
            $filter = 'Бренд = ' . trim($p->brandName) . ' AND КодПоставщика = ' . trim($p->sku) . '';
            $data = $this->getProductRql($filter);
            try{
                if (isset($data[0]->productRelated) && sizeof($data[0]->productRelated) > 0) {
                    DB::table('related_product')->where('product_uid', $p->uid)->delete();
                    foreach($data[0]->productRelated as $r) {
                        $productRelated=Product::where('brandName', $p->brandName)
                            ->where('raec_id', $r->raecId)
                            ->first();

                        if(!empty($productRelated)){
                             DB::table('related_product')
                                    ->insert([
                                        "product_uid"=>$p->uid,
                                        "related_product_uid"=>$productRelated->uid,
                                        "related_type"=>$r->related_type
                                    ]);
                        }else{
                            $raecProduct = $this->getProductRaecId($r->raecId);
                            if(!empty($raecProduct)){
                                DB::table('upload_products_raec')
                                    ->insert([
                                        "sku"=>$raecProduct->supplierId,
                                        "brand"=>$raecProduct->brand->name
                                    ]);
                                RaecUploadJob::dispatch((object)["sku"=>$raecProduct->supplierId, "brand"=>$raecProduct->brand->name]);
                            }
                        }
                    }
                }
            }catch (\Exception $e){

            }

        }
    }

    public function start () {
        $products = Product::where('brandName', '<>', 'Не указан')->where('raec_id', '>', 0)
            ->where('sku', '<>', '')
            ->get();
        foreach ($products as $p) {
            $filter = 'Бренд = ' . trim($p->brandName) . ' AND КодПоставщика = ' . trim($p->sku) . '';
            $data = $this->getProductRql($filter);
            try{
                if (isset($data[0]->productRelated) && sizeof($data[0]->productRelated) > 0) {
                    foreach ($data[0]->productRelated as $r) {
                        $productRelated = Product::where('brandName', $p->brandName)
                            ->where('raec_id', $r->raecId)
                            ->first();

                        if (!empty($productRelated)) {
                            $check= DB::table('related_product')
                                ->where("product_uid", "=", $p->uid)
                                ->where( "related_product_uid", "=", $productRelated->uid)
                                ->where( "related_type", '=', $r->related_type)
                                ->first();
                            if(empty($check)){
                                DB::table('related_product')
                                    ->insert([
                                        "product_uid" => $p->uid,
                                        "related_product_uid" => $productRelated->uid,
                                        "related_type" => $r->related_type
                                    ]);
                            }
                        }
                    }
                }
            }catch (\Exception $e){

            }

        }
    }
}
