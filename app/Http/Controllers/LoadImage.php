<?php

namespace App\Http\Controllers;

use App\Jobs\LoadImageBaseProduct;
use App\Jobs\TrimImage;
use App\Models\Product;
use App\Models\ProductImage;
use App\Traits\RaecProductTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class LoadImage extends Controller
{
    use RaecProductTrait;

    protected $raecId;
    protected $uid;

    public function start()
    {
        $files = Storage::allFiles('images');

        foreach ($files as $f) {
            $arr = explode('/', $f);
            $name = $arr[1];
            $file = Storage::get('images/' . $arr[1]);
            Storage::disk('s3')->put('files/products/' . $name, $file);
            Storage::delete('images/' . $name);
        }
    }

    public function setProductLoadImage()
    {
        $products = DB::table('products')
            ->leftJoin('products_image', 'products.uid', '=', 'products_image.product_uid')
            ->whereNull('products_image.product_uid')
            ->where('products.brandName', '<>', 'Не указан')
            ->where('products.sku', '<>', '')
            ->where('products.display', 1)
            ->select('products.*')
            ->get();

        foreach ($products as $p) {
            DB::table('load_image')
                ->insert([
                    "product_uid" => $p->uid,
                    "raec_id" => $p->raec_id,
                    "brand" => $p->brandName,
                    "sku" => $p->sku,
                    "status" => 0,
                ]);
        }
    }

    public function loadImg()
    {
        $products = Product::where('system', '1C')
            ->where('raec_id', '>', 0)
            ->where('imgLoad', 0)
            ->where('system', '1C')
            ->where(function ($query) {
                $query->has('images', '=', 1)
                    ->orWhereDoesntHave('images');
            })

            ->get();

        foreach ($products as $p) {
//               $this->raecId=$p->raec_id;
//               $this->uid=$p->uid;
//               $this->handleTest();
//               array_push($arr, $p->uid);
            LoadImageBaseProduct::dispatch($p->raec_id, $p->uid);

        }

    }

    public function loadImageUrl()
    {

        $this->uploadRaecImage('https://catalog.raec.su/media/images/263/263769205f239879af17a46c4084683d.jpg', '263769205f239879af17a46c4084683d.jpg');
    }


    private function handleTest()
    {
        $data = $this->getProductRaecId($this->raecId);

        try {
            $p = Product::where('uid', $this->uid)->first();
            if (sizeof($data->gallery) > 0) {
                foreach ($data->gallery as $g) {
                    $arrImage = $this->image($g);
                    if (!isset($arrImage['name']) && !isset($arrImage['url'])) {
                        continue;
                    }
                    if (!$this->checkImage($p->uid, $arrImage)) {
                        if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                            try {
                                $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                                $id = $this->saveImage($arrImage, $p->uid);

                                $this->trim($arrImage['name'], $id);
                            } catch (\Exception $e) {

                            }
                        }
                    }
                }

            } else {
                $arrImage = $this->image($data->image);


                if (!$this->checkImage($p->uid, $arrImage)) {
                    if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                        try {
                            $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                            $id = $this->saveImage($arrImage, $p->uid);
                            $this->trim($arrImage['name'], $id);
                        } catch (\Exception $e) {

                        }
                    }
                }
            }
            Product::where('uid', $p->uid)
                ->update([
                    'imgLoad' => 1
                ]);
        } catch (\Exception $e) {
//            dd($e->getMessage());
            Product::where('uid', $p->uid)
                ->update([
                    'imgLoad' => 2
                ]);
        }
    }


    public function loadImgProduct(Request $request)
    {
        $product = Product::where($request->field, $request->value)
            ->with('images')
            ->get();
        foreach ($product as $p) {
            foreach ($p->images as $i) {
                TrimImage::dispatch($i->name, $i->id);
            }
        }
    }

    private function handleRaec($raecId, $uid)
    {
        $data = $this->getProductRaecId($raecId);

        try {
            $p = Product::where('uid', $uid)->first();
            if (sizeof($data->gallery) > 0) {
                foreach ($data->gallery as $g) {
                    $arrImage = $this->image($g);
                    if (!isset($arrImage['name']) && !isset($arrImage['url'])) {
                        continue;
                    }
                    if (!$this->checkImage($p->uid, $arrImage)) {
                        if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                            try {
                                $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                                $id = $this->saveImage($arrImage, $p->uid);

                                $this->trim($arrImage['name'], $id);
                            } catch (\Exception $e) {

                            }
                        }
                    }
                }

            } else {
                $arrImage = $this->image($data->image);
//                        dd($data, 'image');
                if (!isset($arrImage['name']) && !isset($arrImage['url'])) {

                }
                if (!$this->checkImage($p->uid, $arrImage)) {
                    if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                        try {
                            $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                            $id = $this->saveImage($arrImage, $p->uid);
                            $this->trim($arrImage['name'], $id);
                        } catch (\Exception $e) {

                        }
                    }
                }
            }
            Product::where('uid', $p->uid)
                ->update([
                    'imgLoad' => 1
                ]);
        } catch (\Exception $e) {
            Product::where('uid', $p->uid)
                ->update([
                    'imgLoad' => 2
                ]);
        }
    }

    public function startRaec(Request $request)
    {
//        $products = Product::where('raec_id', ">", 0)
//            ->where('brandName', '<>', 'Не указан')
//            ->where('sku', '<>', '')
//            ->get();
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        set_time_limit(8000000);

//        $products = DB::table('load_image')
//            ->where('status', '0')
//            ->limit(1000)
//            ->get();
        $products = DB::table('products')
            ->where('raec_id', $request->id)
            ->get();
//      $products = DB::table('products')
//            ->where('system', '1C')
//            ->where('brandName', '<>', 'Не указан')
//            ->where('sku', '<>', '')
//            ->limit(50)
//            ->get();


        foreach ($products as $p) {
            $filter = 'Бренд = ' . trim($p->brandName) . ' AND КодПоставщика = ' . trim($p->sku) . '';
            $data = $this->getProductRql($filter);

            try {
                if (sizeof($data) > 0) {
                    if (sizeof($data[0]->gallery) > 0) {
                        foreach ($data[0]->gallery as $g) {
                            $arrImage = $this->image($g);
                            if (!isset($arrImage['name']) && !isset($arrImage['url'])) {
                                continue;
                            }
                            if (!$this->checkImage($p->uid, $arrImage)) {
                                if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                                    try {
                                        $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                                        $id = $this->saveImage($arrImage, $p->uid);

                                        $this->trim($arrImage['name'], $id);
                                    } catch (\Exception $e) {
                                        dd($e->getMessage(), 1);
                                        DB::table('load_image')
                                            ->where('product_uid', $p->uid)
                                            ->update([
                                                "error" => $e->getMessage(),
                                            ]);
                                    }
                                }
                            }
                        }
                        DB::table('load_image')
                            ->where('product_uid', $p->uid)
                            ->update([
                                "status" => 1,
                                "count_image" => sizeof($data[0]->gallery)
                            ]);
                    } else {
                        $arrImage = $this->image($data[0]->image);
//                        dd($data, 'image');
                        if (!isset($arrImage['name']) && !isset($arrImage['url'])) {
                            continue;
                        }
                        if (!$this->checkImage($p->uid, $arrImage)) {
                            if (!$this->checkBlackList($p->raec_id, $p->uid, $arrImage['url'])) {
                                try {
                                    $this->uploadRaecImage($arrImage['url'], $arrImage['name']);
                                    $id = $this->saveImage($arrImage, $p->uid);
                                    $this->trim($arrImage['name'], $id);
                                } catch (\Exception $e) {
                                    DB::table('load_image')
                                        ->where('product_uid', $p->uid)
                                        ->update([
                                            "error" => $e->getMessage(),
                                        ]);
                                }
                            }
                        }
                        DB::table('load_image')
                            ->where('product_uid', $p->uid)
                            ->update([
                                "status" => 1,
                                "count_image" => 1
                            ]);
                    }
                }
            } catch (\Exception $e) {
                dd($data);
                continue;
            }
        }
    }

    public function original(Request $request)
    {
        $originalImageUrl = 'https://s3.timeweb.com/1dafdd54-ac71a2de-0e06-4dc9-8dbc-1992d37c2815/files/products/' . $request->name;
        // Storage::disk('s3')->delete('test/original/110591.png');
        // Получаем оригинальное изображение с Amazon S3
        $response = Http::get($originalImageUrl);
        $image = Image::make($response->body());
        // Получаем имя файла без пути
        $originalFileName = pathinfo(\request()->name, PATHINFO_BASENAME);

        $path = 'test' . '/original/' . $originalFileName;


        $extension = pathinfo($originalImageUrl, PATHINFO_EXTENSION);
        Storage::disk('s3')->put($path, (string)$image->encode($extension, 50));
        return true;
    }

    private function uploadRaecImage($url, $name)
    {
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $contents = file_get_contents($url, false, stream_context_create($arrContextOptions));
        Storage::disk('s3')->put('files/products/' . $name, $contents);
    }

    private function trim($name, $id)
    {
        // URL оригинального изображения на Amazon S3
        $originalImageUrl = 'https://s3.timeweb.com/1dafdd54-ac71a2de-0e06-4dc9-8dbc-1992d37c2815/files/products/' . $name;

        // Получаем оригинальное изображение с Amazon S3
        $response = Http::get($originalImageUrl);

        // Проверяем успешность запроса
        if ($response->successful()) {
            // Открываем изображение с помощью Intervention Image
            $image = Image::make($response->body());

            // Получаем имя файла без пути
            $originalFileName = pathinfo($name, PATHINFO_BASENAME);

            // Нарезка и сохранение изображения для разных разрешений
            $resolutions = [400, 60, 80]; // Здесь вы можете указать нужные вам разрешения
            $resolutionsCart = [80, 60];
            $resizedImage = $image->backup();
            $width = $resizedImage->width();
            $original = $image->backup();
            $path = 'test' . '/original/' . $originalFileName;
            $extension = pathinfo($originalImageUrl, PATHINFO_EXTENSION);
            Storage::disk('s3')->put($path, (string)$original->encode($extension, 50));
            if ($width >= 600) {
                foreach ($resolutions as $resolution) {
                    $original = $image->backup();
                    $original->resize($resolution, $resolution, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $path = 'test/' . $resolution . '/' . $originalFileName;
                    Storage::disk('s3')->put($path, (string)$original->encode($extension, 75));
                }
            } else {
                foreach ($resolutionsCart as $resolutionC) {
                    $original = $image->backup();
                    $original->resize($resolutionC, $resolutionC, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    // Сохранение нарезанного изображения на Amazon S3
                    $path = 'test/' . $resolutionC . '/' . $originalFileName;
                    Storage::disk('s3')->put($path, (string)$image->encode($extension, 75));
                }
            }
            ProductImage::where('id', $id)
                ->update([
                    "trimStatus" => 1
                ]);

        } else {
            ProductImage::where('id', $id)
                ->update([
                    "trimStatus" => 2
                ]);
        }
    }

    private function checkBlackList($raec_id, $uid_product, $image)
    {
        return DB::table('blacklist_image')
            ->where('raec_id', $raec_id)
            ->where('uid_product', $uid_product)
            ->where('image', $image)
            ->first();
    }

    private function saveImage($arr, $uid)
    {
        $model = new ProductImage();
        $model->name = $arr['name'];
        $model->main = 1;
        $model->product_uid = $uid;
        $model->display = 1;
        $model->system = 'RAEC';
        $model->url_raec = $arr['url'];
        $model->save();
        return $model->id;
    }

    private function image($data): array
    {
        $image = '';
        if (isset($data->{'60'})) {
            $image = $data->{'60'};
        }
        if (isset($data->{'150'})) {
            $image = $data->{'150'};
        }
        if (isset($data->{'600'})) {
            $image = $data->{'600'};
        }
        if (isset($data->{'max'})) {
            $image = $data->{'max'};
        }
        if (!empty($image)) {
            $url = $image;
            $name = substr($url, strrpos($url, '/') + 1);
            return [
                "name" => $name,
                "url" => $url
            ];
        }
        return [];
    }

    private function checkImage($uid, $arr)
    {
        return ProductImage::where('product_uid', $uid)
            ->where('url_raec', $arr['url'])->first();
    }
}
