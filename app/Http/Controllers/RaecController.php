<?php

namespace App\Http\Controllers;

use App\Jobs\CheckModelImage;
use App\Jobs\FeatureProcessingRaec;
use App\Jobs\loadImageOne;
use App\Jobs\RaecUploadImage;
use App\Jobs\RaecUploadJob;
use App\Jobs\setJobLoadImage;
use App\Jobs\setLoadVideo;
use App\Jobs\setTaskLoadImageOne;
use App\Models\FeaturesRetail;
use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\FeaturesRetailToEtim;
use App\Models\Product;
use App\Models\ProductFile;
use App\Models\ProductImage;
use App\Models\RetailFeatureValueProduct;
use App\Models\UnitFeatures;
use App\Models\UploadProductsRaec;
use App\Models\ValueFeaturesRetail;
use App\Models\ValueFeatureToCategory;
use App\Traits\ProductTrait;
use App\Traits\RaecProductTrait;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Jenssegers\ImageHash\ImageHash;
use Jenssegers\ImageHash\Implementations\DifferenceHash;
use Rap2hpoutre\FastExcel\FastExcel;

class RaecController extends Controller
{
    use RaecProductTrait, ProductTrait;

    protected $dataSku;

    public function __construct()
    {

    }

    public function index(Request $request)
    {
//        $result = $this->getProductRaecId($request->raecId);
//        dd($result);
    }

    public function getUploadFile(Request $request)
    {
        $collection = (new FastExcel)->import($request->file('fileraec'));

        try {
            \DB::beginTransaction();
            foreach ($collection as $c) {

                $model = new UploadProductsRaec();
                $model->sku = $c['Код производителя'];
                $model->brand = $c['Бренд'];
                $model->seria = $c['Серия'];
                $model->save();
            }
            \DB::commit();
        } catch (\Throwable $e) {
            \DB::rollback();
        }
        $modelUploadSku = new UploadProductsRaec ();
        $dataSku = $modelUploadSku->getSkuFromLoad();
        foreach ($dataSku as $s) {
            RaecUploadJob::dispatch($s);
//            $this->handle($s);
        }
        return redirect()->route('raec.ListProductUpload');
    }

    public function loadRaecSku()
    {
        try {
//            $filter = 'Бренд = ABB AND КодПоставщика = ' . trim('2CMA101077R1000') . '';
//            $product = $this->getProductRql($filter);
//            dd($product[0]);
//            if(!empty($product[0]->features[0]->unit)){
//                dd(1);
//            }

//            $str='16 А';
//            $u='А';
//            (strpos($str, $u));
            $modelUploadSku = new UploadProductsRaec ();
            $dataSku = $modelUploadSku->getSkuFromLoad();
            foreach ($dataSku->pluck('sku') as $s) {
//                $filter = 'Бренд = ABB AND КодПоставщика = ' . trim('2CMA101077R1000') . '';
//                $product = $this->getProductRql($filter);
//                dd($product);
                $this->handle($s);
            }
        } catch (Exception $e) {
//dd($e);
        }
    }


    private function handle($s)
    {
        try {
            $this->dataSku = $s;
            $filter = 'Бренд = ABB AND КодПоставщика = ' . trim($this->dataSku) . '';
            $product = $this->getProductRql($filter);

            $modelProduct = new Product();
            if (sizeof($product) == 1) {

                $data = $modelProduct->getProductRaecId($product[0]->raecId);
                if (empty($data)) {
                    $data = $this->generateProductArray($product[0], 'RAEC');
                    $dataProduct = Product::create($data);

                    $category = $this->getCategoryRetail($product[0]->etimclass->id);

                    if (!empty($product[0]->brand)) {
                        $this->storeBrand('33459724-c5b1-4d03-a314-b2ae5f028d17', $category, $product[0]->brand->name, $dataProduct->uid);
                    }
                    if (!empty($product[0]->series)) {
                        $this->storeBrand('4a81d131-3947-4647-b060-3290802b72bb', $category, $product[0]->series->name, $dataProduct->uid);
                    }
                    if (!empty($product[0]->etimclass)) {
                        $this->storeBrand('09153d2a-6cf8-4e69-84ff-c8e3a1d7b980', $category, $product[0]->etimclass->id, $dataProduct->uid);
                    }

                    if (sizeof($product[0]->features) > 0) {
                        $this->featuresRaec($product[0]->features, $category, $dataProduct->uid);
                    }

                    $image = $this->image($product[0]);
                    if (!empty($image)) {
                        $this->saveImage($image, $dataProduct->uid);
                    }

                    $this->saveImage($image, $dataProduct->uid);
                    $this->generateArrayPriceZero($dataProduct->uid);
                    $this->generateArrayStockZero($dataProduct->uid);
                    $this->files($product, $dataProduct->uid);


                    $modelUploadSku = new UploadProductsRaec ();
                    $modelUploadSku->saveUidProduct($this->dataSku, $dataProduct->uid);
                } else {

                    if ($data->system == '1C') {
                        $modelUploadSku = new UploadProductsRaec ();
                        $modelUploadSku->saveUidProduct($this->dataSku, $data->uid);
                    } else {
                        if (sizeof($product[0]->features) > 0) {
                            $category = $this->getCategoryRetail($product[0]->etimclass->id);
                            $this->featuresRaec($product[0]->features, $category, $data->uid);

                        }

                        $image = $this->image($product[0]);
                        if (!empty($image)) {
                            $this->saveImage($image, $data->uid);
                        }
                        $this->generateArrayPriceZero($data->uid);
                        $this->generateArrayStockZero($data->uid);
                        $this->files($product, $data->uid);


                        if (!empty($product[0]->brand)) {
                            $this->storeBrand('33459724-c5b1-4d03-a314-b2ae5f028d17', $category, $product[0]->brand->name, $data->uid);
                        }
                        if (!empty($product[0]->series)) {
                            $this->storeBrand('4a81d131-3947-4647-b060-3290802b72bb', $category, $product[0]->series->name, $data->uid);
                        }
                        if (!empty($product[0]->etimclass)) {
                            $this->storeBrand('09153d2a-6cf8-4e69-84ff-c8e3a1d7b980', $category, $product[0]->etimclass->id, $data->uid);
                        }


                        $modelUploadSku = new UploadProductsRaec ();
                        $modelUploadSku->saveUidProduct($this->dataSku, $data->uid);
                    }
                }
            } else {
                DB::table('upload_products_raec')->update([
                    "error" => 'Товар в РАЭК не найден'
                ])->where('sku', $this->dataSku);

            }

        } catch (\Exception $e) {
            dd($e->getMessage(), $s);
            DB::table('upload_products_raec')
                ->where('sku', $s)
                ->update([
                    "error" => $e->getMessage()
                ]);
        }
    }

    private function generateProductArray($data, $system)
    {
        $title = $this->getTitle($data);
        $product = [];
        $product['raec_id'] = $data->raecId;
        $product['title'] = (empty($title)) ? 'Нет наименования' : $title;
        $product['sku'] = $data->supplierId;
        $product['brand_uid'] = '17b9ddde-52fb-418e-bee3-10ebe2dbc672';
        $product['description'] = $data->descriptionAuto;
        $product['parent_id'] = $this->getCategoryRetail($data->etimclass->id);
        $product['multiplicity'] = $data->multyplicity;
        $product['unit_id'] = (isset($data->orderUnit)) ? $this->getUnit($data->orderUnit, 'RAEC') : 0;
        $product['display'] = 1;
        $product['system'] = $system;

        return $product;
    }

    private function featuresRaec($data, $category, $productUid)
    {
        foreach ($data as $d) {

            $check = FeaturesRetailToEtim::where('etimId', $d->id)->where('category_retail_uid', $category)
                ->first();

            if ($check) {
//              dd($check, $d, $category, $productUid);
                if ($d->type == 'A') {
                    $this->setType($check->features_retail_uid, 'A');
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    $value = $this->getValue($d->valueDescriptionRu, $check->features_retail_uid);
                    if ($value) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $value->uid, $d->valueDescriptionRu);
                    } else {
                        $valueUid = $this->storeValue($d->valueDescriptionRu, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $d->valueDescriptionRu);
                    }

                }
                if ($d->type == 'N') {
                    $this->setType($check->features_retail_uid, 'N');
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    $value = $this->getValue($d->value, $check->features_retail_uid);
                    if ($value) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $value->uid, $d->value);
                    } else {
                        $valueUid = $this->storeValue($d->value, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $d->value);
                    }
                }
                if ($d->type == 'L') {
                    $this->setType($check->features_retail_uid, 'L');
                    if ($d->value == 'false') {
                        $value = 'Нет';
                    } else if ($d->value == 'true') {
                        $value = 'Да';
                    }
                    $valueRes = $this->getValue($value, $check->features_retail_uid);
                    if ($valueRes) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueRes->uid, $value);
                    } else {
                        $valueUid = $this->storeValue($value, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $value);
                    }
                }
                if ($d->type == 'R') {
                    $this->setType($check->features_retail_uid, 'R');
                    if (!empty($d->unit)) {
                        $this->setUnit($check->features_retail_uid, $d->unit->descriptionRu);
                    }
                    $value = $this->getValue($d->value, $check->features_retail_uid);
                    if ($value) {
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $value->uid, $d->value);
                    } else {
                        $valueUid = $this->storeValue($d->value, $check->features_retail_uid, $d->id, 'RAEC');
                        $this->storeFilter($check->features_retail_uid, $category, $productUid, $valueUid, $d->value);
                    }
                }
            }


        }
    }

    private function setType($f, $type)
    {
        FeaturesRetail::where('uid', $f)
            ->update([
                "type" => $type
            ]);
    }

    private function setUnit($f, $unit)
    {
        FeaturesRetail::where('uid', $f)
            ->update([
                "unit_features" => $unit
            ]);
    }

    private function storeValue($title, $f, $id, $s)
    {
        $m = new ValueFeaturesRetail();
        $m->title = $title;
        $m->features_retail_uid = $f;
        $m->external_uid = $id;
        $m->system = $s;
        $m->value_external = $title;
        $m->save();
        return $m->uid;

    }

    private function storeFilter($feuture, $category, $product, $valueUid, $value)
    {
        $m = new ValueFeatureToCategory ();
        $m->category_retail_uid = $category;
        $m->feature_retail_uid = $feuture;
        $m->feature_value_retail_uid = $valueUid;
        $m->save();

        $model = new RetailFeatureValueProduct();
        $model->category_uid = $category;
        $model->product_uid = $product;
        $model->feature_uid = $feuture;
        $model->value_uid = $valueUid;
        $model->value = $value;
        $model->system = 'RAEC';
        $model->save();
    }


    private function storeBrand($feutureId, $category, $value, $productUid)
    {


        $checkValue = DB::table('feature_value_retail')->where('title', $value)
            ->where('features_retail_uid', $feutureId)->first();
        if (empty($checkValue)) {
            $model = new ValueFeaturesRetail();
            $model->title = $value;
            $model->features_retail_uid = $feutureId;
            $model->value_external = $value;
            $model->system = 'RAEC';
            $model->save();

            DB::table('retail_feature_value_product')->insert([
                "category_uid" => $category,
                "product_uid" => $productUid,
                "feature_uid" => $feutureId,
                "value_uid" => $model->uid,
                "value" => $value,
                "system" => 'RAEC'
            ]);
            DB::table('value_feature_retail_to_category')->insert([
                "category_retail_uid" => $category,
                "feature_retail_uid" => $feutureId,
                "feature_value_retail_uid" => $model->uid,
            ]);

        } else {
            if (empty($this->checkValueRelation($category, $productUid, $feutureId, $checkValue->uid))) {
                DB::table('retail_feature_value_product')->insert([
                    "category_uid" => $category,
                    "product_uid" => $productUid,
                    "feature_uid" => $feutureId,
                    "value_uid" => $checkValue->uid,
                    "value" => $checkValue->title,
                    "system" => 'RAEC'
                ]);
            }

            if (empty($this->checkValueRelationCategory($category, $feutureId, $checkValue->uid))) {
                DB::table('value_feature_retail_to_category')->insert([
                    "category_retail_uid" => $category,
                    "feature_retail_uid" => $feutureId,
                    "feature_value_retail_uid" => $checkValue->uid,
                ]);
            }

        }
    }

    private function checkValueRelationCategory($category, $feutureId, $valueUid)
    {
        $checkValueRelCategory = DB::table('value_feature_retail_to_category')
            ->where("category_retail_uid", $category)
            ->where("feature_retail_uid", $feutureId)
            ->where("feature_value_retail_uid", $valueUid)
            ->first();
        return $checkValueRelCategory;
    }

    private function checkValueRelation($category, $productUid, $feutureId, $valueUid)
    {
        $checkValueRel = DB::table('retail_feature_value_product')
            ->where("category_uid", $category)
            ->where("product_uid", $productUid)
            ->where("feature_uid", $feutureId)
            ->where("value_uid", $valueUid)
            ->first();
    }


    private function getValue($value, $feature)
    {
        return DB::table('feature_value_retail')->where('features_retail_uid', $feature)
            ->where('title', $value)->first();
    }







//    private function generateProductArray($data, $system)
//    {
//        $product = [];
//        $product['raec_id'] = $data->raecId;
//        $product['title'] = $this->getTitle($data);
//        $product['sku'] = $data->supplierId;
//        $product['brand_uid'] = '17b9ddde-52fb-418e-bee3-10ebe2dbc672';
//        $product['description'] = $data->descriptionAuto;
//        $product['parent_id'] = $this->getCategoryRetail($data->etimclass->id);
//        $product['multiplicity'] = $data->multyplicity;
//        $product['unit_id'] = $this->getUnit($data->orderUnit);
//        $product['display'] = 1;
//        $product['system'] = $system;
//
//        return $product;
//    }


    public function featureRaecProcessing(Request $request)
    {
        $arrayF = FeaturesRetail::whereNotNull('unit_features')->get();
        foreach ($arrayF as $f) {
            $fData = FeaturesRetail::where('uid', $f->uid)->first();
            $fDataCategories = FeaturesRetailToCategoryRetail::where('features_retail_uid', $f->uid)->get();
//            dd($fDataCategories);
            if (sizeof($fDataCategories) > 0) {
                foreach ($fDataCategories->pluck('category_retail_uid') as $c) {
                    FeatureProcessingRaec::dispatch($fData, $c);
                }
            }

        }
    }

    public function featureProcessingCategory(Request $request)
    {
        $values = ValueFeaturesRetail::where('features_retail_uid', $request->uid)
            ->get();


        $category = $request->c;
        $f = FeaturesRetail::where('uid', $request->uid)->first();

        foreach ($values as $v) {

            if (strpos($v->title, $f->unit_features) === false) {

            } else {

                $title = trim(substr($v->title, 0, strpos($v->title, $f->unit_features)));
                ValueFeaturesRetail::where('uid', $v->uid)->update(
                    [
                        "title" => $title
                    ]
                );

                DB::table('retail_feature_value_product')
                    ->where('category_uid', $category)
                    ->where('feature_uid', $f->uid)
                    ->where('value_uid', $v->uid)
                    ->update([
                        "value" => $title
                    ]);
            }
        }
        $values = ValueFeaturesRetail::where('features_retail_uid', $f->uid)
            ->get();


        foreach ($values as $v) {
            if ($v->system == 'RAEC') {
                $check = RetailFeatureValueProduct::where('value', $v->title)
                    ->where('feature_uid', $f->uid)
                    ->where('category_uid', $category)
                    ->where('system', '1С')
                    ->first();

                if (!empty($check)) {

                    DB::table('retail_feature_value_product')
                        ->where('category_uid', $category)
                        ->where('feature_uid', $f->uid)
                        ->where('value', $v->title)
                        ->where('system', 'RAEC')
                        ->update([
                            "value_uid" => $check->value_uid
                        ]);
                }
            }
        }
    }

//    Приводим к числу uid фильтра uid категории
    public function reduceToNumber(Request $request)
    {
        $f = FeaturesRetail::where('uid', $request->uid)
            ->first();

        $values = RetailFeatureValueProduct::where('category_uid', $request->c)
            ->where('feature_uid', $request->uid)
            ->where('system', 'RAEC')
            ->get();

        if ($f->type == 'N') {
            foreach ($values as $v) {

                RetailFeatureValueProduct::where('category_uid', $request->c)
                    ->where('feature_uid', $f->uid)
                    ->where('value_uid', $v->value_uid)
                    ->where('system', 'RAEC')
                    ->update([
                        'value' => intval($v->value)
                    ]);
            }
        }
    }


    public function dropProductRaecCategory(Request $request)
    {
        ini_set('max_execution_time', 80010);
        $products = Product::where('system', 'RAEC')
            ->where('parent_id', '64b25d08-c0cf-4232-b95c-a871c5d17080')
            ->get();


        foreach ($products as $p) {
            RetailFeatureValueProduct::where('product_uid', $p->uid)->delete();
            Product::where('uid', $p->uid)->delete();
        }
    }


    public function dropProducts(Request $request)
    {

    }

    public function dropProduct(Request $request)
    {

    }

    public function loadImage()
    {
        $data = Product::where('raec_id', '<>', 0)
            ->get();
        foreach ($data as $d) {
            RaecUploadImage::dispatch($d);
        }
    }

    public function uploadImageShop(Request $request)
    {

//        $hasher = new ImageHash(new DifferenceHash());
//        $hash1 = $hasher->hash('https://aventa.su/files/products/7e9627ac8b42b6e2402007806b677a91.jpg');
//        $hash2 = $hasher->hash('https://aventa.su/files/products/0c575e3c526ba7f734c36e749551aeac.jpg');
//
////        dd($hash1);
//
////        $distance = $hasher->distance($hash1, $hash2);
//        $distance = $hash1->distance($hash2);
//        dd($distance);


        $dataProduct = $this->getProductRaecId($request->id);
        $product = Product::where('raec_id', $request->id)
            ->first();
        $images = ProductImage::where('product_uid', $product->uid)->get();


        if (sizeof($images) == 0 || (sizeof($images) == 1 && $images[0]->name == 'no-photo.png')) {
            if (!empty($dataProduct->image)) {
                $name = $this->image($dataProduct->image);
                $this->saveImage($name, $product->uid);
            }


            if (sizeof($dataProduct->gallery) > 0) {
                foreach ($dataProduct->gallery as $g) {
                    $name = $this->image($g);
                    $this->saveImage($name, $product->uid);
                }
            }
        }

        return $request->id;
    }

    public function uploadImageShopJob(Request $request)
    {

        $products=Product::where('system', 'RAEC')
            ->get();

        foreach ($products as $p){
            $dataProduct = $this->getProductRaecId($p->raec_id);
            $product = Product::where('raec_id', $p->raec_id)
                ->first();
            $images = ProductImage::where('product_uid', $product->uid)->get();
            if (sizeof($images) == 0 || (sizeof($images) == 1 && $images[0]->name == 'no-photo.png')) {
                if (!empty($dataProduct->image)) {
                    $name = $this->image($dataProduct->image);
                    $this->saveImage($name, $product->uid);
                }


                if (sizeof($dataProduct->gallery) > 0) {
                    foreach ($dataProduct->gallery as $g) {
                        $name = $this->image($g);
                        $this->saveImage($name, $product->uid);
                    }
                }
            }
        }


    }

    public function loadImageTask()
    {

        setJobLoadImage::dispatch();
    }

    public function loadImageOne()
    {
        setTaskLoadImageOne::dispatch();
    }


    public function loadImageV()
    {
        $data = Product::where('raec_id', '<>', 0)
            ->where('system', 'RAEC')
            ->get();


        foreach ($data as $d) {
            $dataProduct = $this->getProductRaecId($d->raec_id);
            dd($dataProduct);
//            CheckModelImage::dispatch($d);
        }
    }

    public function video()
    {
        setLoadVideo::dispatch();
    }

    public function getRaecProduct (Request $request) {
        if($request->type=='ids'){
            $data=$this->getProductRaecId($request->id);
        }else{
            $filter = 'Бренд = '.trim($request->brand).' AND АльтКодПоставщика = ' . trim($request->sku) . '';

           $data=$this->getProductRql( $filter);
        }


        dd($data);
    }
}
