<?php

namespace App\Http\Controllers;

use App\Models\BrandMainImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BrandMainPageController extends Controller
{
    private $pathView = 'brand_main_page.';

    public function __construct()
    {

    }

    public function index()
    {
        $data = BrandMainImage::orderBy('sort', 'asc')->get();

        return view($this->pathView . 'index', compact('data'));
    }

    public function create()
    {
        return view($this->pathView . 'create');
    }

    public function store(Request $request)
    {
        $data=new BrandMainImage();
        $data->name=$request->name;
        $data->name = $request->name;
        if (isset($request->image)) {
            $file = $request->file('image');
            $data->image = $this->uploadBrand($file);
        }
        $data->visible = 1;
        $data->sort = 0;
        $data->save();

        return redirect('/brand-main');
    }

    public function edit(Request $request)
    {
        $data = BrandMainImage::where('id', $request->id)->first();

        return view($this->pathView . 'edit', compact('data'));
    }

    public function update(Request $request)
    {
        $data = BrandMainImage::where('id', $request->id)->first();
        $data->name = $request->name;
        if (isset($request->image)) {
            $file = $request->file('image');
            $data->image = $this->uploadBrand($file);
        }
        $data->visible = 1;
        $data->sort = $request->sort;
        $data->save();

        return redirect('/brand-main');
    }

    public function drop(Request $request)
    {
        BrandMainImage::where('id', $request->id)->delete();

        return redirect('/brand-main');
    }

    public function sort(Request $request)
    {
        foreach ($request->brands as $key => $brand) {
            BrandMainImage::where('product_uid', $brand['id'])
                ->update([
                    "sort" => $key + 1
                ]);
        }

        return $this->successResponse();
    }

    private function uploadBrand($file)
    {
        $imageFileName = uniqid() . '.' . $file->getClientOriginalExtension();
        $filePath = '/brands_main/' . $imageFileName;
        Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
        $url = Storage::disk('s3')->url('brands_main/' . $imageFileName);

        return $url;
    }
}
