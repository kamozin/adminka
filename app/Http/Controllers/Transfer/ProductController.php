<?php

namespace App\Http\Controllers\Transfer;

use App\Http\Controllers\Controller;
use App\Models\CatalogToRetail;
use App\Models\CategoryRetail;
use App\Models\FeaturesRetail;
use App\Models\Product;
use App\Models\ProductFile;
use App\Models\ProductImage;
use App\Models\ProductPrice;
use App\Models\ProductStock;
use App\Models\ValueFeaturesRetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;

class ProductController extends Controller
{

    public function __construct()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
    }


    public function index()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        set_time_limit(8000000);
        $products = DB::connection('mysqlOldSite')->table('products')
            ->where('transfer', 0)
            ->get();

        $i = 0;
        $a = 0;
        $b = 0;
        foreach ($products as $p) {

            try {
                if (explode('-', $p->uid)[0] == 'raec') {
                    $a = $a + 1;
                } else {
                    $product = Product::where('external_id', $p->uid)->first();
                    if (empty($product)) {
                        $data = $this->generateProductArray($p, '1С');


                        $modelProduct = new Product();
                        $uid = $modelProduct->create($data);

                        $prices = $this->priceArray($uid->uid, $p);


                        foreach ($prices as $price) {
                            $modelPrice = new ProductPrice();
                            $modelPrice->create($price);
                        }

                        $modelImage = new ProductImage();
                        $modelImage->create($this->imgArray($uid->uid, $p));

                        foreach ($this->stock($uid->uid, $p) as $s) {
                            $modelStock = new ProductStock();
                            $modelStock->create($s);
                        }
                        DB::connection('mysqlOldSite')->table('products')->where('uid', $p->uid)
                            ->update(
                                [
                                    "transfer" => 1
                                ]
                            );
                    } else {
                        DB::connection('mysqlOldSite')->table('products')->where('uid', $p->uid)
                            ->update(
                                [
                                    "transfer" => 1
                                ]
                            );
                    }

                    $i = $i + 1;
                }


            } catch (\Exception $e) {
                Log::info('Не загружено ' . $p->uid . ' ' . $e->getMessage());
                $b = $b + 1;
            }


        }

        dd($i, $b, $a);
    }

    private function stock($uid, $p)
    {
        $array = [
            ["storage_id" => 1, "stock" => $p->stock == null ? 0 : $p->stock, "product_uid" => $uid],
            ["storage_id" => 2, "stock" => 0, "product_uid" => $uid],
            ["storage_id" => 3, "stock" => 0, "product_uid" => $uid],
            ["storage_id" => 4, "stock" => 0, "product_uid" => $uid],
            ["storage_id" => 5, "stock" => 0, "product_uid" => $uid],
        ];

        return $array;
    }

    private function imgArray($uid, $data)
    {
        $array = [
            "name" => $data->img,
            "main" => 1,
            "product_uid" => $uid,
            "display" => 1
        ];

        return $array;
    }

    private function priceArray($uid, $data)
    {

        $array = [
            ['products_uid' => $uid, 'price' => $data->price_zakup, 'type_price' => 1],
            ['products_uid' => $uid, 'price' => $data->price_small_opt, 'type_price' => 2],
            ['products_uid' => $uid, 'price' => $data->price_large_opt, 'type_price' => 3],
            ['products_uid' => $uid, 'price' => $data->price, 'type_price' => 4],
            ['products_uid' => $uid, 'price' => $data->price_opt, 'type_price' => 5],
            ['products_uid' => $uid, 'price' => $data->price_shop, 'type_price' => 6]
        ];

        return $array;
    }


    private function generateProductArray($data, $system)
    {
        $product = [];
        $product['raec_id'] = 0;
        $product['code'] = 0;

        if (!empty($data->code_product))
            $product['code'] = $data->code_product;

        if (!empty($data->raec_id))
            $product['raec_id'] = $data->raec_id;

        $product['title'] = $data->title_product;
        $product['external_id'] = $data->uid;
        $product['sku'] = $data->sku;
        $product['brand_uid'] = $this->getBrand($data->brand);
        $product['description'] = $data->full_title_product;
        $product['parent_id'] = $data->parent_id;
        $product['multiplicity'] = $data->krat;
        $product['unit_id'] = (isset($data->orderUnit)) ? $this->getUnit($data->orderUnit, '1С') : 0;
        $product['display'] = 1;
        $product['liquid_stock'] = $data->liquid_stock;
        $product['attribute_bay'] = $data->attribute_bay;
        $product['system'] = $system;

        return $product;
    }

    private function getBrand($brand)
    {
        $data = ValueFeaturesRetail::where('title', $brand)->first();

        if (empty($data))
            return '0';

        return $data->uid;
    }


    public function getProductsFeatures()
    {
        ini_set('max_execution_time', 172800);
        ini_set("memory_limit", "2000M");
        set_time_limit(8000000);
        $dataArray = DB::table('products_properties')
            ->get();
//
//        +"uid_product": "644b5e9c-a40a-11ea-989d-3cecef00e893"
//        +"uid_category": "78d8d237-babc-11e6-872a-00259003152d"
//        +"uid_filtr": "a7cdb1c1-babc-11e6-872a-00259003152d"
//        +"uid_znach_filtr": "85f15837-20d8-11e7-80a2-00259003152d"

        $arrayError = [];
        foreach ($dataArray as $data) {
            $product = $this->getProduct($data->uid_product);
            if (!empty($product)) {
                try {
                    $category = $this->getCategory($data->uid_category);
                    $filter = $this->getFilter($data->uid_filtr);
                    $value = $this->getFilterValue($data->uid_znach_filtr);

                    DB::table('retail_feature_value_product')
                        ->insert([
                            "category_uid" => $category->uid,
                            "product_uid" => $product->uid,
                            "feature_uid" => $filter->uid,
                            "value_uid" => $value->uid,
                            "value" => $value->title,
                            "system" => "1C"
                        ]);

                } catch (\Exception $e) {

                    array_push($arrayError, $data);
                }
            } else {
                array_push($arrayError, $data);
            }

            $file = public_path() . "/test.txt";
            $fle = fopen($file, 'w+');
            fwrite($fle, json_encode($arrayError, JSON_UNESCAPED_UNICODE)); //преобразуем в json представление
            fclose($fle);
        }
    }

    private function getProduct($uid)
    {

        return Product::where('external_id', $uid)->first();
    }

    private function getCategory($uid)
    {
        $uid = CatalogToRetail::where('catalog_external_uid', $uid)->first();
        return CategoryRetail::find($uid->category_retail_uid);
    }

    private function getFilter($uid)
    {
        return FeaturesRetail::where('external_uid', $uid)->first();
    }

    private function getFilterValue($uid)
    {
        return ValueFeaturesRetail::where('external_uid', $uid)->first();
    }


    public function transferFiles()
    {
        ini_set('max_execution_time', 172800);
        ini_set("memory_limit", "2000M");
        set_time_limit(8000000);

        $products = Product::where('raec_id', '>', 0)
            ->where('system', '1С')
            ->get();

        $apiKey = 'ebb91621277e4915b63c6124f982fefc';
        $baseUrl = 'https://catalog.raec.su/api/';

        foreach ($products as $product) {

                }
            }
        }
    }

}
