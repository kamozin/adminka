<?php

namespace App\Http\Controllers\Transfer;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function index()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        set_time_limit(8000000);
        $users = DB::connection('mysqlOldSite')->table('users')
            ->get();
        $arr = [];
        foreach ($users as $u) {
            $check = User::where('email', $u->email)->first();
            if (empty($check)) {
                DB::table('users')->insert(
                    [
                        "id" => $u->id,
                        "name" => trim($u->name),
                        "email" => $u->email,
                        "password" => $u->password,
                        "address_delivery" => $u->address,
                        "phone" => $u->phone,
                        "created_at" => $u->created_at,
                        "updated_at" => $u->updated_at,
                        "subscribe" => 0
                    ]
                );
            } else {
                array_push($arr, $u->email);
            }
        }

    }

    public function orders()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        set_time_limit(8000000);
        $orders = DB::connection('mysqlOldSite')->table('order')
            ->get();

        foreach ($orders as $o) {
            $check= DB::table('order')->where('id', $o->order_id)->first();
            if(empty($check)) {
                DB::table('order')->insert(
                    [
                        'id' => $o->order_id,
                        'user_id' => (empty($o->user_id) ? 0 : $o->user_id),
                        'name' => $o->fio_user,
                        'email' => $o->email_user,
                        'phone' => $o->phone_user,
                        'paymeant' => $o->payment,
                        'delivery' => $o->delivery,
                        'point' => $o->point,
                        'count_product' => $o->count_products,
                        'total_price' => $o->total_price,
                        'uid' => $o->uid_order,
                        'status' => $o->status,
                        'cmnt' => $o->cmnt_user,
                        'address' => $o->address_user,
                        'delivery_price' => $o->delivery_price,
                        'payment_status'  => $o->payment_status,
                        'action_code'  => $o->action_code,
                        'order_amount'  => $o->order_amount,
                        'notification_order'  => $o->notification_order,
                        'order_number_sberbank'  => $o->order_number_sberbank,
                        'order_status_sberbank'  => $o->order_status_sberbank,
                        'created_at' => $o->created_at,
                        'updated_at' => $o->updated_at
                    ]
                );
            }


        }
    }

    public function orderProducts () {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        set_time_limit(8000000);
        $data = DB::connection('mysqlOldSite')->table('order_products')
            ->get();
        foreach ($data as $d) {
            $checkOrder=DB::connection('mysqlOldSite')->table('order')
                ->where('order_id', $d->order_id)
                ->first();
            if(!empty($checkOrder)) {
              $checkProduct=Product::where('external_id', $d->uid_product)->first();

              if(empty($checkProduct)) {
                  $uid="0";
              }else{
                  $uid=$checkProduct->uid;
              }

                DB::table('order_products')->insert(
                    [
                      'order_id'=>$d->order_id,
                      'uid_product'=>$uid,
                      'price_product'=>$d->price_product,
                      'count_product'=>$d->count_product,
                      'total_price_product'=>$d->total_price_product,
                      'created_at'=>$d->created_at,
                      'updated_at'=>$d->updated_at,
                    ]
                );
            }




        }
    }
}
