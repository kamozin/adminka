<?php

namespace App\Http\Controllers\Transfer;

use App\Http\Controllers\Controller;
use App\Models\Catalog1C;
use App\Models\CatalogToRetail;
use App\Models\Category;
use App\Models\CategoryRetail;
use App\Models\EtimClass;
use App\Models\Product;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;


class CategoryController extends Controller
{
    public function category1C()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        set_time_limit(8000000);

        $data=Category::where('parent_id', '<>', '0')
            ->with('categoryRetail')
            ->get();

        foreach($data as $d) {
            $product=Product::where('system', '1С')
                ->where('parent_id', $d->uid)
                ->get();
            foreach($product as $p) {
                $p->parent_id=$d->categoryRetail[0]->uid;
                $p->save();
            }
        }
//        $price = file_get_contents(public_path() . '/category.json');
//        $json = \GuzzleHttp\json_decode($price, true);
//        foreach ($json as $j) {
//
//            $model = new CategoryRetail();
//            $model->title = $j['title'];
//            $model->sort = 0;
//            $model->display = 0;
//            if ($j['parent_id'] == '0') {
//                $model->parent_id = 0;
//            } else {
//                $model->parent_id = $this->getParent($j['parent_id']);
//            }
//            $model->save();
//
//            $modelRelated = new CatalogToRetail();
//            $modelRelated->category_retail_uid = $model->uid;
//            $modelRelated->catalog_external_uid = $j['uid'];
//            $modelRelated->save();
//
//            $modelExternal=new Category();
//            $modelExternal->uid = $j['uid'];
//            $modelExternal->title = $j['title'];
//            $modelExternal->parent_id = $j['parent_id'];
//            $modelExternal->system = '1С';
//            $modelExternal->parent = 0;
//            $modelExternal->children = 0;
//
//            $modelExternal->save();
//        }
    }


    private function getParent($uid)
    {
        $data = CatalogToRetail::where('catalog_external_uid', $uid)->first();
        return $data->category_retail_uid;
    }

    public function uploadExcelEtimToCategoryRelated() {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        set_time_limit(8000000);

        $collection = (new FastExcel)->import('etim.xlsx');

        $etimError=[];
        foreach ($collection as $c){
           $class=$this->checkEtimCategory($c['Код класса']);
           if($class->category_retail_uid==null){

                $category=CategoryRetail::where('title', $c['Подраздел'])->first();
                if(!empty($category)){

                    EtimClass::where('classId', $c['Код класса'])->update([
                        "category_retail_uid"=>$category->uid
                    ]);
                }else{
                    array_push($etimError, $c['Код класса']);
                }
           }else{

           }
        }

        dd($etimError);
    }

    private function checkEtimCategory ($id) {
        return EtimClass::where('classId', $id)->first();
    }
}
