<?php

namespace App\Http\Controllers\Transfer;

use App\Http\Controllers\Controller;
use App\Models\FeaturesRetail;
use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\ValueFeaturesRetail;
use App\Traits\CategoryRetailTrait;
use App\Traits\FeatureTrait;
use Illuminate\Http\Request;

class FeaturesController extends Controller
{

    use FeatureTrait;
    use CategoryRetailTrait;

    public function __construct()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
    }

    public function parseFeatures()
    {
        $data = file_get_contents(public_path() . '/filters.json');
        $json = \GuzzleHttp\json_decode($data, true);

        foreach ($json as $j) {
            $model = new FeaturesRetail();
            $model->title = $j['title_filtres'];
            $model->external_uid = $j['uid'];
            $model->save();
        }
    }

    public function parseFeaturesToCategory()
    {
        $data = file_get_contents(public_path() . '/filters.json');
        $json = \GuzzleHttp\json_decode($data, true);

        foreach ($json as $j) {
            $feature = $this->getFeatureExternalUid($j['uid_filtr']);
            $categoryRetail = $this->getUidCategoryToExternal($j['uid_category']);
//            dd($j, $feature, $categoryRetail);
            try {
                $model = new FeaturesRetailToCategoryRetail();
                $model->features_retail_uid = $feature->uid;
                $model->category_retail_uid = $categoryRetail->category_retail_uid;
                $model->display_category_retail = 1;
                $model->sort_category_retail = 0;
                $model->sort_category_retail_product = 0;
                $model->save();
            } catch (\Exception $e) {
                dd($j, $feature, $categoryRetail);
            }
        }
    }

    public function parceFeatureValue()
    {
        $data = file_get_contents(public_path() . '/value.json');
        $json = \GuzzleHttp\json_decode($data, true);

        foreach ($json as $j) {
            $feature = $this->getFeatureExternalUid($j['uid_filter']);
            $model = new ValueFeaturesRetail();
            $model->title = $j['title'];
            $model->external_uid = $j['uid'];
            $model->features_retail_uid = $feature->uid;
            $model->save();
        }
    }

    public function parseValueFeatureCategory()
    {
        $data = file_get_contents(public_path() . '/filters.json');
        $json = \GuzzleHttp\json_decode($data, true);

        foreach ($json as $j) {
            foreach (explode(',', $j['item']) as $item) {
                try{
                    $value = $this->getFeaturesValueExternal($item);
                    $category = $this->getUidCategoryToExternal($j['uid_category']);
                    $feature = $this->getFeatureExternalUid($j['uid_filtr']);
                    $this->saveValueFeatureToCategoryRetail($category->category_retail_uid, $feature->uid, $value->uid);
                }catch (\Exception $e){

                }

            }
        }
    }
}
