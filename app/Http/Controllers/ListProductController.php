<?php

namespace App\Http\Controllers;

use App\Models\ListProduct;
use App\Models\ListProductTags;
use App\Models\ListProductToProduct;
use App\Models\Product;
use App\Models\StickerProduct;
use App\Models\TagList;
use App\Traits\ApiControllerTrait;
use App\Traits\ApiReplyTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ListProductController extends Controller
{
    use ApiReplyTrait;

    protected $stickerModel;

    public function __construct(StickerProduct $stickerProduct)
    {
        $this->stickerModel = $stickerProduct;
    }

    public function index()
    {
        $data = ListProduct::orderBy('sort', 'asc')->get();

        return view('list.index', compact('data'));
    }

    public function getList()
    {
        $data = ListProduct::all();

        return $this->successResponse($data);
    }

    public function storeList(Request $request)
    {
        try {
            ListProduct::create($request->all());
            $data = [
                "message" => 'Список успешно создан',
                "list" => ListProduct::all()
            ];

            return $this->successResponse($data);
        } catch (\Exception $e) {

            return $this->errorResponse(["error" => $e->getMessage()]);
        }
    }

    public function updateList(Request $request)
    {

        ListProduct::where('id', $request->id)->update(
            [
                "title" => $request->title
            ]
        );

        $data = [
            "message" => "Список успешно обновлен",
            "list" => ListProduct::all()
        ];

        return $this->successResponse($data);
    }


    public function displayList(Request $request)
    {

        if ($request->display == 1) {
            $data = ListProductToProduct::where('list_id', $request->list_id)->count();

            if ($data == 0) {
                $data = [
                    "message" => 'Вы не можете включить список. Отсутствуют товары'
                ];
                return $this->errorResponse($data);
            }
        }


        ListProduct::where('id', $request->list_id)->update(
            [
                "display" => $request->display
            ]
        );

        if ($request->display == 1) {
            $message = "Список включен";
        } else {
            $message = "Список выключен";
        }

        $data = [
            "message" => $message
//            "list"=>ListProductToProduct::where('list_id', $request->list_id)->with('product')->get()
        ];

        return $this->successResponse($data);

    }

    public function dropList(Request $request)
    {
        ListProductToProduct::where('list_id', $request->id)->delete();
        ListProduct::where('id', $request->id)->delete();

        $data = [
            "message" => "Список успешно удален",
            "list" => ListProduct::all()
        ];

        return $this->successResponse($data);
    }

    public function storeProductList(Request $request)
    {


        $product = Product::where('sku', $request->sku)->get();

        if (sizeof($product) > 0) {
            foreach ($product as $p) {

                $pcheck = DB::table('list_product_to_product')
                    ->where('list_id', $request->list_id)
                    ->where('product_uid', $p->uid)->first();

                if (empty($pcheck)) {
                    DB::table('list_product_to_product')->insert(
                        [
                            "list_id" => $request->list_id,
                            "product_uid" => $p->uid
                        ]
                    );
                } else {

                    return $this->errorResponse(['message' => 'Товар уже присутствует в списке']);

                }

            }

            $data = [
                "message" => "Товар успешно добавлен",
                "list" => ListProductToProduct::where('list_id', $request->list_id)->with('product')->get()
            ];

            return $this->successResponse($data);
        } else {
            return $this->errorResponse(["message" => "Товар с таким артикулом не найден"]);
        }

    }

    public function deleteProduct(Request $request)
    {
        DB::table('list_product_to_product')->where('product_uid', $request->uid_product)
            ->where('list_id', $request->list_id)->delete();


        $data = [
            "message" => "Товар удален из списка",
            "list" => ListProductToProduct::where('list_id', $request->list_id)->with('product')->get()
        ];
        return $this->successResponse($data);
    }

    public function getProduct(Request $request)
    {

        $products = ListProductToProduct::where('list_id', $request->list_id)->with('product')
            ->orderBy('sort', 'asc')
            ->get();

        foreach ($products as $key => $product) {
            $products[$key]->tags = ListProductTags::select('tag as text')->where('list_id', $request->list_id)->where('product_uid', $product->product_uid)->get();
        }

        $tags = TagList::select('tag')->where('list_id', $request->list_id)->get()->pluck('tag');
        $data = (object)[];
        $data->products = $products;
        $data->tags = [];
        foreach ($tags as $tag) {
            array_push($data->tags, (object)['text' => $tag]);
        }


        return $this->successResponse($data);
    }

    public function sortUpdate(Request $request)
    {
        foreach ($request->list as $key => $list) {
            ListProduct::where('id', $list['id'])
                ->update([
                    "sort" => $key + 1
                ]);
        }
        return $this->successResponse();
    }

    public function saveTagsProduct(Request $request)
    {
        foreach ($request->products as $key => $list) {
            ListProductToProduct::where('product_uid', $list['product_uid'])
                ->update([
                    "sort" => $key + 1
                ]);
        }
        return $this->successResponse();
    }

    public function uploadIcon(Request $request)
    {

        $file = $request->file('sticker')->store('sticker');
        ListProductToProduct::where('list_id', request()->get('id'))->update([
            "icon" => $file,
            "iconDisplay" => 1
        ]);

        return $this->successResponse();
    }

    public function uploadIconProduct(Request $request)
    {

        $file = $request->file('sticker')->store('sticker');
        ListProductToProduct::where('list_id', request()->get('id'))
            ->where('product_uid', request()->get('product_uid'))
            ->update([
                "icon" => $file,
                "iconDisplay" => 1
            ]);

        return $this->successResponse();
    }

    public function titleSave(Request $request)
    {
        ListProduct::where('id', $request->list_id)
            ->update([
                "title" => $request->title
            ]);
        $data = [
            "message" => 'Наименование изменено',
            "list" => ListProduct::orderBy('sort', 'asc')->get()
        ];
        return $this->successResponse($data);
    }

    public function changeView(Request $request)
    {
        ListProduct::where('id', $request->id)
            ->update([
                "view" => $request->view
            ]);
        return $this->successResponse();
    }

}
