<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CheckImageController extends Controller
{
    public function start () {
        $data = ProductImage::where('system', '<>', "1C")->get();

        foreach ($data as $d) {
           $name=explode('.', $d->name);
           if(empty($d->name)){
               ProductImage::where('id', $d->id)->delete();
           }else{
               $product=Product::where('code', $name[0])->get();

               if(sizeof($product)==1) {
                   $check = Storage::disk('s3')->exists('/files/products/'.$d->name);
                   if($check)
                   {
                       ProductImage::where('id', $d->id)
                           ->update([
                                "system"=>"1C"
                           ]);
                   }else
                   {
                       ProductImage::where('id', $d->id)->delete();
                   }
               }else{
                   try{
                        Storage::disk('s3')->delete('/files/products/'.$d->name);
                   }catch (\Exception $e){

                   }
                   ProductImage::where('id', $d->id)->delete();
               }
           }

        }
    }


}
