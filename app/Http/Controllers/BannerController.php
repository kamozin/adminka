<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Traits\ApiControllerTrait;
use App\Traits\ApiReplyTrait;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    use ApiReplyTrait;

    public function __construct (){

    }


    public function index () {

        $data=$this->getAll();
        return view('banners.index', compact('data'));
    }

    public function store (Request $request) {

        try{

            Banner::create($request->all());

            $data = [
                "message" => "Баннер успешно добавлен",
                "banner" => $this->getAll()
            ];

            return $this->successResponse($data);

        }catch (\Exception $e){

            return $this->errorResponse();
        }
    }

    public function update (Request $request) {

        try{
            $d=Banner::find($request->id);
            $d->name=$request->name;
            $d->type=$request->type;
            $ban=explode('/', $request->file);
            $d->file='slidersright/'.$ban[1];
            $d->url=$request->url;
            $d->save();


            $data = [
                "message" => "Баннер успешно обновлен",
                "banner" => $this->getAll()
            ];

            return $this->successResponse($data);
        }catch (\Exception $e) {
            return $this->errorResponse();
        }

    }

    private function getAll () {
        return Banner::all();
    }


    public function upload (Request $request) {
        return $request->file('file')->store('banner');
    }
}
