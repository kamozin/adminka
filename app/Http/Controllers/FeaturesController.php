<?php

namespace App\Http\Controllers;

use App\Models\CategoryRetail;
use App\Models\EtimClass;
use App\Models\Features;
use App\Models\FeaturesRetail;
use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\FeaturesRetailToEtim;
use App\Models\FeaturesToEtimClass;
use App\Models\FeaturesValue;
use App\Models\Product;
use App\Models\RetailFeatureValueProduct;
use App\Models\ValueEtimFeatureClass;
use App\Models\ValueFeaturesRetail;
use App\Models\ValueFeatureToCategory;
use App\Traits\ApiReplyTrait;
use App\Traits\FeatureTrait;
use Illuminate\Http\Request;

class FeaturesController extends Controller
{
    use ApiReplyTrait;
    use FeatureTrait;

    public function __construct()
    {

    }

    public function index(Request $request)
    {

        if ($request->category) {

            $data = $this->getFeaturesCategoryRetail($request->category);

            $etim = EtimClass::where('category_retail_uid', $request->category)->get();

            $features = FeaturesToEtimClass::whereIn('classId', $etim->pluck('classId'))
                ->with(['featureInfo'])
                ->get();

//            dd($features[0]);

            $featuresArray = [];
            foreach ($features as $f) {
                $dataFeature = (object)[];
                $dataFeature->featureId = $f->featureInfo->featureId;
                $dataFeature->descriptionRu = $f->featureInfo->descriptionRu . ' | ' . $f->featureInfo->featureId;
                $dataFeature->classId = $f->classId;
                $dataFeature->unit = $f->unit_features_id;
                array_push($featuresArray, $dataFeature);
            }

//            dd($etim);
//
            foreach ($data->features as $key => $f) {
                $data->features[$key]->value = $this->getRelatedPropertyEtim($request->category, $f);
            }


            return view('features.indexCategoryFeatures', compact('data', 'featuresArray'));
        } else {
            $data = FeaturesRetail::with('category', 'value')->paginate(50);

            return view('features.index', compact('data'));
        }
    }

    public function getFeatureJson(Request $request)
    {
        $data = $this->getFeaturesCategoryRetail($request->category, $request->sortType);
        return $this->successResponse($data);
    }

    public function show(Request $request)
    {
        $data = FeaturesRetail::where('uid', $request->uid)
            ->with('category')
            ->first();


        return view('features.edit', compact('data'));
    }

    public function setSortFeatures(Request $request)
    {
        if ($request->sortType == 'product')
            $fieldSort = 'sort_category_retail_product';

        if ($request->sortType == 'filter')
            $fieldSort = 'sort_category_retail';

        foreach ($request->list as $key => $f) {
            try {
                $category = $f["pivot"]["category_retail_uid"];
                $feature = $f["pivot"]["features_retail_uid"];
                $sort = $key + 1;
                FeaturesRetailToCategoryRetail::where('category_retail_uid', $category)
                    ->where('features_retail_uid', $feature)
                    ->update(
                        [
                            $fieldSort => $sort
                        ]
                    );
            } catch (\Exception $e) {

            }

        }
        return $this->successResponse();
    }

    public function updateDisplay(Request $request)
    {
        FeaturesRetailToCategoryRetail::where('category_retail_uid', $request->category)
            ->where('features_retail_uid', $request->feature)
            ->update(
                [
                    'display_category_retail' => $request->display
                ]
            );
        return $this->successResponse();
    }


    public function updateGold(Request $request)
    {
        FeaturesRetailToCategoryRetail::where('category_retail_uid', $request->category)
            ->where('features_retail_uid', $request->feature)
            ->update(
                [
                    'gold' => $request->gold
                ]
            );
        return $this->successResponse();
    }

    public function updateQueueFeature(Request $request)
    {
        FeaturesRetailToCategoryRetail::where('category_retail_uid', $request->category)
            ->where('features_retail_uid', $request->feature)
            ->update(
                [
                    'queueFeature' => $request->value
                ]
            );
        return $this->successResponse();
    }

    public function updateUseFilter(Request $request)
    {
        FeaturesRetailToCategoryRetail::where('category_retail_uid', $request->category)
            ->where('features_retail_uid', $request->feature)
            ->update(
                [
                    'useFilter' => $request->use
                ]
            );
        return $this->successResponse();
    }

    public function getEtimFeatures(Request $request)
    {
        $etim = EtimClass::where('category_retail_uid', $request->category)->get();

        $features = FeaturesToEtimClass::whereIn('classId', $etim->pluck('classId'))
            ->with('featureInfo')
            ->get();

        $featuresArray = [];
        foreach ($features as $item) {
            $item->featureInfo->classId = $item->classId;
            $title = $item->featureInfo->featureId . '|' . $item->featureInfo->descriptionRu;
            array_push($featuresArray, $title);
        };

        return $this->successResponse($featuresArray);

    }

    public function associateEtim(Request $request)
    {

        $data = $request->all();

        FeaturesRetailToEtim::where('features_retail_uid', $data['uid'])
            ->where('category_retail_uid', $data['pivot']['category_retail_uid'])
            ->delete();

        foreach ($data['value'] as $value) {


            $model = new FeaturesRetailToEtim();
            $model->features_retail_uid = $data['uid'];
            $model->etimId = $value['featureId'];
            $model->category_retail_uid = $data['pivot']['category_retail_uid'];
            $model->classId = $value['classId'];
            $model->save();

        }
        return $this->successResponse();
    }


    public function getValue(Request $request)
    {

        $values = ValueFeatureToCategory::where('category_retail_uid', $request->category)
            ->where('feature_retail_uid', $request->feature)
            ->groupBy('feature_value_retail_uid')
            ->with('value')
            ->get();
        return $this->successResponse($values);
    }

    public function filterProducts(Request $request)
    {
        $dataProducts = RetailFeatureValueProduct::where('category_uid', $request->category)
            ->where('feature_uid', $request->feature)
            ->where('value_uid', $request->v)->get()->pluck('product_uid');

        $data=Product::whereIn('uid', $dataProducts)->get();
        $titleFilter=FeaturesRetail::where('uid', $request->feature)->first();

        $titleValue=ValueFeaturesRetail::where('uid', $request->v)->first();

        return view('products.featureProducts', compact('data', 'titleFilter', 'titleValue'));
    }

    public function uploadFeaturesValues (Request $request) {

    }


}
