<?php

namespace App\Http\Controllers;

use App\Models\EtimGorup;
use Illuminate\Http\Request;

class EtimGroupController extends Controller
{
    public function __construct () {

    }

    public function index (Request  $request) {

        $data=EtimGorup::all();
        return view('etimGroup.index', compact('data'));
    }

    public function create () {

        return view('etimGroup.create');
    }

    public  function edit (Request  $request) {

        $data=EtimGorup::find($request->id);
        return view('etimGroup.create', compact('data'));
    }

    public function store (Request  $request) {

    }
    public function update (Request  $request) {

    }

}

