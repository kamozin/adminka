<?php

namespace App\Http\Controllers;

use App\Models\Contacts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactsController extends Controller
{

    private $pathView = 'contacts.';

    public function __construct()
    {

    }

    public function index()
    {

        $contacts = Contacts::all();

        return view($this->pathView . 'index', compact('contacts'));
    }

    public function create()
    {

        return view($this->pathView . 'create');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * 'type',
     *'address',
     *'phone',
     *'email',
     *'time_work',
     *'time_work_weekends'
     */
    public function store(Request $request)
    {

        $contact = new Contacts();
        $contact->type = $request->type;
        $contact->address = $request->address;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->time_work = $request->time_work;
        $contact->time_work_weekends = $request->time_work_weekends;
        $contact->save();

        return redirect()->route('contacts.index')->with('messageSuccess', 'Контакт добавлен');
    }

    public function edit(Request $request)
    {
        $contact=Contacts::find($request->id);
        return view($this->pathView . 'edit', compact('contact'));
    }

    public function update(Request $request)
    {
        $contact=Contacts::find($request->id);
        $contact->type = $request->type;
        $contact->address = $request->address;
        $contact->phone = $request->phone;
        $contact->email = $request->email;
        $contact->time_work = $request->time_work;
        $contact->time_work_weekends = $request->time_work_weekends;
        $contact->save();

        return redirect()->route('contacts.index')->with('messageSuccess', 'Контакт добавлен');
    }

    public function drop(Request $request)
    {

        return redirect()->route('contacts.index')->with('messageSuccess', 'Контакт удален');
    }
}
