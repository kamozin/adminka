<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Jobs\StockProduct;
use App\Traits\ApiReplyTrait;
use App\Traits\ProductTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StockController extends Controller
{
    use ApiReplyTrait, ProductTrait, ResponseTrait;

    protected $success = [];
    protected $errors = [];

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $data = json_decode($request->getContent());
        StockProduct::dispatch($data);
        return $this->successResponse();
    }
}
