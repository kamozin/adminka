<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Jobs\FeatureRetail;
use App\Models\Features;
use App\Models\FeaturesRetail;
use App\Models\FeaturesRetailToCategoryRetail;
use App\Models\Guides\Storage;
use App\Models\Product;
use App\Models\RetailFeatureValueProduct;
use App\Models\ValueFeaturesRetail;
use App\Models\ValueFeatureToCategory;
use App\Traits\ApiReplyTrait;
use App\Traits\FeatureTrait;
use App\Traits\ProductTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FeaturesController extends Controller
{

    use ProductTrait, FeatureTrait, ResponseTrait, ApiReplyTrait;

    protected $success = [];
    protected $errors = [];

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $data = json_decode($request->getContent());
        FeatureRetail::dispatch($data);
        return $this->successResponse();
    }

    private function features($product, $features)
    {
//        Проходим массив характеристик
        foreach ($features as $f) {
            $featureInfo = $this->checkfeatures($f->uid);
            if (empty($featureInfo)) {
                $uidFeature = $this->storeFeature($f);
            } else {
                $uidFeature = $featureInfo->uid;
            }
            if (sizeof($f->values) > 0) {
                $this->values($product->uid, $product->parent_id, $uidFeature, $f->values);
            }
        }
    }

    private function getInfoProduct($uidProduct)
    {
        return Product::where('external_id', $uidProduct)->first();
    }

    private function checkfeatures($uid)
    {
        $data = FeaturesRetail::where('external_uid', $uid)->first();
        return $data;
    }

    private function storeFeature($data)
    {
        $model = new FeaturesRetail();
        $model->title = $data->title;
        $model->external_uid = $data->uid;
        $model->save();
        return $model->uid;
    }

    private function values($uidProduct, $uidCategory, $uidFeature, $values)
    {
        foreach ($values as $v) {
            $value = $this->checkValue($v->uid);
            if (empty($value)) {
                $valueUid = $this->storeValue($v, $uidFeature);
            } else {
                $valueUid = $value->uid;
            }

            $this->checkPopulateCategory($uidFeature, $uidCategory);
            $this->saveRelateValueFeatureProduct($uidCategory, $uidProduct, $uidFeature, $valueUid, $v->value, "1С");
            $this->saveValueFeatureToCategoryRetail($uidCategory, $uidFeature, $valueUid);
        }

    }

    private function storeValue($v, $featureUid)
    {
        $modelValue = new ValueFeaturesRetail();
        $modelValue->title = $v->value;
        $modelValue->external_uid = $v->uid;
        $modelValue->value_external = $v->value;
        $modelValue->features_retail_uid = $featureUid;
        $modelValue->save();

        return $modelValue->uid;
    }

    private function checkValue($uid)
    {
        return ValueFeaturesRetail::where('external_uid', $uid)->first();
    }

    private function checkPopulateCategory($featureUid, $categoryUid)
    {
        if (empty($this->checkFeatureCategory($featureUid, $categoryUid))) {
            $model = new FeaturesRetailToCategoryRetail();
            $model->features_retail_uid = $featureUid;
            $model->category_retail_uid = $categoryUid;
            $model->useFilter = 1;
            $model->useFilter = 1;
            $model->display_category_retail = 1;
            $model->sort_category_retail = 0;
            $model->sort_category_retail_product = 0;
            $model->save();
        }
    }

    private function checkFeatureCategory($uidFeature, $uidCategory)
    {
        return FeaturesRetailToCategoryRetail::where('features_retail_uid', $uidFeature)
            ->where('category_retail_uid', $uidCategory)
            ->first();
    }

    private function saveRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system)
    {
        if (empty($this->checkRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system))) {
            $model = new RetailFeatureValueProduct ();
            $model->category_uid = $categoryUid;
            $model->product_uid = $productUid;
            $model->feature_uid = $featureUid;
            $model->value_uid = $valueUid;
            $model->value = $value;
            $model->system = $system;
            $model->save();
        }
    }

    private function checkRelateValueFeatureProduct($categoryUid, $productUid, $featureUid, $valueUid, $value, $system)
    {
        return RetailFeatureValueProduct::where('category_uid', $categoryUid)
            ->where('product_uid', $productUid)
            ->where('feature_uid', $featureUid)
            ->where('value_uid', $valueUid)
            ->where('value', $value)
            ->where('system', $system)
            ->first();
    }

    private function saveValueFeatureToCategoryRetail($category, $feature, $value)
    {
        if (empty($this->getValueFeatureToCategoryRetail($category, $feature, $value))) {
            $model = new ValueFeatureToCategory();
            $model->category_retail_uid = $category;
            $model->feature_retail_uid = $feature;
            $model->feature_value_retail_uid = $value;
            $model->save();
        }
    }

    private function getValueFeatureToCategoryRetail($category, $feature, $value)
    {
        return ValueFeatureToCategory::where('category_retail_uid', $category)
            ->where('feature_retail_uid', $feature)
            ->where('feature_value_retail_uid', $value)
            ->first();
    }


//    Изменение наименования фильтра
    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        foreach ($data as $d) {
            $f = $this->checkfeatures($d->uid);
            if (empty($f)) {
                $this->storeFeature($d);
            } else {
                FeaturesRetail::where('external_uid', $d->uid)
                    ->update(
                        [
                            "title" => $d->title
                        ]
                    );
            }
        }
        return $this->successResponse();
    }

//    Изменение наименования свойства
    public function updateValue(Request $request)
    {
        $data = json_decode($request->getContent());
        foreach ($data as $d) {

            $features=FeaturesRetail::where('external_uid', $d->{"filter-uid"})->first();

            foreach ($d->values as $value) {
                $v = ValueFeaturesRetail::where('external_uid', $value->uid)
                    ->where('features_retail_uid', $features->uid)
                    ->first();
                if(empty($v)) {
                    $dataValue=(object) [];
                    $dataValue->value=$value->title;
                    $dataValue->uid=$value->uid;
                    $dataValue->features_retail_uid=$features->uid;
                    $this->storeValue($dataValue, $features->uid);
                }else{
                    ValueFeaturesRetail::where('uid', $v->uid)
                        ->where('features_retail_uid', $features->uid)
                        ->update(
                            [
                                "title" => $value->title
                            ]
                        );
                    RetailFeatureValueProduct::where('value_uid', $v->uid)
                        ->where('feature_uid', $features->uid)
                        ->update(
                            [
                                "value" => $value->title
                            ]
                        );
                }
            }
        }
        return $this->successResponse();
    }


}
