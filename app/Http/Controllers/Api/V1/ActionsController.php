<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Actions;
use App\Models\ListProduct;
use App\Models\ListProductToProduct;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Traits\check1CTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ActionsController extends Controller
{

//Список всех цен - 0
//Закупочная  -   1
//Мелкооптовая - 2
//Крупнооптовая - 3
//Розничная -  4
//Базовая (опт) -  5
//Цена И-М -  6

    use check1CTrait;

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $data = json_decode($request->getContent());
        $actions = Actions::where('uid_dok', $data->uid_dok)->first();

        if (empty($actions)) {
            $this->create($data);
        } else {
            $this->update($data, $actions->id);
        }

        return response()->json(['success' => 1]);
    }

    private function create($data)
    {
        $action = new Actions();
        $action->title = $data->cmnt;
        $action->dt = $data->ot . '|' . $data->do;
        $action->description = $data->text_akcii;
        $action->text = $data->text_akcii;
        $action->image = '';
        $action->display = 0;
        $action->main_page = 0;
        $action->list_id = 0;
        $action->dt_start = $data->ot;
        $action->dt_end = $data->do;
        $action->uid_dok = $data->uid_dok;
        $action->number_dok = $data->number_dok;
        $action->data_dok = $data->data_dok;
        $action->tip_dok = $data->tip_dok;
        $action->skidka = $this->getSkidka($data->item);
        $action->tip_cen = $data->tip_cen;
        $action->save();

        $this->createListProduct($data->item, $action->id, $data->cmnt, $data->do);


    }

    private function update($data, $id)
    {

        $action = Actions::find($id);
        $action->title = $data->cmnt;
        $action->dt =  $this->generateDt($data->ot) . '|' .  $this->generateDt($data->do);
        $action->dt_start = $this->generateDt($data->ot);
        $action->dt_end = $this->generateDt($data->do);
        $action->data_dok = $data->data_dok;
        $action->tip_dok = $data->tip_dok;
        $action->skidka = $this->getSkidka($data->item);
        $action->tip_cen = $data->tip_cen;
        $action->save();
        $this->updateListActions((array)$data->item, $action->list_id, $id, $data->do);
    }

    private function generateDt ($dt) {
      return implode('-', array_reverse(explode('.', $dt)));
    }

    private function updateListActions($items, $listId, $actionId, $dt)
    {
        $listProducts = ListProductToProduct::where('list_id', $listId)
            ->get();

        foreach ($listProducts as $product) {
            $productInfo = Product::where('uid', $product->product_uid)->first();

            $key = $this->getIndexElList($productInfo->external_id, $items);
            if ($key == -1) {
                $object = (object)[
                    "uid" => $productInfo->external_id,
                    "typePrice" => 6
                ];
                try{
                    $price = $this->getPrice($object);
                    ProductPrice::where('type_price', 6)
                        ->where('products_uid', $productInfo->uid)
                        ->update([
                            "price" => $price[0]->price,
                            "dt_end" => null
                        ]);
                }catch (\Exception $e){
                    ProductPrice::where('type_price', 6)
                        ->where('products_uid', $productInfo->uid)
                        ->update([
                            "price" => 0,
                            "dt_end" => null
                        ]);
                }
//                Удаляем товар из списка
                ListProductToProduct::where('list_id', $listId)
                    ->where('product_uid', $productInfo->uid)
                    ->delete();
            } else {
                $object = (object)[
                    "uid" => $productInfo->external_id,
                    "typePrice" => 6
                ];
                try{
                    $price = $this->getPrice($object);
                    ProductPrice::where('type_price', 6)
                        ->where('products_uid', $productInfo->uid)
                        ->update([
                            "price" => $price[0]->price,
                            "dt_end" => null
                        ]);
                }catch (\Exception $e){
                    ProductPrice::where('type_price', 6)
                        ->where('products_uid', $productInfo->uid)
                        ->update([
                            "price" => 0,
                            "dt_end" => null
                        ]);
                }

//                Удаляем товар из списка
                ListProductToProduct::where('list_id', $listId)
                    ->where('product_uid', $productInfo->uid)
                    ->delete();
            }
            //


        }

        $this->addProductList($items, $listId, $dt);
    }

    private function getPriceAccountingSystem()
    {

    }

    private function getIndexElList($uid, $items)
    {
        foreach ($items as $key => $i) {
            if ($i->uid_product == $uid) {
                return $key;
            }
        }
        return -1;
    }

    private function addProductList($arrayProducts, $listId, $dt)
    {
        foreach ($arrayProducts as $item) {
            $product = $this->getProduct($item->uid_product);
            if (!empty($product)) {
                $listProduct = new ListProductToProduct();
                $listProduct->list_id = $listId;
                $listProduct->product_uid = $product->uid;
                $listProduct->skidka = $item->skidka;
                $listProduct->save();
                $this->setPriceProduct($product, $item->skidka, $dt);
            }
        }
    }


    private function createListProduct($arrayProducts, $actionId, $title, $dt)
    {

        $list = new ListProduct();
        $list->title = 'Список для акции - ' . $title;
        $list->display = 0;
        $list->not = 1;
        $list->save();

        foreach ($arrayProducts as $item) {
            $product = $this->getProduct($item->uid_product);
            if (!empty($product)) {
                $listProduct = new ListProductToProduct();
                $listProduct->list_id = $list->id;
                $listProduct->product_uid = $product->uid;
                $listProduct->skidka = $item->skidka;
                $listProduct->save();
                $this->setPriceProduct($product, $item->skidka, $dt);
            }
        }
        Actions::where('id', $actionId)
            ->update([
                "display" => 1,
                "list_id" => $list->id
            ]);
    }

    private function getSkidka($arrayProducts)
    {
        $data = (array)$arrayProducts;
        $skidka = $data[0]->skidka;

        foreach ($data as $item) {
            if ($item->skidka == $skidka) {

            } else {
                return 0;
            }
        }
        return $skidka;
    }

    private function setPriceProduct($product, $skidka, $dt)
    {
        $productPrice = ProductPrice::where('products_uid', $product->uid)
            ->where('type_price', 4)->first();
        if ($productPrice->price == 0) {

        } else {
            $skidkaPrice = $productPrice->price - ($productPrice->price * ($skidka / 100));
            ProductPrice::where('products_uid', $product->uid)
                ->where('type_price', 6)
                ->update([
                    "price" => $skidkaPrice,
                    "dt_end" => $dt
                ]);
        }
    }

    private function getProduct($uid)
    {
        $data = Product::where('external_id', $uid)->first();

        return $data;
    }

    public function stopActions()
    {
        $actions = Actions::all();

        foreach ($actions as $action) {
            if ($action->dt_end == Carbon::now()->format('d.m.Y')) {
                dd(1);
            }
        }
    }
}
