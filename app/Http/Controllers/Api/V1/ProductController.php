<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Jobs\StoreProduct;
use App\Models\Guides\Storage;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Models\ProductStock;
use App\Models\ProductWeight;
use App\Models\TypePrice;
use App\Models\TypeStorage;
use App\Traits\ApiReplyTrait;
use App\Traits\FeatureTrait;
use App\Traits\ProductTrait;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{

    use ApiReplyTrait, ProductTrait, FeatureTrait, ResponseTrait;

    public function __construct()
    {

    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        StoreProduct::dispatch($data);
        return $this->successResponse();

    }



    /**
     * Загрузка фото для товара
     * @param Request $request
     */
    public function image(Request $request)
    {
        $data = json_decode($request->getContent());
        foreach ($data->images as $d) {
            $image_64 = $d->img; //your base64 encoded data
            $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf
            $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
            $image = str_replace($replace, '', $image_64);
            $image = str_replace(' ', '+', $image);

            $imageName = md5(microtime()) . '.' . $extension;

            \Illuminate\Support\Facades\Storage::put('/products/' . $imageName, base64_decode($image));

            return $imageName;
        }
    }

    /**
     * Выключение / Включение товара
     * @param Request $request
     */
    public function display(Request $request)
    {
        $data = json_decode($request->getContent());
        Product::where('external_id', $data->uid_product)
            ->update(
                [
                    "display"=>$data->display
                ]
            );
        if($data->display==0) {
            DB::table('retail_feature_value_product')
                ->where('product_uid', $data->uid_product)
                ->delete();
        }

        return response()->json(["status" => true]);
    }



    public  function getRaecProducts()
    {
        $products = Product::where('system', 'RAEC')->get();

        foreach ($products as $p) {

        }
    }


    public function updateWeghtProduct($uid, $data)
    {
        $model = ProductWeight::where('product_uid', $uid);
        $model->length = $data->length;
        $model->width = $data->width;
        $model->height = $data->height;
        $model->volume = $data->volume;
        $model->weight = $data->weight;
        $model->save();
    }


}
