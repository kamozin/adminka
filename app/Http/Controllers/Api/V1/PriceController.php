<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Jobs\PriceProduct;
use App\Traits\ApiReplyTrait;
use App\Traits\ProductTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PriceController extends Controller
{

    use ProductTrait;
    use ApiReplyTrait;

    protected $errors = [];
    protected $success = [];

    public function __construct()
    {

    }

//    Обновление цен на сайте
    public function index(Request $request)
    {
        $data = json_decode($request->getContent());
        if (sizeof($data) > 500)
            return $this->errorResponse('Превышен допустимый (500 товаров) размер массива', 500);

        PriceProduct::dispatch($data);
        return $this->successResponse();
    }

    public function getTypePrice($type)
    {
        $data = $this->getTypePriceProduct($type);
        return $type - $this->successResponse($data);
    }

    public function getTypesPrice(Request $request)
    {
        return $this->successResponse($this->getTypesPrice());
    }


}
