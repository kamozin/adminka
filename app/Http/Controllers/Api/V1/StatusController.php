<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Mail\OrderEmailAdmin;
use App\Mail\readyOrderClient;
use App\Models\Order;
use App\SMSRU\SMSRU;
use App\Traits\ApiReplyTrait;
use App\Traits\SmsTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class StatusController extends Controller
{
    use ApiReplyTrait;
    use SmsTrait;
    private $shops = [
        'г. Брянск, ул. Бурова, 8.',
        'г. Брянск, ул. Ст. Димитрова, 67.',
        'г. Брянск, ул. Степная, 12.',
        'г. Брянск, ул. 22-го Сьезда КПСС, 96.',
        'г. Брянск, ул. Белобережская, 20.',
        'г. Брянск, ул. Флотская, 123а'
    ];

    public function index(Request $request)
    {
        $data = json_decode($request->getContent());
        $order = Order::where('uid', $data->uid)->first();
        $order->status = $data->status;
        $order->save();
        if ($data->status == 2) {
            $textSms = 'Заказ №' . $order->id . ' готов к получению. Ваш заказа готов к получению и ожидает вас в магазине. Адрес: ' . $this->shops[$order->point];

            $dataMail = (object)[];
            $dataMail->order = $order->id;
            $dataMail->point = $this->shops[$order->point];

            Mail::to(
                [$order->email]
            )->send(new readyOrderClient($dataMail));

            //$smsru = new SMSRU('09680556-7A6A-D840-E479-D53124BE68B1'); // Ваш уникальный программный ключ, который можно получить на главной странице
            $dataObject = (object)[];
            $dataObject->to = $order->phone;
            $dataObject->text = $textSms; // Текст сообщения
            //$sms = $smsru->send_one($dataObject);
            $this->smsSend($dataObject->to, $dataObject->text);
            return $this->successResponse();
//            if(isset($sms->sms_id)){
//                Order::where('uid', $data->uid)
//                    ->update([
//                        "smsId" => $sms->sms_id
//                    ]);
//                return $this->successResponse();
//            }else{
//                return $this->errorResponse();
//            }



        }

        return $this->successResponse();
    }

    public function getStatus(Request $request)
    {
        $data = json_decode($request->getContent());
        $order = Order::where('uid', $data->uid)->first();
        if($order->smsId) {
            $smsru = new SMSRU('09680556-7A6A-D840-E479-D53124BE68B1');
            $result = $smsru->getStatus($order->smsId);
            $resSms = (array)$result->sms;
            return $this->successResponse($resSms[$order->smsId]->status_text);
        }

        return $this->successResponse('Сообщение не отпралялось');


    }

    public function sendSmsOrder(Request $request)
    {
        $order = Order::where('id', $request->id)->first();


        $dataMail = (object)[];
        $dataMail->order = $order->id;
        $dataMail->point = $this->shops[$order->point];
        Mail::to(
            ['hrolenkov@yandex.ru']
        )->send(new readyOrderClient($dataMail));
        /*
         $textSms = 'Заказ №' . $order->id . ' готов к получению.
 Ваш заказа готов к получению и ожидает вас в магазине. Адрес: ' . $this->shops[$order->point];
         $smsru = new SMSRU('09680556-7A6A-D840-E479-D53124BE68B1'); // Ваш уникальный программный ключ, который можно получить на главной странице
         $data = (object)[];
         $data->to = '+79529662913';
         $data->text = $textSms; // Текст сообщения
         $sms = $smsru->send_one($data);

         $result = $smsru->getStatus($sms->sms_id);
         $resSms = (array)$result->sms;

         dd($resSms[$sms->sms_id]->status_text);
        */
    }
}
