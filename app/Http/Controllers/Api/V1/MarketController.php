<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Traits\ApiReplyTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MarketController extends Controller
{
    use ApiReplyTrait;
    public function __construct () {

    }

    public function index () {
        $data=DB::table('market_products')->select('uid')->get();
        return $this->successResponse($data);
    }


}
