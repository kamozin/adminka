<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryRetail;
use App\Models\ExternalCatalog;
use App\Traits\ApiReplyTrait;
use App\Traits\CategoryRetailTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{

    use CategoryRetailTrait, ApiReplyTrait;

    public function __construct()
    {

    }

    public function index(Request $request)
    {
        $data = json_decode($request->getContent());

    }

    public function getCatalog(Request $request)
    {

    }

    public function getCatalogItem(Request $request)
    {

    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        DB::beginTransaction();
        foreach ($data as $d) {
            try{
                $category = $this->getItemCatalogExternal('uid', $d->uid);

                if (empty($category)) {

                    $externalCategory = new Category();
                    $fields = (array)$d;
                    $fields['system'] = '1C';
                    if ($fields['parent_uid'] == "00000000-0000-0000-0000-000000000000") {
                        $fields['parent'] = 1;
                        $fields['children'] = 0;
                    } else {
                        $fields['parent'] = 0;
                        $fields['children'] = 1;
                    }
                    $fields['parent_id'] = $fields['parent_uid'];
                    unset($fields['parent_uid']);
                    $externalCategory->create($fields);

                    $retail=new CategoryRetail();
                    $retail->title=$d->title;
                    if ($fields['parent_id'] == "00000000-0000-0000-0000-000000000000") {
                        $retail->parent_id=0;
                    }else{
                        $retail->parent_id=$this->getParentId($fields['parent_id']);
                    }
                    $retail->icon="";
                    $retail->display=1;
                    $retail->sort=0;
                    $retail->save();

                    DB::table('catalog_to_retail')
                        ->insert([
                            "category_retail_uid"=>$retail->uid,
                            "catalog_external_uid"=>$fields['uid']
                        ]);

                } else {

                    $this->update((array)$d, $category);
                }
            }catch (\Exception $e){
                dd($d, $e->getMessage());
            }

        }
        DB::commit();

        return $this->successResponse();
    }

    private function getParentId ($uid)
    {
        $data=DB::table('catalog_to_retail')->where('catalog_external_uid', $uid)->first();

        return $data->category_retail_uid;
    }

    private function update($fields, $model)
    {
        if ($fields['parent_uid'] == '00000000-0000-0000-0000-000000000000') {
            $fields['parent'] = 1;
            $fields['children'] = 0;
        } else {
            $fields['parent'] = 0;
            $fields['children'] = 1;
        }
        $fields['parent_id'] = $fields['parent_uid'];
        unset($fields['parent_uid']);
        $model->update($fields);

        $data=DB::table('catalog_to_retail')->where('catalog_external_uid', $fields['uid'])->first();

        $m=CategoryRetail::where('uid', $data->category_retail_uid)->first();
        $m->title=$fields['title'];
        $m->save();
    }

    public function delete (Request $request) {
        $data = json_decode($request->getContent());
        Category::where('uid', $data->uid)->delete();
        return $this->successResponse();
    }


    public function retail (Request $request)
    {
        $externalCategory = DB::table('external_catalog')->where('')->first();
        $m = new CategoryRetail();

    }


    private function storeRetail($fields, $model)
    {
        $model->create($fields);
    }

    private function updateRetail($fields, $model)
    {
        $model->update($fields);
    }


}
