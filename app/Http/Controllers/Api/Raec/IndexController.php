<?php

namespace App\Http\Controllers\Api\Raec;

use App\Http\Controllers\Controller;
use App\Models\EtimClass;
use App\Models\Features;
use App\Models\FeaturesToEtimClass;
use App\Models\FeaturesValue;
use App\Models\UnitFeatures;
use App\Models\ValueEtimFeatureClass;
use App\Traits\RaecEtimTrait;
use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Xml\Unit;

class IndexController extends Controller
{
    use RaecEtimTrait;

    public function __construct()
    {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
    }

    /**
     *
     */
    public function uploadEtimGroup()
    {
        $this->getEtimGroup();
    }

    /**
     *
     */
    public function uploadEtimClass()
    {
        $this->getPages('etimclass');
        $this->getEtimClass();
    }

    public function uploadFeuters()
    {
        $data = EtimClass::all();
        foreach ($data as $d) {
            $result = $this->getEtimClassId($d->classId);
            $this->forResultFeuters($result->features, $d);
        }
    }

    private function forResultFeuters($result, $etim)
    {

        foreach ($result as $r) {
            if (empty($r->unit)) {
                $unitId = null;
            } else {
                if (!UnitFeatures::where('id', $r->unit->id)->exists()) {
                    $unitModel = new UnitFeatures();
                    $unitModel->id = $r->unit->id;
                    $unitModel->descriptionEn = $r->unit->descriptionEn;
                    $unitModel->descriptionRu = $r->unit->descriptionRu;
                    $unitModel->save();

                }

                $unitId = $r->unit->id;
            }
            if (empty($this->checkFeatureEtim($r->featureId))) {
                $data = [
                    'featureId' => $r->featureId,
                    'descriptionRu' => $r->descriptionRu,
                    'descriptionEn' => $r->descriptionEn,
                    'required' => $r->required,
                    'gold' => $r->gold,
                    'type' => $r->type,
                    'unit_features_id' => $unitId
                ];
                Features::createFeature($data);
            }

            $modelRelatedFeatureEtim = new FeaturesToEtimClass();
            $modelRelatedFeatureEtim->fill(["featureId" => $r->featureId, "classId" => $etim->classId]);
            $modelRelatedFeatureEtim->save();

            if (sizeof($r->values) > 0) {

                foreach ($r->values as $v) {

                    if (FeaturesValue::where('valueId', $v->valueId)->first()==null) {

                        $modelValue = new FeaturesValue();
                        $modelValue->valueId = $v->valueId;
                        $modelValue->descriptionRu = $v->descriptionRu;
                        $modelValue->descriptionEn = $v->descriptionEn;
                        $modelValue->feature_id = $r->featureId;
                        $modelValue->save();
                        $this->setValueRelation($v->valueId, $r->featureId, $etim->classId);
                    } else {

                        $this->setValueRelation($v->valueId, $r->featureId, $etim->classId);
                    }

                }
            }
        }
    }

    private function setValueRelation($valueId, $featureId, $classId)
    {
        $m = new ValueEtimFeatureClass();
        $m->valueId = $valueId;
        $m->featureId = $featureId;
        $m->classId = $classId;
        $m->save();
    }


    public function uploadRaec () {

    }
}
