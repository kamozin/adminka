<?php

namespace App\Http\Controllers;

use App\Models\BrandRaec;
use App\Models\StorageRaec;
use App\Traits\ApiReplyTrait;
use App\Traits\RaecTrait;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    use RaecTrait;
    use ApiReplyTrait;
    public function __construct () {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
    }

    public function index () {
        $query=BrandRaec::query();
        $query->whereHas('storageBrand');
        $query->with('storageBrand');
        $data=$query->get();

        return view('brand.index', compact('data'));
    }

    public function getStorage (Request $request) {
        $query=BrandRaec::query();
        $query->where('brand_id', $request->brand_id);
        $query->with('storageBrand');
        $data=$query->first();
        return $this->successResponse($data);
    }

    public function upload () {
       $data=$this->getBrand();

       foreach ($data as $d) {
           $this->storeBrand($d);
//           if(!empty($d->synonyms)) {
//               $this->storeSynonimBrand(explode(', ', $d->synonyms), $d->id);
//           }
//           if(sizeof($d->series)>0) {
//               $this->storeSeriaBrand($d->series, $d->id);
//           }
//           if(sizeof($d->seriesMarketing)>0) {
//               $this->storeSeriaMarketingBrand($d->seriesMarketing, $d->id);
//           }
       }
    }

    public function setTimeDelivery(Request $request) {
        BrandRaec::where('brand_id', $request->id)
            ->update([
               "timeDelivery"=>$request->timeDelivery
            ]);
        return $this->successResponse();
    }

    public function storageUpload () {
            $storages=$this->getStorage();
            foreach($storages as $s) {
                $model=new StorageRaec();
                $model->storage_id=$s->id;
                $model->brand_id=$s->brandId;
                $model->title=$s->name;
                $model->display=1;
                $model->save();
            }
    }

    public function setTimeDeliveryStorage(Request $request) {
        StorageRaec::where('storage_id', $request->storageId)
            ->update([
                "timeDelivery"=>$request->timeDelivery
            ]);
        return $this->successResponse();
    }
    public function setTimeDeliveryBrand(Request $request) {
        BrandRaec::where('id', $request->brandId)
            ->update([
                "timeDelivery"=>$request->timeDelivery
            ]);
        return $this->successResponse();
    }
    public function display (Request $request) {

        BrandRaec::where('brand_id', $request->brandId)
            ->update(
                [
                    "display"=>$request->display
                ]
            );
        StorageRaec::where('brand_id', $request->brandId)
            ->update(
                [
                    "display"=>$request->display
                ]
            );

        return $this->successResponse();
    }

    public function displayStorage (Request $request) {

        StorageRaec::where('storage_id', $request->storageId)
            ->update(
                [
                    "display"=>$request->display
                ]
            );
        return $this->successResponse();
    }
}
