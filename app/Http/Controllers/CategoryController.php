<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\CategoryRetail;
use App\Models\EtimClass;
use App\Models\SeoCategory;
use App\Models\SeoProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Psy\Util\Str;
use function Symfony\Component\String\s;

class CategoryController extends Controller
{
    public function __construct()
    {

    }


    public function index(Request $request)
    {
        $query = CategoryRetail::query();

        if (isset($request->uid)) {
            $parent = CategoryRetail::find($request->uid);
            $query->with(['etim', 'features']);
            $query->where('parent_id', $request->uid);
        } else {
            $parent = '';
            $query->with(['etim', 'features']);
            $query->where('parent_id', '0');
        }
        $query->with('categories');

        $query->orderBy('sort', 'asc');
        $data = $query->get();
        return view('categoryRetail.index', compact('data', 'parent'));
    }


//'image'
    public function store(Request $request)
    {

        $fields = $request->all();

        $categoryRetail = new CategoryRetail();
        $categoryRetail->title = $request->title;
        $categoryRetail->parent_id = 0;
        $categoryRetail->display = 0;
        $categoryRetail->sort = 0;
        $categoryRetail->save();

        $seo = [
            "title" => $request->titleSeo,
            "description" => $request->descriptionSeo,
            "keywords" => $request->keywordsSeo,
        ];

        $this->storeCategorySeo($categoryRetail->uid, $seo);

        return redirect('category-retail');
    }

    private function storeCategorySeo($uid, $data)
    {


        $seo = new SeoCategory();
        $seo->title = $data['title'];
        $seo->description = $data['description'];
        $seo->keywords = $data['keywords'];
        $seo->category_retail_uid = $uid;
        $seo->save();
    }

    public function create(Request $request)
    {
        return view('categoryRetail.create');
    }

    public function edit(Request $request)
    {
        $data = CategoryRetail::find($request->uid);
        $data->seo()->first();

        return view('categoryRetail.edit', compact('data'));
    }

    public function update(Request $request)
    {
        $categoryRetail = CategoryRetail::find($request->uid);
        $fields = $request->all();

        if ($request->image) {
            $filesImages = $fields['image'] = $request->file('image')->store('categories/image');
            $fields['image'] = basename($filesImages);
            Storage::delete('categories/image/' . $categoryRetail->image);
        }
        if ($request->icon) {
            $file = $request->file('icon');
            $imageFileName = uniqid() . '.' . $file->getClientOriginalExtension();
            $filePath = '/iconsCategory/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $url = Storage::disk('s3')->url('iconsCategory/' . $imageFileName);
        }
        $fields['display'] = ($request->display) ? 1 : 0;
        $categoryRetail->update($fields);


        $seo = [
            "title" => $request->titleSeo ?? '',
            "description" => $request->descriptionSeo ?? '',
            "keywords" => $request->keywordsSeo ?? '',
        ];

        $this->updateCategorySeo($categoryRetail->uid, $seo);
        return redirect('category-retail');
    }


    private function updateCategorySeo($uid, $data)
    {

        $seo = SeoCategory::where('category_retail_uid', $uid)->first();
        if (!empty($seo)) {
            $seo->title = $data['title'];
            $seo->description = $data['description'];
            $seo->keywords = $data['keywords'];
            $seo->save();
        }else{
            $seo=new SeoCategory();
            $data['category_retail_uid']=$uid;
            $seo->create($data);
        }

    }


    public function delete(Request $request)
    {

    }

    public function display(Request $request)
    {
        $categoryRetail = CategoryRetail::find($request->uid);
        $categoryRetail->display = $request->display;
        $categoryRetail->save();
    }

    public function sort(Request $request)
    {
        $query = CategoryRetail::query();

        if (isset($request->uid)) {
            $parent = CategoryRetail::find($request->uid);
            $query->where('parent_id', $request->uid);
        } else {
            $parent = '';
            $query->where('parent_id', '0');
        }

        $query->orderBy('sort', 'asc');
        $data = $query->get();


        return view('categoryRetail.sort', compact('data', 'parent'));
    }

    public function sortUpdate(Request $request)
    {
        try {
            foreach ($request->all() as $key => $c) {

                $cat = CategoryRetail::find($c['uid']);
                $cat->sort = $key + 1;
                $cat->save();
            }
            return response()->json(["success" => 1]);
        } catch (\Exception $e) {
            return response()->json(["success" => 0, "message" => $e->getMessage()]);
        }

    }

    public function struct()
    {
        $query = CategoryRetail::query();
        $query->where('parent_id', '0');
        $query->with('categories');
        $query->orderBy('sort', 'asc');
        $categories = $query->get();

        $data = [];
        foreach ($categories as $d) {

            (object)$obj = [
                "id" => $d->uid,
                "text" => $d->title,
                "children" => []
            ];

            if (sizeof($d->categories) > 0) {
                foreach ($d->categories as $c) {
                    (object)$objChild = [
                        "id" => $c->uid,
                        "text" => $c->title
                    ];

                    array_push($obj['children'], $objChild);
                }
            }


            array_push($data, $obj);
        }


        return view('categoryRetail.struct', compact('data'));
    }

    public function structUpdate(Request $request)
    {

        foreach ($request->all() as $category) {
            $model = CategoryRetail::find($category['id']);
            $model->parent_id = 0;
            $model->save();

            if (sizeof($category['children']) > 0) {
                foreach ($category['children'] as $child) {
                    $childModel = CategoryRetail::find($child['id']);
                    $childModel->parent_id = $category['id'];
                    $childModel->save();
                }
            }
        }

    }

    public function relationEtim(Request $request)
    {
        $category = CategoryRetail::find($request->uid);
        $class = EtimClass::whereNull('category_retail_uid')->get();
        $classRelated = EtimClass::where('category_retail_uid', $request->uid)->get();

        $data = [
            "etim" => [],
            "related" => []
        ];
        foreach ($class as $c) {

            $str = $c->classId . ' | ' . $c->descriptionRu;
            array_push($data['etim'], $str);

        }

        $dataRelated = [];
        foreach ($classRelated as $c) {

            $str = $c->classId . ' | ' . $c->descriptionRu;
            array_push($data['related'], $str);

        }

        return view('categoryRetail.etim', compact('data', 'category'));
    }

    public function relationEtimUpdate(Request $request)
    {

        EtimClass::where('category_retail_uid', $request->uid)->update(
            [
                'category_retail_uid' => null
            ]
        );

        foreach ($request->etim as $etim) {
            EtimClass::where('classId', explode(' | ', $etim)[0])->update(
                [
                    'category_retail_uid' => $request->uid
                ]
            );
        }
    }


    public function getEtimNotRelation()
    {
        $data = EtimClass::whereNull('category_retail_uid')->get();

        $class = [];
        foreach ($data as $c) {
            $str = $c->classId . ' | ' . $c->descriptionRu;
            array_push($class, $str);
        }
        return response()->json($class);
    }


    public function relationExternalCatalog(Request $request)
    {

    }

}
