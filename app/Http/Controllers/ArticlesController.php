<?php

namespace App\Http\Controllers;

use App\Models\Actions;
use App\Models\Article;
use App\Models\CategoryRetail;
use App\Models\ListProduct;
use App\Models\ListProductToProduct;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Traits\ApiReplyTrait;
use App\Traits\check1CTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ArticlesController extends Controller
{
    private $pathView = 'articles.';
    private $model;
    private $modelListProduct;

    use check1CTrait;
    use ApiReplyTrait;

    public function __construct(Article $article, ListProduct $listProduct, CategoryRetail $categoryRetail)
    {
        $this->model = $article;
        $this->modelListProduct = $listProduct;
        $this->modelCategoryRetail = $categoryRetail;
    }

    public function index()
    {
        $articles = Article::orderBy('id', 'desc')->get();
        return view($this->pathView . 'index', compact('articles'));
    }

    public function create()
    {
        $lists = $this->modelListProduct->all();
        $categories = CategoryRetail::all();

        return view($this->pathView . 'create', compact('lists', 'categories'));
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $article = new Article();
        $article->title = $request->title;//+
        $article->dt = $request->dt;
        $article->description = $request->description;//+
        $article->description_seo = $request->description_seo;//+
        $article->keywords_seo = $request->keywords_seo;//+
        $article->title_seo = $request->title_seo;//+
        $article->text = $request->text;
        $article->display = $request->display ?? 0;
        $article->main_page = $request->main_page ?? 0;
        $article->category_uid = $request->category_uid;
        $article->erid = $request->erid ?? '';

        if (isset($request->imageMain)) {

            $file = $request->file('imageMain');
            $imageFileName = uniqid() . '.' . $file->getClientOriginalExtension();
            $filePath = '/articles/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $url = Storage::disk('s3')->url('articles/' . $imageFileName);
            $article->image = $url;
        }

        $article->save();
        if(sizeof($request->list)>0){
            foreach ($request->list as $l){
                DB::table('articles_lists')
                    ->insert([
                        "article_id"=>$article->id,
                        "list_id"=>$l
                    ]);
            }
        }
        return redirect()->route('articles.index')->with('messageSuccess', 'Статья создана');
    }

    private function uploadImage($file)
    {
        return $file->store('image');
    }

    public function uploadImageText(Request $request)
    {
        return $request->file('file')->store('image');
    }

    private function uploadSlider($file)
    {
        return $file->store('slider');
    }

    public function edit(Request $request)
    {
        $data = $this->model->find($request->id);
        $categories = CategoryRetail::all();
        $lists = $this->modelListProduct->select('id', 'title')->get();
        $selectedLists=DB::table('articles_lists')->where('article_id', $request->id)->get()->pluck('list_id')->toArray();
//        foreach ($lists as $l){
//            if(in_array($l->id, $selectedLists)){
//                dd($l);
//            }
//        }
        return view($this->pathView . 'edit', compact('data', 'categories', 'lists', 'selectedLists'));
    }

    public function update(Request $request)
    {

        $article = Article::find($request->id);

        $article->title = $request->title;
        $article->dt = $request->dt;
        $article->description_seo = $request->description_seo;//+
        $article->keywords_seo = $request->keywords_seo;//+
        $article->title_seo = $request->title_seo;//+
        $article->description = $request->description;
        $article->text = $request->text;
        $article->category_uid = $request->category_uid;
        $article->erid = $request->erid ?? '';
        if (isset($request->imageMain)) {
            $file = $request->file('imageMain');
            $imageFileName = uniqid() . '.' . $file->getClientOriginalExtension();
            $filePath = '/articles/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $url = Storage::disk('s3')->url('articles/' . $imageFileName);
            $article->image = $url;
        }
        $article->display = $request->display ?? 0;
        $article->main_page = $request->main_page ?? 0;
        $article->save();

        if($request->list && sizeof($request->list)>0){
            foreach ($request->list as $l){
                DB::table('articles_lists')
                    ->insert([
                        "article_id"=>$article->id,
                        "list_id"=>$l
                    ]);
            }
        }else{
            DB::table('articles_lists')->where('article_id', $request->id)->delete();
        }
        return redirect()->route('articles.index')->with('messageSuccess', 'Статья обновлена');
    }


    public function drop(Request $request)
    {
        return redirect()->route('articles.index')->with('messageSuccess', 'Статья удалена');
    }

    public function updateVisible(Request $request)
    {
        $article = Article::find($request->id);
        $article->visible = $request->visible;
        $article->save();
        return $this->successResponse();
    }
}
