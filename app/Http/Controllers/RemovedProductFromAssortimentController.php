<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Traits\RaecProductTrait;
use Illuminate\Http\Request;

class RemovedProductFromAssortimentController extends Controller
{
    use RaecProductTrait;

    public function start()
    {
        $products = Product::where('brandName', 'Arlight')
            ->where('sku', '<>', '')
            ->get();

        foreach ($products as $p) {
            try{
                $filter = 'Бренд = ' . trim($p->brandName) . ' AND КодПоставщика = ' . trim($p->sku) . '';
                $data = $this->getProductRql($filter);
                if (isset($data[0])) {
                    if ($data[0]->supplierStatus->id == 11) {
                        Product::where('uid', $p->uid)
                            ->update([
                                "display"=>0
                            ]);
                    }
                }
            }catch (\Exception $e){

            }
        }
    }
}
