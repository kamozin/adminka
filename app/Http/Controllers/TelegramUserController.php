<?php

namespace App\Http\Controllers;

use App\Models\TelegramUser;
use Illuminate\Http\Request;

class TelegramUserController extends Controller
{
    protected $model;

    protected $shop = [
        '0'=>'Все магазины',
        '1' => 'Бурова',
        '2' => 'Ст. Димитрова, 67',
        '3' => 'Степная, 12',
        '4' => '22-го Сьезда КПСС, 96',
        '5' => 'Белобережская, 20',
    ];

    protected $role = [
        'admin',
        'user'
    ];

    public function __construct(TelegramUser $telegramUser)
    {
        $this->model = $telegramUser;
    }

    public function index()
    {
        $data = $this->model->get();
        $shop = $this->shop;
        return view('telegram_user.index', compact('data', 'shop'));
    }

    public function create()
    {
        $shop = $this->shop;
        $role = $this->role;
        return view('telegram_user.create', compact('role', 'shop'));
    }

    public function store(Request $request)
    {
        $this->model->create($request->all());

        return redirect()->route('telegram-user');
    }

    public function edit(Request $request)
    {
        $data = $this->model->where('id', $request->id)->first();

        $shop = $this->shop;
        $role = $this->role;
        return view('telegram_user.edit', compact('data','role', 'shop'));

    }


    public function update(Request $request)
    {

        $this->model->where('id', $request->id)->update([
            "user"=>$request->user,
            "role"=>$request->role,
            "shop"=>$request->shop,
        ]);
        return redirect()->route('telegram-user');
    }

    public function display(Request $request)
    {
        $this->model->where('id', $request->user_id)
            ->update([
                "disabled"=>$request->disabled
            ]);
        return redirect()->route('telegram-user');
    }

    public function delete(Request $request)
    {
        $this->model->where('id', $request->id)->delete();
        return redirect()->route('telegram-user');
    }


}
