<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OptUserController extends Controller
{
    public function __construct () {

    }

    public function index () {
        $users=DB::connection('mysqlOldSite')->table('opt_users')
            ->where('name', '<>', 'demo')
            ->orderBy('id', 'desc')
            ->paginate(300);
        return view('opt.users.index', compact('users'));
    }
}
