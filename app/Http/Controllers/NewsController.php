<?php

namespace App\Http\Controllers;

use App\Models\Actions;
use App\Models\ListProduct;
use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{

    private $pathView = 'news.';
    private $model;
    private $modelListProduct;
    public function __construct(News $news, ListProduct $listProduct)
    {
        $this->model=$news;
        $this->modelListProduct=$listProduct;
    }

    public function index()
    {
        $news = News::all();
        return view($this->pathView . 'index', compact('news'));
    }

    public function create()
    {
        $lists=$this->modelListProduct->all();
        return view($this->pathView . 'create', compact('lists'));
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $news = new News();
        $news->title = $request->title;//+
        $news->description = $request->description;//+
        $news->text = $request->text;
        $news->display = $request->display ?? 0;
        $news->main_page = $request->main_page ?? 0;
        $news->list_id = $request->list_id;
        $news->erid = $request->erid ?? '';
        if(isset($request->imageMain)){
            $news->image = $request->file('imageMain')->store('actions');
        }
        $news->save();
        if(isset($request->list)){
            if($request->list && sizeof($request->list)>0){
                foreach ($request->list as $l){
                    DB::table('news_lists')
                        ->insert([
                            "news_id"=>$news->id,
                            "list_id"=>$l
                        ]);
                }
            }else{
                DB::table('news_lists')->where('news_id', $request->id)->delete();
            }
        }

        return redirect()->route('news.index')->with('messageSuccess', 'Акция создана');
    }

    private function uploadImage($file)
    {
        return $file->store('image');
    }

    public function uploadImageText (Request $request) {
        return $request->file('file')->store('image');
    }


    public function edit(Request $request)
    {
        $data = $this->model->find($request->id);
        $lists = $this->modelListProduct->select('id', 'title')->get();
        $selectedLists=DB::table('news_lists')->where('news_id', $request->id)->get()->pluck('list_id')->toArray();
        return view($this->pathView . 'edit', compact('data', 'lists', 'selectedLists'));
    }

    public function update(Request $request)
    {
        $news = News::find($request->id);
        $news->title = $request->title;
        $news->description = $request->description;
        $news->text = $request->text;
        $news->list_id = $request->list_id;
        $news->erid = $request->erid ?? '';
        if(isset($request->imageMain)){
            $news->image = $request->file('imageMain')->store('news');
        }
        $news->display = $request->display ?? 0;
        $news->main_page = $request->main_page ?? 0;
        $news->save();

        if($request->list && sizeof($request->list)>0){
            DB::table('news_lists')->where('news_id', $news->id)->delete();
            foreach ($request->list as $l){
                DB::table('news_lists')
                    ->insert([
                        "news_id"=>$news->id,
                        "list_id"=>$l
                    ]);
            }
        }else{
            DB::table('news_lists')->where('news_id', $request->id)->delete();
        }
        return redirect()->route('news.index')->with('messageSuccess', 'Акция обновлена');
    }


    public function drop(Request $request)
    {
        return redirect()->route('news.index')->with('messageSuccess', 'Акция удалена');
    }
}
