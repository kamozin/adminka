<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Rap2hpoutre\FastExcel\FastExcel;

class UploadController extends Controller
{
    public function __construct () {

    }

    public function upload (Request $request) {
        if($request->hasFile('upload')) {
//            $originName = $request->file('upload')->getClientOriginalName();
//            $fileName = pathinfo($originName, PATHINFO_FILENAME);
//            $extension = $request->file('upload')->getClientOriginalExtension();
//            $fileName = $fileName.'_'.time().'.'.$extension;
//
////            $request->file('upload')->move(public_path('uploads'), $fileName);
//            $url=Storage::disk('s3')->url('actions/' . $fileName);
            $file = $request->file('upload');
            $imageFileName = uniqid(). '.' . $file->getClientOriginalExtension();
            $filePath = '/actions/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $url=Storage::disk('s3')->url('actions/' . $imageFileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
//            $url = '/uploads/'.$fileName;
            $msg = 'Image uploaded successfully';
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }

    public function antey () {
        ini_set('max_execution_time', 86400);
        ini_set("memory_limit", "1000M");
        $data=DB::connection('antey')->table('products')
            ->select('products.id', 'products.title', 'products.slug', 'products.supplier', 'products.sku', 'products.parent_id', 'products.price', 'category.title as title_category')
            ->join('category', 'products.parent_id', '=', 'category.id')
            ->get();

        $products=[];

        foreach($data as $d) {
            $arr=[
                "ID товара"=>$d->id,
                "Наименование товара"=>$d->title,
                "Наименование категории"=>$d->title_category ?? '',
                "Артикул товара"=>$d->sku,
                "Поставщик"=>$d->supplier,
                "Цена"=>$d->price,
                "Ссылка"=>'http://antey32.ru/product/'.$d->slug,
            ];
            array_push($products, $arr);
        }

        (new FastExcel($products))->export('antey.xlsx');
    }
}
