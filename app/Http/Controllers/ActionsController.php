<?php

namespace App\Http\Controllers;

use App\Models\Actions;
use App\Models\ListProduct;
use App\Models\ListProductToProduct;
use App\Models\Product;
use App\Models\ProductPrice;
use App\Traits\ApiReplyTrait;
use App\Traits\check1CTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ActionsController extends Controller
{

    private $pathView = 'actions.';
    private $model;
    private $modelListProduct;

    use check1CTrait;
    use ApiReplyTrait;

    public function __construct(Actions $actions, ListProduct $listProduct)
    {
        $this->model = $actions;
        $this->modelListProduct = $listProduct;
    }

    public function index()
    {


        $actions = Actions::orderBy('id', 'desc')->get();
        return view($this->pathView . 'index', compact('actions'));
    }

    public function create()
    {

        $list = $this->modelListProduct->all();
        return view($this->pathView . 'create', compact('list'));
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        $action = new Actions();
        $action->title = $request->title;//+
        $action->dt = $request->dt_start . '|' . $request->dt_end;
        $action->dt_start = $request->dt_start;
        $action->dt_end = $request->dt_end;
        $action->description = $request->description;//+
        $action->text = $request->text;
        $action->display = $request->display ?? 0;
        $action->list_id = $request->list_id;
        $action->main_page = $request->main_page ?? 0;
        $action->erid = $request->erid ?? '';
        if (isset($request->imageMain)) {

            $file = $request->file('imageMain');
            $imageFileName = uniqid() . '.' . $file->getClientOriginalExtension();
            $filePath = '/actions/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $url = Storage::disk('s3')->url('actions/' . $imageFileName);
            $action->image = $url;
        }

        if (isset($request->promotionalSupplement)) {
            $filePromotionalSupplement = $request->file('promotionalSupplement');
            $imageFileName = uniqid() . '.' . $filePromotionalSupplement->getClientOriginalExtension();
            $filePath = '/promotional_supplement/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($filePromotionalSupplement), 'public');
            $urlPromotionalSupplement = Storage::disk('s3')->url('actions/' . $imageFileName);
            $action->promotionalSupplement = $urlPromotionalSupplement;
        }

        $action->save();

        return redirect()->route('actions.index')->with('messageSuccess', 'Акция создана');
    }

    private function uploadImage($file)
    {
        return $file->store('image');
    }

    public function uploadImageText(Request $request)
    {
        return $request->file('file')->store('image');
    }

    private function uploadSlider($file)
    {
        return $file->store('slider');
    }

    public function edit(Request $request)
    {
        $data = $this->model->find($request->id);
        $data->dt_start = explode('|', $data->dt)[0];
        $data->dt_end = explode('|', $data->dt)[1];
        $list = $this->modelListProduct->all();

        return view($this->pathView . 'edit', compact('data', 'list'));
    }

    public function update(Request $request)
    {

        $action = Actions::find($request->id);

        $action->title = $request->title;
        if (!$action->uid_dok) {
            $action->dt = $request->dt_start . '|' . $request->dt_end;
        }
        if (!$action->uid_dok) {
            $action->dt_start = $request->dt_start;
            $action->dt_end = $request->dt_end;
        }

        $action->description = $request->description;
        $action->text = $request->text;
        $action->erid = $request->erid ?? '';
        if (isset($request->imageMain)) {
            $file = $request->file('imageMain');
            $imageFileName = uniqid() . '.' . $file->getClientOriginalExtension();
            $filePath = '/actions/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
            $url = Storage::disk('s3')->url('actions/' . $imageFileName);
            $action->image = $url;
        }
        if (isset($request->promotionalSupplement)) {
            $filePromotionalSupplement = $request->file('promotionalSupplement');

            $imageFileName = uniqid() . '.' . $filePromotionalSupplement->getClientOriginalExtension();
            $filePath = '/promotional_supplement/' . $imageFileName;
            Storage::disk('s3')->put($filePath, file_get_contents($filePromotionalSupplement), 'public');
            $urlPromotionalSupplement = Storage::disk('s3')->url('promotional_supplement/' . $imageFileName);
            $action->promotionalSupplement = $urlPromotionalSupplement;
        }
        $action->display = $request->display ?? 0;
        if (!$action->uid_dok) {
            $action->list_id = $request->list_id;
        }
        $action->main_page = $request->main_page ?? 0;
        $action->save();

        $listProducts = ListProductToProduct::where('list_id', $action->list_id)
            ->get();
        if ($action->uid_dok) {
            if ($request->display == 0) {

                foreach ($listProducts as $product) {
                    $productInfo = Product::where('uid', $product->product_uid)->first();

                    $object = (object)[
                        "uid" => $productInfo->external_id,
                        "typePrice" => 6
                    ];

//                    $price = $this->getPrice($object);
////                    dd($price, $productInfo);
//                    ProductPrice::where('type_price', 6)
//                        ->where('products_uid', $productInfo->uid)
//                        ->update([
//                            "price" => $price[0]->price,
//                            "dt_end" => null
//                        ]);
                    try {
                        $price = $this->getPrice($object);
                        ProductPrice::where('type_price', 6)
                            ->where('products_uid', $productInfo->uid)
                            ->update([
                                "price" => $price[0]->price,
                                "dt_end" => null
                            ]);
                    } catch (\Exception $e) {
                        ProductPrice::where('type_price', 6)
                            ->where('products_uid', $productInfo->uid)
                            ->update([
                                "price" => 0,
                                "dt_end" => null
                            ]);
                    }
                }
            } else {
                foreach ($listProducts as $product) {
                    $productInfo = Product::where('uid', $product->product_uid)->first();
//                    dd($productInfo, $product, $request->dt_end);
                    $this->setPriceProduct($productInfo, $product->skidka, $request->dt_end);
                }
            }
        }


        ListProductToProduct::where('list_id', $action->list_id)
            ->update([
                "iconDisplay" => $request->display
            ]);


        return redirect()->route('actions.index')->with('messageSuccess', 'Акция обновлена');
    }

    private function setPriceProduct($product, $skidka, $dt)
    {
        $productPrice = ProductPrice::where('products_uid', $product->uid)
            ->where('type_price', 4)->first();
        if ($productPrice->price == 0) {

        } else {
            $skidkaPrice = $productPrice->price - ($productPrice->price * ($skidka / 100));
//            dd($skidkaPrice);
            ProductPrice::where('products_uid', $product->uid)
                ->where('type_price', 6)
                ->update([
                    "price" => $skidkaPrice,
                    "dt_end" => $dt
                ]);
        }
    }

    public function drop(Request $request)
    {

        return redirect()->route('actions.index')->with('messageSuccess', 'Акция удалена');
    }

    public function updateVisible(Request $request)
    {
        $action = Actions::find($request->id);
        $action->visible = $request->visible;
        $action->save();
        return $this->successResponse();
    }
}
