<?php

namespace App\Http\Controllers;

use App\Models\RetailFeatureValueProduct;
use Illuminate\Http\Request;

class ValueRetailFeatureValueProductDropDuplicateController extends Controller
{
    public function start (){
        $data= RetailFeatureValueProduct::all();

        foreach ($data as $d){
            $check = RetailFeatureValueProduct::where('category_uid', $d->category_uid)
                ->where('system', $d->system)
                ->where('product_uid', $d->product_uid)
                ->where('feature_uid', $d->feature_uid)
                ->where('value_uid', $d->value_uid)
                ->where('value', $d->value)
                ->get();

            if(sizeof($check)>1){
                RetailFeatureValueProduct::where('category_uid', $d->category_uid)
                    ->where('system', $d->system)
                    ->where('product_uid', $d->product_uid)
                    ->where('feature_uid', $d->feature_uid)
                    ->where('value_uid', $d->value_uid)
                    ->where('value', $d->value)
                    ->where('id', '<>', $d->id)
                    ->delete();
            }
        }
    }
}
