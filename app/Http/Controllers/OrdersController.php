<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderProducts;
use App\Traits\SmsTrait;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    use SmsTrait;

    public function __construct()
    {

    }

    public function index () {
        $data=Order::orderBy('id', 'desc')->paginate(100);
        return view('orders.index', compact('data'));
    }

    public function order (Request  $request) {

        $data=Order::where('id', $request->id)
            ->first();

        if($data->user_id!=0){
            $user=\App\User::where('id', $data->user_id)->first();
            $data->phone=$user->phone;
        }

        $ordersProduct=OrderProducts::with('product')
            ->where('order_id', $request->id)->get();

        return view('orders.order', compact('data', 'ordersProduct'));
    }

    public function ordersSuccessSms (Request $request) {
        if(Auth::user()->id==3){
            $order=Order::where('id', $request->id)->first();
            if($order->user_id==0){
                $text= "Ваш заказ №".$order->id." готов к получению. Срок хранения заказа 5 дней";
                $this->smsSend($order->phone, $text);
                return redirect()->back();
            }else{
                $user=\App\User::where('id', $order->user_id)->first();
                $text= "Ваш заказ №".$order->id." готов к получению. Срок хранения заказа 5 дней";
                $this->smsSend($user->phone, $text);
                return redirect()->back();
            }
        }
    }

    public function updatePayment (Request $request) {
        Order::where('id', $request->id)
            ->update(
                [
                    "paymeant"=>$request->paymeant
                ]
            );
        return redirect()->back();
    }
}
