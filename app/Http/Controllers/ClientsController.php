<?php

namespace App\Http\Controllers;

use App\Traits\ApiReplyTrait;
use App\User;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    use ApiReplyTrait;
    public function __construct () {

    }

    public function index (Request $request, User $user) {
        if($request->email){
            $data=$user->select('id', 'name', 'phone', 'email')
                ->where('email', $request->email)
                ->paginate(300);
        }else{
            $data=$user->select('id', 'name', 'phone', 'email')->paginate(300);
        }


       return view('clients.index', compact('data'));
    }

    public function updatePhone (Request $request) {
        User::where('id', $request->userId)->update(
            [
                "phone"=>$request->phone
            ]
        );

        return $this->successResponse();
    }

    public function client (Request $request) {

    }
}
