<?php

namespace App\Http\Controllers;

use App\Models\Slider;
use App\Traits\ApiControllerTrait;
use App\Traits\ApiReplyTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    use ApiReplyTrait;

    public function __construct()
    {

    }

    public function index()
    {

        $data = Slider::all();

        return view('slider.index', compact('data'));

    }

    public function store(Request $request)
    {
        try {
            Slider::create($request->all());


            $data = [
                "message" => "Слайдер успешно создан",
                "slider" => Slider::all()
            ];
            return $this->successResponse($data);

        } catch (\Exception $e) {
            return $this->errorResponse();
        }

    }

    public function update(Request $request)
    {

        try {
            $slider = Slider::find($request->id);
            $slider->title = $request->title;
            $slider->file = $request->file;
            $slider->url = $request->url;
            $slider->sort = $request->sort;
            $slider->save();

            $data = [
                "message" => "Слайдер успешно обновлен",
                "slider" => Slider::all()
            ];

            return $this->successResponse($data);
        } catch (\Exception $e) {
            return $this->errorResponse();
        }


    }

    public function uploadSlider(Request $request)
    {
        return $request->file('file')->store('slider');
    }

    public function delete(Request $request)
    {
        try {

            Slider::where('id', $request->id)->delete();
            $data = [
                "message" => "Слайдер успешно удален",
                "slider" => Slider::all()
            ];
            return $this->successResponse($data);

        } catch (\Exception $e) {
            return $this->errorResponse();
        }
    }
}
